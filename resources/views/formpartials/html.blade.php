@php
        //dd($contentvariable);
@endphp

<div class="form-group m-form__group row @if ($errors->has(data_get($value, 'type'))) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        {{data_get($value, 'title') }}
    </label>
    @php 
        if (Request::segment(4) != 'add' && empty(old('content'))) {
            if (is_null($content->variableLang(Request::segment(6)))) {
                $contentvariable = $content->variableLang($langs->first()->code);
            }else {
                $contentvariable = $content->variableLang(Request::segment(6));
            }   
        }else{
            $_content_value = old('content');
        }
            //dd($contentvariable);
        $x = ''; 
        if(!empty(json_decode($contentvariable->content)->tax_rate)){
        	$tax_rate=json_decode($contentvariable->content)->tax_rate;
        }
        else{
        	$tax_rate=0;
        }
        if(!empty(json_decode($contentvariable->content)->discounted_price)){
        	$price=json_decode($contentvariable->content)->discounted_price;
        }
        else{
	        if(!empty(json_decode($contentvariable->content)->price)){
        		$price=json_decode($contentvariable->content)->price;
	        }else{
	        	$price=0;
	        }
	    }	

    @endphp
    <div class="col-7">
    	Vergi Oranı: <span style="font-size: 14px" id="taxrate" class="badge badge-info"> %{{$tax_rate}}</span>
    	Vergi Tutarı: <span style="font-size: 14px" id="taxtprice" class="badge badge-info"> {{($price/100)*$tax_rate}}₺</span>
    	Toplam Fiyat: <span style="font-size: 14px" id="totalcost"class="badge badge-info"> {{(($price/100)*$tax_rate)+$price}}₺</span>
		<button type="button" class="btn btn-primary" onclick="update()">Hesapla</button>
    </div>
</div>
     <script type="text/javascript">

     	function update() {
     		var tax_rate=document.getElementById("tax_rate").value;
     		var price=document.getElementById("price").value;
     		var discounted_price=document.getElementById("discounted_price").value;

     		if(discounted_price){
     			var current_price=discounted_price;
     		}
     		else{
     			var current_price=price;
     		}
     		console.log(current_price);

     		document.getElementById("taxrate").innerHTML ="%"+tax_rate;
     		var tax_price=(tax_rate*current_price)/100;
     		document.getElementById("taxtprice").innerHTML =tax_price+"₺";
     		document.getElementById("totalcost").innerHTML =parseFloat(tax_price)+parseFloat(current_price)+"₺";


		}
     	 
     </script>

