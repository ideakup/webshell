@php
    //dd(json_decode($contentvariable->content)->$key);
    if (Request::segment(4) != 'add' && empty(old('content'))) {
        if (is_null($content->variableLang(Request::segment(6)))) {
            $contentvariable = $content->variableLang($langs->first()->code);
        }else{
            $contentvariable = $content->variableLang(Request::segment(6));
        }   
    }else {
        $_content_value = old('content');
    }
@endphp

<div class="form-group m-form__group" style="padding-bottom: 0;">
    <label>{{data_get($value, 'title')}}</label>
</div>
@php 
    //dd(data_get($value, 'type'));
	$x = ''; 
@endphp
<div class="form-group m-form__group @if ($errors->has(data_get($value, 'type'))) has-danger @endif">
    <textarea class="form-control m-input" type="{{data_get($value, 'type')}}" id="{{ $key }}" name="{{ $key }}" rows="3">
		{{ (empty(json_decode($contentvariable->content)->$key)) ? '' : json_decode($contentvariable->content)->$key }} {!! $x !!}
	</textarea>

    @if ($errors->has(data_get($value, 'type')))
        <div id="{{data_get($value, 'type')}}-error" class="form-control-feedback">{{ $errors->first(data_get($value, 'type')) }}</div>
    @endif
</div>