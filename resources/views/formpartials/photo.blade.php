@php
    //dd(json_decode($contentvariable->content)->$key);
   if (Request::segment(4) != 'add' && empty(old('content'))) {
        if (is_null($content->variableLang(Request::segment(6)))) {
            $contentvariable = $content->variableLang($langs->first()->code);
        } else {
            $contentvariable = $content->variableLang(Request::segment(6));
        }   
    } else {
        $_content_value = old('content');
    }

@endphp

<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">  {{data_get($value, 'title') }} Yükle </h3>
        </div>
    </div>
</div>
<div class="m-portlet__body">

    <div class="form-group m-form__group row">

        <div class="col-12">
            <div class="m-dropzone dropzone" action="{{ url('/uploadFile') }}" id="{{$key}}">
                <div class="m-dropzone__msg dz-message needsclick">
                    <h3 class="m-dropzone__msg-title">
                        Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın...
                    </h3>
                    <span class="m-dropzone__msg-desc">
                        Bu alan <strong>jpg</strong> ve <strong>png</strong> formatlarını yüklemenize izin verir.
                    </span>
                </div>
            </div>
        </div>

    </div>


    <div class="form-group m-form__group row">
                    
                    @php $isImgExist = false; @endphp
                    @if (!is_null($content->variableLang(Request::segment(6))) && !empty(json_decode($contentvariable->content)->$key))
                        @php 
                        	$isImgExist = true;
                        	$img_path=json_decode($contentvariable->content)->$key;
                        @endphp
                    @endif
                        
                    <div class="col-6 text-center">
                        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">
                            <div class="m-portlet__head m-portlet__head--fit" style="padding: 0 15px;">
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">

                                        <li class="m-portlet__nav-item" aria-expanded="true">
                                            <a href="#" id="cropPhotoButton" @if($isImgExist) onclick="photoGalleryThumbnailCrop('{{ url('upload/xlarge/'.$img_path) }}', {{$contentvariable->id}}, '{{ $content->type }}'); return false;" @endif class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">
                                                <i class="fa fa-crop"></i>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <div class="m-widget19">
                                    <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" id="imgContainer">
                                        @if($isImgExist)
                                            <img class="img-fluid" src="{{ url('upload/xlarge/'.$img_path) }}" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6 text-center">
                        <div class="m-portlet m-portlet--bordered-semi" style="margin-bottom: 0;">
                            <div class="m-portlet__head m-portlet__head--fit" style="padding: 0 15px;">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body" style="padding-bottom: 0;">
                                <div class="m-widget19">
                                    <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" id="thumbnailContainer">

                                        <img class="img-fluid" id="thumbnail-{{ $content->variableLang(Request::segment(6))->id }}" @if($isImgExist) src="{{ url('upload/thumbnail/'.$img_path) }}" @endif />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

</div>