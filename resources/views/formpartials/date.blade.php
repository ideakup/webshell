<div class="form-group m-form__group row @if ($errors->has(data_get($value, 'type'))) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        {{data_get($value, 'title') }}
    </label>
    @php 
        if (Request::segment(4) != 'add' && empty(old('content'))) {
            if (is_null($content->variableLang(Request::segment(6)))) {
                $contentvariable = $content->variableLang($langs->first()->code);
            }else {
                $contentvariable = $content->variableLang(Request::segment(6));
            }   
        }else{
            $_content_value = old('content');
        }
            //dd($key);
            $x = ''; 
            //dump(json_decode($contentvariable->content)->$key);
    @endphp
    <div class="col-7">


       <input type="text"  class="form-control tleft past-enabled" placeholder="MM/DD/YYYY" id="{{ $key }}" name="{{ $key }}"
        value="{{ (empty(json_decode($contentvariable->content)->$key)) ? '' : json_decode($contentvariable->content)->$key }}" {!! $x !!} >

        @if ($errors->has(data_get($value, 'type')))
            <div id="{{data_get($value, 'type')}}-error" class="form-control-feedback">{{ $errors->first(data_get($value, 'type')) }}</div>
        @endif
    </div>
</div>



