@php
    //dd($contentvariable);
    if(Request::segment(4) != 'add' && empty(old('content'))){
        if(is_null($content->variableLang(Request::segment(6)))){
            $contentvariable = $content->variableLang($langs->first()->code);
        }else{
            $contentvariable = $content->variableLang(Request::segment(6));
        }   
    } else {
        $_content_value = old('content');
    }
@endphp

<div class="form-group m-form__group row @if ($errors->has($key)) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        {{data_get($value, 'title') }}
    </label>
    <div class="col-7">
        @if ($errors->has($key))
            <div id="select-error" class="form-control-feedback">{{ $errors->first($key) }}</div>
        @endif
        <select class="form-control m-select2" id="{{$key}}" name="{{$key}}" >
            <option value="">  Seçiniz... </option>
            @foreach (data_get($value, 'options') as $keyv => $colv)
                <option value="{{ $colv['value'] }}" 
                    @if (!empty(json_decode($contentvariable->content)->$key))
                        @if (!empty($content) && json_decode($contentvariable->content)->$key == $colv['value']) 
                            {{ 'selected' }} 
                        @endif
                    @endif
                    > {{ $colv['name'] }} 
                </option>
            @endforeach
        </select>
    </div>
</div>


