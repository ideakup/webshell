<div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow">
            @foreach(config('webshell.aside_menu') as $key)
                @if(data_get($key, 'visible')=='true')
                    <li class="m-menu__item {{ (Request::segment(1) === data_get($key, 'name')) ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                        <a href="{{ url(data_get($key, 'url_name')) }}" class="m-menu__link ">
                            <i class="m-menu__link-icon {{data_get($key, 'icon')}}"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        {{data_get($key, 'title')}}
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>
                @endif   
            @endforeach
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>

