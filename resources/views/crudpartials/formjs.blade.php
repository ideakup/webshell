@if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))
    @if ($content->type == 'form')
        <script src="{{ asset('js/formBuilder.js') }}"></script>
    @endif
@endif


@if ((Request::segment(4) == 'edit' || Request::segment(4) == 'galup') && !is_null(Request::segment(6)))

    @if ($content->type == 'form')

    	<script type="text/javascript">

	        var options = {
	            i18n: {
	                locale: 'tr-TR'
	            },
	            formData: @if(is_null($content->variableLang(Request::segment(6)))) JSON.stringify({!! $content->variableLang($langs->first()->code)->content!!}) @else JSON.stringify({!! $content->variableLang(Request::segment(6))->content!!}) @endif,
	            dataType: 'json',
	            editOnAdd: true,
	            disableFields: ['button', 'autocomplete', 'hidden', 'file'],
	            showActionButtons: false,
	            sortableControls: true,
	            controlPosition: 'left',
	            stickyControls: {
	                enable: false
	            },
	            controlOrder: [
	                'header',
	                'paragraph',
	                'text',
	                'textarea',
	                'date',
	                'number',
	                'checkbox-group',
	                'radio-group',
	                'select'
	            ],
	            disabledAttrs: [
	                'inline',
	                'className',
	                'access',
	                'description',
	                'maxlength',
	                'multiple',
	                'name',
	                'other',
	                'step',
	                'toggle',
	                'value'
	            ],
	            disabledSubtypes: {
	                paragraph: ['address', 'blockquote', 'canvas', 'output'],
	                text: ['password', 'color'],
	                textarea: ['tinymce', 'quill'],
	            },
	            replaceFields: [
	                {
	                    type: "text",
	                    label: "Metin",
	                    className: "sm-form-control",
	                    maxlength: 250
	                },
	                {
	                    type: "textarea",
	                    label: "Metin Alanı",
	                    className: "sm-form-control",
	                    rows: 4,
	                    maxlength: 1000
	                },
	                {
	                    type: "number",
	                    label: "Numara",
	                    className: "sm-form-control",
	                    step: 1
	                },
	                {
	                    type: "checkbox-group",
	                    label: "Onay Kutusu Grubu",
	                    className: "sm-form-control",
	                    inline: true
	                },
	                {
	                    type: "radio-group",
	                    label: "Radyo Grubu",
	                    className: "sm-form-control",
	                    inline: true
	                }
	            ]
	        };

        </script>
        
    @endif

@endif