@php

    $_form_visible = false;
  
    

    // $_form_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){
        if ($menu->type != 'content'){
            if (starts_with($content->type , 'group')||starts_with($content->type , 'section')){
                $_form_visible = true;
            }
        }
    }
    //dd($content->type);
    if(!empty($content->type)){
        if(starts_with($content->type,'group')){
            $list_type=explode("-",$menu->type);
            $content_type=explode("-",$content->type);
            if($content_type[0]=='section'){
               $menu_type=$content->type;
            }
            elseif($list_type[1]!=$content_type[1]){
                $menu_type=$list_type[0]."-".$content_type[1];
            }
            elseif($content_type[0]=='section'){
               $menu_type=$content->type;
            }
            else{
               $menu_type =$menu->type;
            }

        }
        else{
            $menu_type=$content->type;
 
        }
    }
    //dd($menu_type);
@endphp

                    @if ($_form_visible)

                        @if(!empty(config('webshell.menu_types.'.$menu_type.'.content')))
                            @foreach (config('webshell.menu_types.'.$menu_type.'.content') as $key => $value)
                                   
                                @if($value['type']=='text'||$value['type']=='number'||$value['type']=='datetime-local')
                                    @include('formpartials.input')
                                @endif
                                @if($value['type']=='select')
                                    @include('formpartials.select')
                                @endif   
                                @if($value['type']=='HTML')
                                    @include('formpartials.html')
                                @endif 
                                @if($value['type']=='photo')
                                    @include('formpartials.photo')
                                @endif
                                @if($value['type']=='tiynmce')
                                    @include('formpartials.tinymce')
                                @endif 
                                @if($value['type']=='date')
                                    @include('formpartials.date')
                                @endif    

                           
                            @endforeach
                        @endif
                    @endif

                    