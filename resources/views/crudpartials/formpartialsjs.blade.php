@if(!empty(config('webshell.menu_types.'.$menu->type.'.content')))
    @foreach (config('webshell.menu_types.'.$menu->type.'.content') as $key => $value)
        @if($value['type']=='tiynmce')
            @include('formpartials.tinymcejs')
        @endif     
    @endforeach
@endif
@if(!empty(config('webshell.menu_types.'.$menu->type.'.content')))
    @foreach (config('webshell.menu_types.'.$menu->type.'.content') as $key => $value)           
        @if($value['type']=='photo')
            @include('formpartials.photojs')
        @endif        
    @endforeach
@endif
