@php

    $_category_visible = false;
    $_category_value = '';
    $_category_disabled = '';
    
    // $_category_visible
    if (Request::segment(4) == 'edit' && is_null(Request::segment(6))){
        if (starts_with($content->type ,'group') || $content->type == 'photogallery'){
            $_category_visible = true;
        }
    }

    // $_category_value
    

    // $_category_disabled
    if (Request::segment(4) == 'delete'){
        $_category_disabled = ' disabled="disabled" ';
    }
    //dd(json_decode($menu->topHasSub->first()->props)->catbarvisible);
    function topcats($cths ,$catnames){
        if(!empty($cths->topCategoryVariable)){
            $catnames[]=$cths->topCategoryVariable->title;
        }
        if($cths->top_category_id!=null){
            $catnames=array_merge(topcats($cths->topCat,$catnames));
        }
        return array_unique($catnames);
    }
    $array=array();
   // dd(implode(" | ",topcats($categories[6],$array)));
                            
@endphp

@if ($_category_visible)
    <div class="form-group m-form__group row @if ($errors->has('category')) has-danger @endif">
        <label for="example-text-input" class="col-2 col-form-label">
            Kategori
        </label>
        <div class="col-7">
            @if ($errors->has('category'))
                <div id="category-error" class="form-control-feedback">{{ $errors->first('category') }}</div>
            @endif
            <select class="form-control m-select2" id="category" name="category[]" multiple="multiple" {!! $_category_disabled !!} >
                <option value="null">Seçiniz...</option>

                @foreach ($categories as $cths)
                   @if($cths->subCat->count()== 0)
                        <option value="{{ $cths->id }}" @if ($content->contentHasCategory()->where('category_id', $cths->id)->count()) {{ 'selected' }} @endif> {{ $cths->variableLang($langs->first()->code)->title }}
                            @if(!empty($cths->topCategoryVariable))
                                [@php
                                    print_r(implode(" | ",topcats($cths,$array)));
                                @endphp
                                ] 
                            @endif
                        </option>
                    @endif
                @endforeach
                
            </select>
        </div>
    </div>
@endif