@if (Request::segment(4) == 'edit' && is_null(Request::segment(6)))
@php
//dd($ths);
@endphp
<hr>
    @if(!starts_with($content->type,"section"))
        <div class="form-group m-form__group row @if ($errors->has('props_section')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Bölüm
            </label>
            <div class="col-7">
                @if ($errors->has('props_section'))
                <div id="props_section-error" class="form-control-feedback">{{ $errors->first('props_section') }}</div>
                @endif
                <select class="form-control m-select2" id="props_section" name="props_section"
                @if (Request::segment(4) == 'delete')
                disabled="disabled" 
                @endif
                >
                @foreach (config('webshell.props.default.section') as $key => $sec)
                <option value="{{ $sec['value'] }}" 
                @if (!empty(json_decode($ths->props)->props_section))
                @if (!empty($content) && json_decode($ths->props)->props_section == $sec['value']) 
                {{ 'selected' }} 
                @endif
                @endif
                > {{ $sec['name'] }} </option>
                @endforeach
            </select>
        </div>
        </div>
    @endif
    @if(starts_with($content->type,"section"))
        <div class="form-group m-form__group row @if ($errors->has('section_props_section')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Bölüm
            </label>
            <div class="col-7">
                @if ($errors->has('section_props_section'))
                <div id="section_props_section-error" class="form-control-feedback">{{ $errors->first('section_props_section') }}</div>
                @endif
                <select class="form-control m-select2" id="section_props_section" name="section_props_section"
                @if (Request::segment(4) == 'delete')
                disabled="disabled" 
                @endif
                >
                @foreach (config('webshell.props.default.section') as $key => $sec)
                <option value="{{ $sec['value'] }}" 
                @if (!empty(json_decode($ths->props)->section_props_section))
                @if (!empty($content) && json_decode($ths->props)->section_props_section == $sec['value']) 
                {{ 'selected' }} 
                @endif
                @endif
                > {{ $sec['name'] }} </option>
                @endforeach
            </select>
        </div>
        </div>
    @endif        

<div class="form-group m-form__group row @if ($errors->has('props_colortheme')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Renk Teması
    </label>
    <div class="col-7">
        @if ($errors->has('props_colortheme'))
        <div id="props_colortheme-error" class="form-control-feedback">{{ $errors->first('props_colortheme') }}</div>
        @endif
        <select class="form-control m-select2" id="props_colortheme" name="props_colortheme"
        @if (Request::segment(4) == 'delete')
        disabled="disabled" 
        @endif
        >
        @foreach (config('webshell.props.default.colortheme') as $key => $cth)
        <option value="{{ $cth['value'] }}" 
        @if (!empty(json_decode($ths->props)->props_colortheme))
        @if (!empty($content) && json_decode($ths->props)->props_colortheme == $cth['value']) 
        {{ 'selected' }} 
        @endif
        @endif
        > {{ $cth['name'] }} </option>
        @endforeach
    </select>
</div>
</div>
    @if(!starts_with($content->type,"section"))

        <div class="form-group m-form__group row @if ($errors->has('props_colvalue')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Sütun Genişliği
            </label>
            <div class="col-7">
                @if ($errors->has('props_colvalue'))
                <div id="props_colvalue-error" class="form-control-feedback">{{ $errors->first('props_colvalue') }}</div>
                @endif
                <select class="form-control m-select2" id="props_colvalue" name="props_colvalue"
                @if (Request::segment(4) == 'delete')
                disabled="disabled" 
                @endif
                >
                @foreach (config('webshell.props.default.colvalue') as $key => $colv)
                <option value="{{ $colv['value'] }}" 
                @if (!empty(json_decode($ths->props)->props_colvalue))
                @if (!empty($content) && json_decode($ths->props)->props_colvalue == $colv['value']) 
                {{ 'selected' }} 
                @endif
                @endif
                > {{ $colv['name'] }} </option>
                @endforeach
            </select>
        </div>
        </div>
    @endif
@php
//dd($menu->type);
@endphp


@if ($content->type == 'form')

<hr>
<div class="form-group m-form__group row @if ($errors->has('props_eposta')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Bilgilendirme E-Postası
    </label>
    @php 
    $props_eposta_val = ''; 
    @endphp
    <div class="col-7">
        <input class="form-control m-input" type="text" id="props_eposta" name="props_eposta" 
        @if (Request::segment(4) != 'add' && empty(old('props_eposta')))
        @if(is_null($ths->props))
        @if(!empty(json_decode($ths->props)->props_eposta))
        @php 
        $props_eposta_val = json_decode($ths->props)->props_eposta; 
        @endphp
        @endif
        @else
        @if(!empty(json_decode($ths->props)->props_eposta))
        @php 
        $props_eposta_val = json_decode($ths->props)->props_eposta;
        @endphp
        @endif
        @endif
        value="{{ $props_eposta_val }}"
        @else 
        value="{{ old('props') }}" 
        @endif

        @if (Request::segment(4) == 'delete')
        disabled="disabled" 
        @endif
        >
        @if ($errors->has('props_eposta'))
        <div id="props_eposta-error" class="form-control-feedback">{{ $errors->first('props_eposta') }}</div>
        @endif
    </div>
</div>

<div class="form-group m-form__group row @if ($errors->has('props_buttonname')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Buton Adı
    </label>
    @php 
    $props_buttonname_val = ''; 
    @endphp
    <div class="col-7">
        <input class="form-control m-input" type="text" id="props_buttonname" name="props_buttonname" 
        @if (Request::segment(4) != 'add' && empty(old('props_buttonname')))
        @if(is_null($ths->props))
        @if(!empty(json_decode($ths->props)->props_buttonname))
        @php 
        $props_buttonname_val = json_decode($ths->props)->props_buttonname; 
        @endphp
        @endif
        @else
        @if(!empty(json_decode($ths->props)->props_buttonname))
        @php 
        $props_buttonname_val = json_decode($ths->props)->props_buttonname;
        @endphp
        @endif
        @endif
        value="{{ $props_buttonname_val }}"
        @else 
        value="{{ old('props_buttonname') }}" 
        @endif

        @if (Request::segment(4) == 'delete')
        disabled="disabled" 
        @endif
        >
        @if ($errors->has('props_buttonname'))
        <div id="props_buttonname-error" class="form-control-feedback">{{ $errors->first('props_buttonname') }}</div>
        @endif
    </div>
</div>

<div class="m-form__group form-group row">
    <label for="example-text-input" class="col-2 col-form-label">
        Yorumları Gizle / Göster
    </label>
    <div class="col-3">
        <span class="m-switch">
            <label>
                <input type="checkbox" id="props_comment_status" name="props_comment_status" value="active" @if(json_decode($ths->props)->props_comment_status == 'active') checked="checked" @endif />
                <span></span>
            </label>
        </span>
    </div>
</div>


@if(json_decode($ths->props)->props_comment_status == 'active')

@if(is_null($content->variableLang(Request::segment(6)))) 
@php
$formElements = json_decode($content->variableLang($langs->first()->code)->content);
@endphp
@else 
@php
$formElements = json_decode($content->variableLang(Request::segment(6))->content);
@endphp
@endif

<div class="m-form__group form-group row">
    <label class="col-2 col-form-label">Gösterilecek Alanlar</label>
    <div class="col-7">
        <div class="m-checkbox-list">
            @php
                            //dump($formElements);
            @endphp
            @foreach($formElements as $formElement)
            @if($formElement->type == 'text' || $formElement->type == 'textarea' || $formElement->type == 'date' || $formElement->type == 'number' || $formElement->type == 'checkbox-group' || $formElement->type == 'radio-group' || $formElement->type == 'select') 
            <label class="m-checkbox m-checkbox--state-primary">

                @php
                $props_visible_elemenets = json_decode($ths->props, true)['props_visible_elemenets'];
                $checkedValue = "";

                if(isset($props_visible_elemenets)){
                    foreach ($props_visible_elemenets as $value) {
                        if($value == $formElement->name){
                            $checkedValue = 'checked="checked"';
                        }
                    }
                }
                @endphp

                <input type="checkbox" name="formelements[]" value="{{ $formElement->name }}" {{ $checkedValue }}> 
                @if(!empty($formElement->placeholder))
                {{ $formElement->placeholder }}
                @elseif(!empty($formElement->label))
                {{ $formElement->label }}
                @endif
                <span></span>

            </label>
            @endif
            @endforeach
        </div>
    </div>
</div>

@endif


@endif

@if (starts_with($content->type , 'section'))
@php
//dd(json_decode($ths->props));
@endphp

    <div class="form-group m-form__group row @if ($errors->has('props_fullwidth')) has-danger @endif">
        <label for="example-text-input" class="col-2 col-form-label">Tip</label>
        <div class="col-7">
            <div class="m-radio-inline">
                <label class="m-radio m-radio--solid m-radio--state-brand">
                    <input type="radio" name="props_fullwidth" value="Full-Width" @if (json_decode($ths->props)->props_fullwidth=="Full-Width" ) {{ 'checked="checked"' }} @endif> Full-Width
                    <span></span>
                </label>

                <label class="m-radio m-radio--solid m-radio--state-brand">
                    <input type="radio" name="props_fullwidth" value="Container" @if (json_decode($ths->props)->props_fullwidth=="Container" ) {{ 'checked="checked"' }} @endif> Container
                    <span></span>
                </label>
            </div>
        </div>
        @if ($errors->has('props_fullwidth'))
            <div id="props_fullwidth-error" class="form-control-feedback">{{ $errors->first('props_fullwidth') }}</div>
        @endif
    </div>

    <div class="form-group m-form__group row @if ($errors->has('bg_img')) has-danger @endif">
        <label for="example-text-input" class="col-2 col-form-label">background image</label>
        <div class="col-7">
            <div class="m-radio-inline">
                <label class="m-radio m-radio--solid m-radio--state-brand">
                    <input type="radio" name="bg_img" value="false" @if (json_decode($ths->props)->bg_img=="false" ) {{ 'checked="checked"' }} @endif> yok
                    <span></span>
                </label>

                <label class="m-radio m-radio--solid m-radio--state-brand">
                    <input type="radio" name="bg_img" value="true" @if (json_decode($ths->props)->bg_img=="true" ) {{ 'checked="checked"' }} @endif> var
                    <span></span>
                </label>
            </div>
        </div>
        @if ($errors->has('bg_img'))
            <div id="bg_img-error" class="form-control-feedback">{{ $errors->first('bg_img') }}</div>
        @endif
    </div>    


@endif
@if ($content->type == 'seperator')

<hr>
<div class="form-group m-form__group row @if ($errors->has('props_type')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Tip
    </label>
    <div class="col-7">
        @if ($errors->has('props_type'))
        <div id="props_type-error" class="form-control-feedback">{{ $errors->first('props_type') }}</div>
        @endif
        <select class="form-control m-select2" id="props_type" name="props_type"
        @if (Request::segment(4) == 'delete')
        disabled="disabled" 
        @endif
        >
        @foreach (config('webshell.props.content.seperator') as $key => $sep)
        <option value="{{$sep['value']}}" 
        @if (!empty(json_decode($ths->props)->props_type))
        @if (!empty($content) && json_decode($ths->props)->props_type == $sep['value']) 
        {{ 'selected' }} 
        @endif
        @endif
        > {{$sep['name']}} </option>
        @endforeach

    </select>
</div>
</div>

@endif

@if ($content->type == 'photo')
  

<hr>
<div class="form-group m-form__group row @if ($errors->has('props_type')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Gösterim Türü
    </label>
    <div class="col-7">
        @if ($errors->has('props_type'))
        <div id="props_type-error" class="form-control-feedback">{{ $errors->first('props_type') }}</div>
        @endif
        <select class="form-control m-select2" id="props_type" name="props_type"
        @if (Request::segment(4) == 'delete')
        disabled="disabled" 
        @endif
        >
        @foreach (config('webshell.props.content.photo.type') as $key => $conf)
        <option value="{{ config('webshell.props.content.photo.type.'.$key.'.value') }}" 
        @if (!empty(json_decode($ths->props)->props_type))
        @if (!empty($content) && json_decode($ths->props)->props_type == config('webshell.props.content.photo.type.'.$key.'.value'))
        {{ 'selected' }}
        @endif
        @endif
        > {{config('webshell.props.content.photo.type.'.$key.'.label')}} </option>
        @endforeach

    </select>
</div>
</div>



@endif

@if ($content->type == 'photogallery')

<hr>
<div class="form-group m-form__group row @if ($errors->has('props_type')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Kolon Sayısı
    </label>
    <div class="col-7">
        @if ($errors->has('props_type'))
        <div id="props_type-error" class="form-control-feedback">{{ $errors->first('props_type') }}</div>
        @endif
        <select class="form-control m-select2" id="props_type" name="props_type"
        @if (Request::segment(4) == 'delete')
        disabled="disabled" 
        @endif
        >

        @foreach (config('webshell.props.content.photogallery.type') as $key => $conf)
        <option value="{{ config('webshell.props.content.photogallery.type.'.$key.'.value') }}" 
        @if (!empty(json_decode($ths->props)->props_type))
        @if (!empty($content) && json_decode($ths->props)->props_type == config('webshell.props.content.photogallery.type.'.$key.'.value')) 
        {{ 'selected' }} 
        @endif
        @endif
        > {{config('webshell.props.content.photogallery.type.'.$key.'.label')}} </option>
        @endforeach

    </select>
</div>
</div>
<div class="form-group m-form__group row @if ($errors->has('tag')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">Gösterim Şekli</label>
   
    <div class="col-7">
        <div class="m-radio-inline">
            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="display_type" value="horizontal" @if (json_decode($ths->props)->display_type== 'horizontal') {{ 'checked="checked"' }} @endif >Yatay
                <span></span>
            </label>
            <label class="m-radio m-radio--solid m-radio--state-brand">
                <input type="radio" name="display_type" value="vertical" @if (json_decode($ths->props)->display_type== 'vertical') {{ 'checked="checked"' }} @endif >Dikey
                <span></span>
            </label>
     
        </div>
    </div>
    @if ($errors->has('display_type'))
    <div id="display_type-error" class="form-control-feedback">{{ $errors->first('display_type') }}</div>
    @endif
</div>

@endif

@if ($content->type == 'group')

@if(!empty(config('webshell.props.menu.'.$menu->type)))
@foreach (config('webshell.props.menu.'.$menu->type) as $key => $field)
@if($field['type'] == 'number')
<div class="form-group m-form__group row @if ($errors->has('props_'.$key)) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        {{ $field['name'] }}
    </label>
    <div class="col-7">
        <input class="form-control m-input" type="{{ $field['type'] }}" id="{{'props_'.$key}}" name="{{'props_'.$key}}" step="0.01" value="@if(!empty(json_decode($ths->props, true)['props_'.$key])){{ json_decode($ths->props, true)['props_'.$key] }}@endif">
        @if ($errors->has('props_'.$key))
        <div id="title-error" class="form-control-feedback">{{ $errors->first('props_'.$key) }}</div>
        @endif
    </div>
</div> 
@endif

@if($field['type'] == 'radio')

<div class="m-form__group form-group row">
    <label class="col-2 col-form-label">{{ $field['name'] }}</label>
    <div class="col-7">
        <div class="m-radio-inline">

            @foreach($field['value'] as $key_y => $value_e)
            <label class="m-radio">
                <input type="radio" name="{{'props_'.$key}}" value="{{ $key_y }}" 
                @if(!empty(json_decode($ths->props, true)['props_'.$key])) 
                @if(json_decode($ths->props, true)['props_'.$key] == $key_y) 
                checked="checked" 
                @endif
                @endif
                > {{ $value_e }}
                <span></span>
            </label>
            @endforeach

        </div>
        <!-- <span class="m-form__help"> </span> -->
    </div>
</div>

@endif

@endforeach
@endif
@endif


@endif

@if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))



@endif