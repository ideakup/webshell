@php

    $_content_visible = false;
    $_content_value = '';
    $_content_disabled = '';
    
    // $_content_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){
    	if (starts_with($content->type , 'text')){
    		$_content_visible = true;
    	}else if ($content->type == 'link'){
            $_content_visible = true;
        }else if ($content->type == 'video'){
            $_content_visible = true;    
        }else if ($content->type == 'rssfeed'){
            $_content_visible = true;
        }
    }
    
    // $_content_value
    if (Request::segment(4) != 'add' && empty(old('content'))) {
		if (is_null($content->variableLang(Request::segment(6)))) {
			$_content_value = $content->variableLang($langs->first()->code)->content;
		} else {
			$_content_value = $content->variableLang(Request::segment(6))->content;
		}   
	} else {
		$_content_value = old('content');
	}

    // $_content_disabled
    if (Request::segment(4) == 'delete'){
        $_content_disabled = ' disabled="disabled" ';
    }
//dd($_content_value);

@endphp

@if ($_content_visible)

	@if (starts_with($content->type , 'text'))

		<div class="form-group m-form__group" style="padding-bottom: 0;">
		    <label>İçerik</label>
		</div>
        @php
        @endphp
		<div class="form-group m-form__group @if ($errors->has('content')) has-danger @endif">
		    <textarea class="form-control m-input" id="content" name="content" rows="3">{!! $_content_value !!}</textarea>

		    @if ($errors->has('content'))
		        <div id="content-error" class="form-control-feedback">{{ $errors->first('content') }}</div>
		    @endif
		</div>

	@endif

    @if ($content->type == 'rssfeed')
        <hr>
        <div class="form-group m-form__group row @if ($errors->has('content_url')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                RSS Feed Adresi
            </label>
            <div class="col-7">
                <input class="form-control m-input" type="text" id="content_url" name="content_url" value="{{ (empty(json_decode($_content_value)->content_url)) ? '' : json_decode($_content_value)->content_url }}" {!! $_content_disabled !!} required>
                
                @if ($errors->has('content_url'))
                    <div id="content_url-error" class="form-control-feedback">{{ $errors->first('content_url') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group m-form__group row @if ($errors->has('content_rowcount')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Satır Sayısı
            </label>
            <div class="col-7">
                <input class="form-control m-input" type="number" min="1" max="50" id="content_rowcount" name="content_rowcount" value="{{ (empty(json_decode($_content_value)->content_rowcount)) ? '' : json_decode($_content_value)->content_rowcount }}" {!! $_content_disabled !!}>
                
                @if ($errors->has('content_rowcount'))
                    <div id="content_rowcount-error" class="form-control-feedback">{{ $errors->first('content_rowcount') }}</div>
                @endif
            </div>
        </div>

    @endif

    @if ($content->type == 'link')
        
        <hr>
        <div class="form-group m-form__group row @if ($errors->has('content_url')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Link
            </label>
            <div class="col-7">
                <input class="form-control m-input" type="text" id="content_url" name="content_url" value="{{ (empty(json_decode($_content_value)->content_url)) ? '' : json_decode($_content_value)->content_url }}" {!! $_content_disabled !!} required>
                
                @if ($errors->has('content_url'))
                    <div id="content_url-error" class="form-control-feedback">{{ $errors->first('content_url') }}</div>
                @endif
            </div>
        </div>

        <div class="m-form__group form-group row">
            <label for="example-text-input" class="col-2 col-form-label">
                Dahili / Dış Link {{ json_decode($_content_value)->content_target }}
            </label>
            <div class="col-3">
                <span class="m-switch">
                    <label>
                        <input type="checkbox" @if (json_decode($_content_value)->content_target == 'external' || Request::segment(4) == 'add') {{ 'checked="checked"' }} @endif id="content_target" name="content_target" value="external" 
                            @if (Request::segment(4) == 'delete')
                                disabled="disabled" 
                            @endif
                        />
                        <span></span>
                    </label>
                </span>
            </div>
        </div>

    @endif

    @if ($content->type == 'video')
        
        <hr>
        <div class="form-group m-form__group row @if ($errors->has('embed_code')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Embed Code
            </label>
            <div class="col-7">
                <input class="form-control m-input" type="text" id="embed_code" name="embed_code" value="{{ (empty(json_decode($_content_value)->embed_code)) ? '' : json_decode($_content_value)->embed_code }}" {!! $_content_disabled !!} required>
                
                @if ($errors->has('embed_code'))
                    <div id="embed_code-error" class="form-control-feedback">{{ $errors->first('embed_code') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group m-form__group row @if ($errors->has('type')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">Video tipi</label>
            <div class="col-7">
                <div class="m-radio-inline">
                    <label class="m-radio m-radio--solid m-radio--state-brand">
                        <input type="radio" name="type" value="youtube"@if (json_decode($_content_value)->type == "youtube") {{ 'checked="checked"' }} @endif> You tube
                        <span></span>
                    </label>
                </div>
            </div>
            @if ($errors->has('type'))
                <div id="type-error" class="form-control-feedback">{{ $errors->first('type') }}</div>
            @endif
        </div>
    @endif

@endif