@if ((Request::segment(4) == 'edit' || Request::segment(4) == 'galup') && !is_null(Request::segment(6)))
    @if ($content->type == 'code')
        
        <script type="text/javascript">
        
            tinymce.init({
                selector: 'textarea#code',

                @if (Request::segment(4) == 'delete')
                    readonly : 1,
                @endif
                
                plugins: "code",
  toolbar: "code"
            });

        </script>

    @endif
@endif

