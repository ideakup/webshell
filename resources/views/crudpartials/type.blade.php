@php

$_type_visible = false;
$_type_value = '';
$_type_disabled = '';
$list_type=explode("-",$menu->type);
//dd($menu);
    // $_type_visible
if (Request::segment(4) == 'add'){
    $_type_visible = true;
}

    // $_type_value


    // $_type_disabled
if (Request::segment(4) == 'delete'){
    $_type_disabled = ' disabled="disabled" ';
}else if(is_null($menu) && Request::segment(3) == 0) {
    $_type_disabled = ' disabled="disabled" ';
}
@endphp


@if ($_type_visible)

@if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')

<input type="hidden" name="type" value="{{ Request::segment(1) }}">

@else

<div class="form-group m-form__group row @if ($errors->has('type')) has-danger @endif">
    <label for="example-text-input" class="col-2 col-form-label">
        Tip
    </label>
    <div class="col-7">
        @if ($errors->has('type'))
        <div id="type-error" class="form-control-feedback">{{ $errors->first('type') }}</div>
        @endif

        <select class="form-control m-select2" id="type" name="type" {!! $_type_disabled !!} >

            <option value="null">Seçiniz...</option>

            @if (is_null($content) && starts_with($menu->type, 'list'))
        
                <option value="group-{{$list_type[1]}}">İçerik Grubu [{{$menu->variable->name}}] </option>
                <option value="section-video">Play-List(video) </option>
                <option value="section"> Section </option>
                    
            @elseif(starts_with($menu->type, 'list') && starts_with($content->type,"section-")) 
                @php
                    $content_type=explode("-",$content->type);
                @endphp  
                <option value="group-{{$content_type[1]}}">İçerik Grubu [{{$content->variable->title}}] </option>
            @else
                <option value="text"> Metin & HTML </option>
                <option value="photo"> Fotoğraf </option>
                <option value="photogallery"> Foto Galeri </option>
                <option value="link"> Button & Link </option>
                <option value="video"> Video</option>
                <option value="slide"> Slide </option>
                <option value="seperator"> Seperatör </option>
                <option value="mapturkey"> Türkiye Haritası </option>
                <option value="form" @if(is_null($menu) && Request::segment(3) == 0) selected @endif > Form </option>
               <!-- <option value="rssfeed"> RSS Feed </option>-->
                <option value="code"> Code </option>
                <option value="section"> Section </option>
                <option value="section-video">Play-List(video) </option>
                <option value="section-audio">Play-List(audio) </option>
                

            @endif
                    <!--
                        <option value="youtube"> Video (Youtube) </option>
                        <option value="vimeo"> Video (Vimeo) </option>
                        <option value="file"> Dosya </option>
                        <option value="audio"> Ses </option>
                        <option value="code"> Kod </option>
                    -->
                </select>
                
            </div>
        </div>

        @endif

        @endif