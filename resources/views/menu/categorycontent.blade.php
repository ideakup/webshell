
@extends('layouts.webshell')

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Kategori
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       {{$cat_Variable->title}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ url('tag') }}/add" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="fa fa-plus"></i>
                                <span>
                                    Yeni Etiket Ekle
                                </span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="tagDataTable">
                <thead>
                    <tr>
                    	<th style="width: 50px;">
                            NO
                        </th>
                        <th>
                            {{$cat_Variable->title}} Etiketi Altındakiler
                        </th>
                    </tr>
                </thead>
                <tbody>  	
            		@foreach($cat_content as $index =>$c_c)
                		<tr>
                			<td>
	                			{{$index+1}}
	                		</td>
	                		<td>
	                			{{$c_c->title}}
	                		</td>
	                	</tr>	
            		@endforeach	
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection