@php
//dd($menuroots);
@endphp
@foreach (config('webshell.menu_types') as $key )
@php
//dd($key);
@endphp

@endforeach
@extends('layouts.webshell')

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    @php
                    //dd($menuroots);
                    @endphp
                    @foreach($menuroots as $menuroot)
                        @if($menuroot->sub_model=='Menu')
                            
                        @else  
                            @if($menuroot->top_model=='Menu')
                                <li class="m-nav__separator"> - </li>
                                <li class="m-nav__item">
                                    <a href="{{ url('menu/content/'.$menu->topHasSub->first()->id) }}" class="m-nav__link">
                                        <span class="m-nav__link-text">
                                            {{ $menuroot->topElementForMenu->variable->name}}   
                                        </span>
                                    </a>                            
                                </li>
                            @else    
                                <li class="m-nav__separator"> - </li>
                                <li class="m-nav__item">
                                    <a href="{{ url('menu/content/'.$menu->topHasSub->first()->id.'/subcontent/'.$menuroot->topThs->id) }}" class="m-nav__link">
                                        <span class="m-nav__link-text">
                                            {{ $menuroot->topElementForContent->variable->title}}   
                                        </span>
                                    </a>                            
                                </li>
                            @endif    
                        @endif     
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            @if(is_null($content))
                                {{ $menu->variableLang($langs->first()->code)->name }}
                                @if ($menu->type == 'menuitem')
                                    <span class="m-badge m-badge--brand m-badge--wide">Menü Öğesi (İçerik Yok)</span>
                                @elseif ($menu->type == 'content')
                                    <span class="m-badge m-badge--brand m-badge--wide">İçerik</span>
                                @elseif($menu->type == 'photogallery')
                                    <span class="m-badge m-badge--success m-badge--wide">Foto Galeri</span>
                                @elseif(starts_with($menu->type, 'list'))
                                    <span class="m-badge m-badge--success m-badge--wide">Liste</span>
                                @elseif($menu->type == 'link')
                                    <span class="m-badge m-badge--success m-badge--wide">Link (Yönlendirme)</span>
                                @endif
                            @else
                                {{ $content->variableLang($langs->first()->code)->title }}
                                <span class="m-badge m-badge--metal m-badge--wide">
                                    @if ($content->type == 'group')
                                        İçerik Grubu
                                    @elseif ($content->type == 'text')
                                        Metin & HTML
                                    @elseif ($content->type == 'photo')
                                        Fotoğraf
                                    @elseif ($content->type == 'photogallery')
                                        Foto Galeri
                                    @elseif ($content->type == 'link')
                                        Button & Link
                                    @elseif ($content->type == 'slide')
                                        Slide
                                    @elseif ($content->type == 'form')
                                        Form
                                    @elseif ($content->type == 'seperator')
                                        Seperatör
                                    @elseif ($content->type == 'code')
                                        Code
                                    @endif
                                </span>
                            @endif
                            
                            
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}/add{{ (!is_null($content)) ? '/'.Request::segment(5) : '' }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="fa fa-plus"></i>
                                    <span>
                                        Yeni İçerik Ekle
                                    </span>
                                </span>
                            </a>
                          
                            @if (!starts_with($menu->type, 'list'))
                             <!--   <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}/addexist/@if(!is_null($content) && $content->type=='section'){{ Request::segment(5) }} @endif " class="btn btn-info m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="fa fa-th-large"></i>
                                        <span>
                                            Mevcut İçerik Ekle
                                        </span>
                                    </span>
                                </a>-->
                            @elseif (Request::segment(4) == 'subcontent')
                                <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}/addexist/{{ Request::segment(5) }}" class="btn btn-info m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="fa fa-th-large"></i>
                                        <span>
                                            Mevcut İçerik Ekle
                                        </span>
                                    </span>
                                </a>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <input type="hidden" name="menu_id" id="menu_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="content_id" id="content_id" value="{{ Request::segment(5) }}">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="contentDataTable">
                    <thead>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th width="30">
                                Sıra
                            </th>
                            <th>
                                Adı
                            </th>
                            <th>
                                Slug
                            </th>
                            <th>
                                Tip
                            </th>
                            <th width="50">
                                Durum
                            </th>
                            <th width="150">
                                İşlemler
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    
@endsection

@section('inline-scripts')
<script type="text/javascript">
    
    $(document).ready(function(){
        var color=['#C9F7F5','#FFE2E5','#FFF4DE','#E1F0FF','#D1D3E0','#EEE5FF '];
        var table = $('#contentDataTable').DataTable({
            responsive: true,
            dom: `<'row'<'col-sm-12'tr>> <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ ",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
            },
            searching: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "/getContent", // ajax source
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('#token').val(), 'MENU-ID': $('#menu_id').val() },
                data: {
                    // parameters for custom backend script demo
                    menuId: $('#menu_id').val(),
                    contentId: $('#content_id').val(),
                    columnsDef: ['id', 'order', 'title', 'slug', 'type', 'status', 'actions'],
                },
            },
            columns: [
                {name: 'id'},
                {name: 'order'},
                {name: 'title'},
                {name: 'slug'},
                {name: 'type'},
                {name: 'status'},
                {name: 'actions'},
            ],
            columnDefs: [
                {
                    targets: 0,
                    orderable: false,
                    visible: false,
                    
                },
                {
                    targets: 4,
                    render: function(data, type, full, meta) {
                        
                        

                        $(table.row(meta.row).node()).css("background-color",color[full[8]%color.length ]);



                   

                        var splitedData = '';
                        var ctypeKey = '';
                        var atypeKey = '';

                        if(data.search("##") >= 0){
                            splitedData = data.split("##");
                            ctypeKey = splitedData[0];
                            atypeKey = splitedData[1];
                        }else{
                            ctypeKey = data;
                            atypeKey = '';
                        }

                        var ctype = {
                            @foreach (config('webshell.menu_types') as $key )
                                '{{$key['value']}}': {'title': '{{$key['title']}}'+'['+full[7]+']', 'class': ' m-badge--{{$key['m-badge']}}'},
                            @endforeach
                            'section': {'title': 'SECTION', 'class': 'm-badge--secondary'}, 
                            'group-product': {'title': 'İçerik Grubu[Ürün]', 'class': 'm-badge--primary'},
                            'group-video': {'title': 'İçerik Grubu[Video]', 'class': 'm-badge--primary'},
                            'group-audio': {'title': 'İçerik Grubu[Audio]', 'class': 'm-badge--primary'},
                            'group-default': {'title': 'İçerik Grubu[Blog]', 'class': 'm-badge--focus'},
                            'group-show': {'title': 'İçerik Grubu[Gösteri]', 'class': 'm-badge--focus'},
                            'group-event': {'title': 'İçerik Grubu[Etkinlik]', 'class': 'm-badge--danger'},
                            'group-photo': {'title': 'İçerik Grubu[foto-link]', 'class': 'm-badge--danger'},
                            'group-team': {'title': 'İçerik Grubu[Ekip]', 'class': 'm-badge--danger'},


                            'text': {'title': 'Metin & HTML', 'class': 'm-badge--accent'},
                            'text-product': {'title': 'Metin & HTML[Ürün]', 'class': 'm-badge--primary'},

                            'photo': {'title': 'Fotoğraf', 'class': ' m-badge--brand'},
                            'photogallery': {'title': 'Foto Galeri', 'class': ' m-badge--info'},
                            'photogallery-product': {'title': 'Foto Galeri[Ürün]', 'class': 'm-badge--primary'},

                            'link': {'title': 'Button & Link', 'class': ' m-badge--danger'},
                            'video': {'title': 'Video', 'class': ' m-badge--danger'},

                            'form': {'title': 'Form', 'class': ' m-badge--success'},
                            'seperator': {'title': 'Seperatör', 'class': ' m-badge--success'},
                            'slide': {'title': 'Slide', 'class': ' m-badge--success'},
                            'mapturkey': {'title': 'Harita', 'class': ' m-badge--success'},
                            'rssfeed': {'title': 'RSS Feed', 'class': ' m-badge--success'},
                            'code': {'title': 'Code', 'class': ' m-badge--success'}
                        };

                        var atype = {
                            'content': {'title': 'İçerik', 'class': 'm-badge--warning'},
                            'menu': {'title': 'Menü', 'class': 'm-badge--danger'}
                        };
                        
                        if (typeof ctype[ctypeKey] === 'undefined') {
                            return data;
                        }
                        
                        var returnVal = '';
                        if(atypeKey != ''){
                            returnVal = '<span class="m-badge ' + atype[atypeKey].class + ' m-badge--wide">' + atype[atypeKey].title + '</span> ';
                        }
                        returnVal += '<span class="m-badge ' + ctype[ctypeKey].class + ' m-badge--wide">' + ctype[ctypeKey].title + '</span>';
                        
                        return returnVal
                        
                    },
                },
                {
                    targets: -2,
                    render: function(data, type, full, meta) {
                        var status = {
                            'active': {'title': 'Aktif', 'class': 'm-badge--brand'},
                            'passive': {'title': 'Pasif', 'class': ' m-badge--metal'}
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="m-badge ' + status[data].class + ' m-badge--wide">' + status[data].title + '</span>';
                    },
                },
                {
                    targets: -1,
                    title: 'İşlemler',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        console.log(full)
                        
                        if(full[4].search("##") >= 0){
                            isplitedData = full[4].split("##");
                            ictypeKey = isplitedData[0];
                            iatypeKey = isplitedData[1];
                        }else{
                            ictypeKey = full[4];
                            iatypeKey = '';
                        }
                        //console.log(ictypeKey+"-"+iatypeKey);
                        var editButtons = '';
                        @foreach ($langs as $lang)
                            editButtons += `<a href="{{ url('menu/content') }}/{{ $menu_new_ths->id }}/edit/`+full[0]+`/{{ $lang->code }}" class="m-portlet__nav-link btn btn-sm m-btn btn-outline-brand m-btn--pill" title="Düzenle {{ $lang->code }}">
                                {{ $lang->code }}
                            </a> `;
                        @endforeach


                        @if(!is_null(Request::segment(4)) && !is_null($content))
                            if(iatypeKey == 'content'){

                                var propButtons = `<a href="{{ url('menu/content') }}/{{ $menu_new_ths->id }}/edit/`+full[0]+`?tcid={{$content->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Ayarlar">
                                  <i class="fa fa-cogs"></i>
                                </a>`;

                            }else if(iatypeKey == 'menu'){

                                var propButtons = `<a href="{{ url('menu/edit') }}/`+full[0]+`?tcid={{$content->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Ayarlar">
                                  <i class="fa fa-cogs"></i>
                                </a>`;

                            }

                            
                        @else
                            if(iatypeKey == 'content'){
                                var propButtons = `<a href="{{ url('menu/content') }}/{{ $menu_new_ths->id }}/edit/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Ayarlar">
                                  <i class="fa fa-cogs"></i>
                                </a>`;

                            }else if(iatypeKey == 'menu'){
                                var propButtons = `<a href="{{ url('menu/edit') }}/`+full[0]+`?tmid=`+{{ $menu_new_ths->id }}+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Ayarlar">
                                  <i class="fa fa-cogs"></i>
                                </a>`;
                            }
                        @endif
                        

                        var subContentButtons = '';


                        
                        if(ictypeKey.startsWith("section")){
                             subContentButtons = `<a href="{{ url('menu/content') }}/{{ $menu_new_ths->id }}/add/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="İçerik Ekle">
                                      <i class="fa fa-plus"></i>
                                    </a>`;
                         }
                          if(ictypeKey.startsWith("group")){
                            @foreach (config('webshell.menu_types') as $key )
                                @if($key['value']==$menu->type)
                                    @if($key['directed'])
                                    subContentButtons = `<a href="{{ url('menu/content') }}/{{ $menu_new_ths->id }}/add/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="İçerik Ekle">
                                      <i class="fa fa-plus"></i>
                                    </a>`;
                                    @endif
                                @endif
                            @endforeach 
                         }
                         if(ictypeKey == 'group-audio'){
                            subContentButtons = ``;
                         }
                         
                         console.log(ictypeKey);
                        var section_bg_image="";
                         if(full[6]=="true"){
                             section_bg_image = `<a href="{{ url('menu/content') }}/{{ $menu_new_ths->id }}/sectionbgimage/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="İçerikler">
                              <i class="fa fa-image"></i>
                            </a>`;
                         }

                        var mapButtons = '';
                        if(ictypeKey == "mapturkey"){
                            mapButtons += `<a href="{{ url('menu/content') }}/{{ $menu_new_ths->id }}/mapmarker/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Harita İşaretleri">
                              <i class="fa fa-map-marker-alt"></i>
                            </a>`
                        }else{
                            mapButtons += ``;
                        }

                        if(iatypeKey == 'content'){
                            return editButtons + propButtons + subContentButtons + mapButtons + section_bg_image;    
                        }else if(iatypeKey == 'menu'){
                            return propButtons;
                        }
                        
                    },
                }
            ],
        });

    });
</script>
@endsection
