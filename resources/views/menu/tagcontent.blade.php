@extends('layouts.webshell')

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Etiket
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       {{$tagVariable->title}}
                    </h3>
                </div>
            </div>
            
        </div>
        <div class="m-portlet__body">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="tagDataTable">
                <thead>
                    <tr>
                    	<th style="width: 50px;">
                            NO
                        </th>
                        <th>
                            {{$tagVariable->title}} Etiketi Altındakiler
                        </th>
                    </tr>
                </thead>
                <tbody>  	
            		@foreach($tag_content as $index =>$t_c)
                		<tr>
                			<td>
	                			{{$index+1}}
	                		</td>
	                		<td>
	                			{{$t_c->title}}
	                		</td>
	                	</tr>	
            		@endforeach	
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection