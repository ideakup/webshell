@php
//dd($menus);
@endphp

@extends('layouts.webshell')

@section('content') 



	<div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        @if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')
                            <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    Formlar
                                </span>
                            </a>
                        @else
                            <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    İçerik Listesi @if(!is_null($menu))({{ $menu->variableLang($langs->first()->code)->name }})@endif
                                </span>
                            </a>
                        @endif
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            @if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')
                                Yeni Form Ekle
                            @else
                                @if (Request::segment(4) == 'add')
                                    İçerik Ekle
                                @elseif (Request::segment(4) == 'edit')
                                    @if (is_null(Request::segment(6)))
                                        İçerik Detayları
                                    @else
                                        İçerik Düzenle
                                    @endif
                                @elseif (Request::segment(4) == 'delete')
                                    İçerik Sil
                                @endif
                            @endif
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    
    
	<div class="m-content">

		<div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">

                <div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">

                            @if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')
                                Yeni Form Ekle
                            @else
                                
                                @if (Request::segment(4) == 'add')
                                    İçerik Ekle
                                @elseif (Request::segment(4) == 'edit')
                                    @if (is_null(Request::segment(6)))
                                        İçerik Detayları
                                    @else
                                        İçerik Düzenle
                                    @endif
                                @elseif (Request::segment(4) == 'delete')
                                    İçerik Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                                @elseif (Request::segment(4) == 'galup')
                                    Foto Galeri Düzenle ({{ $menu->variableLang($langs->first()->code)->name }})
                                @endif

                            @endif

						</h3>
					</div>
				</div>
                
				<div class="m-portlet__head-tools">
                    @if (Request::segment(4) == 'edit' && is_null(Request::segment(6)))
                        <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}/delete/{{Request::segment(5)}}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="İçerik Sil">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endif
				</div>

			</div>

        @if(Request::segment(4) == 'add')
            <div class="card card-custom">
             <div class="card-header card-header-tabs-line">
              
              <div class="card-toolbar">
               <ul class="nav nav-tabs nav-bold nav-tabs-line">
                <li class="nav-item">
                 <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_1_2">İçerik Ekle</a>
                </li>
                <li class="nav-item">
                 <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_2_2">Menü Özeti Ekle</a>
                </li>
                <li class="nav-item">
                 <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_3_2">İçerik Özeti Ekle</a>
                </li>
               </ul>
              </div>
             </div>
             <div class="card-body">
              <div class="tab-content">
               <div class="tab-pane fade show active" id="kt_tab_pane_1_2" role="tabpanel">
                    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/content/save') }}" id="contentForm">
                    
                        {{ csrf_field() }}
                        <input type="hidden" name="crud" value="{{ Request::segment(4) }}">
                        <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
                        <input type="hidden" name="top_content_id" value="{{ (!is_null($ths)) ? $ths->top_id : '' }}">
                        <input type="hidden" name="content_ths_id" value="{{ (!is_null($ths)) ? $ths->id : '' }}">
                        <input type="hidden" name="content_id" value="{{ (!is_null($content)) ? $content->id : '' }}">

                        <input type="hidden" name="lang" value="{{ Request::segment(6) }}">
                        <input type="hidden" id="formdata" name="formdata"> 
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                        <div class="m-portlet__body">
                            
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">

                                        @if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')
                                            Yeni Form Ekle
                                        @else

                                            @if (!is_null(Request::segment(6))) 
                                                [{{ Request::segment(6) }}] 
                                            @endif

                                            @if (Request::segment(4) == 'add')
                                                İçerik Ekle
                                            @elseif (Request::segment(4) == 'edit')
                                                @if (is_null(Request::segment(6)))
                                                    İçerik Detayları
                                                @else
                                                    İçerik Düzenle
                                                @endif
                                            @elseif (Request::segment(4) == 'delete')
                                                İçerik Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                                            @elseif (Request::segment(4) == 'galup')
                                                Galeri Düzenle
                                            @endif

                                            @if (Request::segment(4) != 'add')
                                                <small> (
                                                    @if (starts_with($content->type ,'group'))
                                                        İçerik Grubu
                                                    @elseif ($content->type == 'text')
                                                        Metin & HTML
                                                    @elseif ($content->type == 'photo')
                                                        Fotoğraf
                                                    @elseif ($content->type == 'photogallery')
                                                        Foto Galeri
                                                    @elseif ($content->type == 'link')
                                                        Button & Link
                                                    @elseif ($content->type == 'video')
                                                        Video  
                                                    @elseif ($content->type == 'slide')
                                                        Slide
                                                    @elseif ($content->type == 'form')
                                                        Form
                                                    @elseif ($content->type == 'seperator')
                                                        Seperatör
                                                    @elseif ($content->type == 'mapturkey')
                                                        Türkiye Haritası
                                                    @elseif ($content->type == 'rssfeed')
                                                        RSS Feed
                                                    @endif
                                                ) </small>
                                            @endif

                                        @endif

                                    </h3>
                                </div>
                            </div>
                     


                                @include('crudpartials.title')

                                @include('crudpartials.slug')

                                
                                @include('crudpartials.type')   

                                @include('crudpartials.order')   

                                @include('crudpartials.status')
                                
                                @include('crudpartials.tag')

                                @include('crudpartials.category')

                                @include('crudpartials.short_content')

                                @include('crudpartials.content')

                                @if(starts_with($menu->type,'list'))

                                    @include('crudpartials.content_form')

                                @endif
                                
                                @include('crudpartials.code')

                                @include('crudpartials.props')

                                @include('crudpartials.form')

                        </div>
                        @php
                        //dd($ths);
                        @endphp
                        

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-7">

                                        @if (Request::segment(4) == 'add' || Request::segment(4) == 'edit')

                                            <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                                Kaydet
                                            </button>
                                            &nbsp;&nbsp;

                                            @if(is_null($menu) && Request::segment(3) == 0)
                                                <a href="{{ url('form/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    Vazgeç
                                                </a>
                                            @else
                                                @if(starts_with($menu->type, 'list'))
                                                    @if(!is_null($ths))
                                                        @if(($ths->top_model=='Content'))
                                                           <a href="{{ url('menu/content/') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                        @else
                                                            <a href="{{ url('menu/content/') }}/{{Request::segment(3)}}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                         @endif
                                                    @else 
                                                       <a href="{{ url('menu/content/') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">   
                                                    @endif       
                                                        Vazgeç
                                                    </a>
                                                @else
                                                    @if(!empty($ths->top_id))
                                                       <a href="{{ url('menu/content/') }}/{{Request::segment(3)}}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    @else
                                                        <a href="{{ url('menu/content/') }}{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                     @endif   
                                                        Vazgeç
                                                    </a>
                                                @endif    
                                            @endif

                                        @elseif (Request::segment(4) == 'delete')

                                            <div class="alert alert-danger" role="alert">
                                                <strong> Siliyorsunuz... </strong>
                                                Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                            </div>
                                            <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                                Kaydı Sil
                                            </button>
                                            &nbsp;&nbsp;
                                            <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                Vazgeç
                                            </a>

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>

               </div>
               <div class="tab-pane fade " id="kt_tab_pane_2_2" role="tabpanel">
                    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/content/saveexist') }}" id="existContentForm">
                        {{ csrf_field() }}
                        <input type="hidden" name="crud" value="addexist">
                        <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
                        <input type="hidden" name="content_id" value="{{ Request::segment(5) }}">
                        <input type="hidden" name="lang" value="{{ Request::segment(6) }}">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="m-portlet__body">
                            
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        Mevcut İçerik Seç 
                                        (
                                            @if ($menu->type == 'menuitem')
                                                Menü Öğesi (İçerik Yok)
                                            @elseif ($menu->type == 'content')
                                                İçerik
                                            @elseif($menu->type == 'photogallery')
                                                Foto Galeri
                                            @elseif(starts_with($menu->type, 'list'))
                                                Liste
                                            @elseif($menu->type == 'link')
                                                Link (Yönlendirme)
                                            @endif
                                        )
                                    
                                    </h3>
                                </div>
                            </div>

                            <div class="form-group m-form__group row @if ($errors->has('existmenu')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Menü
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('existmenu'))
                                        <div id="existmenu-error" class="form-control-feedback">{{ $errors->first('existmenu') }}</div>
                                    @endif
                                    <select style="width: 855px;" class="form-control m-select2" id="existmenu" name="existmenu"
                                        @if (Request::segment(4) == 'delete')
                                            disabled="disabled" 
                                        @endif
                                        >
                                        <option value="null">Seçiniz...</option>

                                        @php
                                            $typeMenuText = '';
                                            $typeMenuSlug = '';
                                        @endphp

                                        @foreach ($menus as $men)

                                            @if ($men->type == 'photogallery')
                                                @php
                                                    $typeMenuText = 'Foto Galeri';
                                                @endphp
                                            @elseif ($men->type == 'menuitem')
                                                @php
                                                    $typeMenuText = 'Menü İtem';
                                                @endphp    
                                            @elseif (starts_with($men->type, 'list'))
                                                @php
                                                    $typeMenuText = 'Liste';
                                                @endphp
                                            @endif

                                            @php
                                                $menuType = explode('-',$men->type)[0];
                                               // dump( $menuType);
                                            @endphp

                                            @if ($typeMenuSlug != $menuType)
                                                @if (!$loop->first)
                                                    </optgroup>
                                                @endif
                                                <optgroup label="{{ $typeMenuText }}">
                                                    
                                                @php 
                                                    if(starts_with($men->type, 'list')){
                                                        $typeMenuSlug = 'list';
                                                    }else{
                                                        //$typeMenuSlug = $men->type;
                                                    }
                                                @endphp
                                            @endif
                                            
                                            <option value="{{ $men->menu_id }}"> {{ $men->menutitle }} 
                                                @if(starts_with($men->type, 'list'))
                                                    -- {{ config('webshell.menu_types.'.$men->type.'.title') }}
                                                @endif
                                            </option>
                                            
                                            @if ($loop->last)
                                                </optgroup>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                           

                        </div>

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-7">
                                            <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                                Ekle
                                            </button>
                                            &nbsp;&nbsp;
                                            <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                Vazgeç
                                            </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
               </div>
               <div class="tab-pane fade" id="kt_tab_pane_3_2" role="tabpanel">
                    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/content/saveexist') }}" id="existContentForm">
                        {{ csrf_field() }}
                        <input type="hidden" name="crud" value="addexist">
                        <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
                        <input type="hidden" name="content_id" value="{{ Request::segment(5) }}">
                        <input type="hidden" name="lang" value="{{ Request::segment(6) }}">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="m-portlet__body">
                            
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        Mevcut İçerik Seç 
                                        (
                                            @if ($menu->type == 'menuitem')
                                                Menü Öğesi (İçerik Yok)
                                            @elseif ($menu->type == 'content')
                                                İçerik
                                            @elseif($menu->type == 'photogallery')
                                                Foto Galeri
                                            @elseif(starts_with($menu->type, 'list'))
                                                Liste
                                            @elseif($menu->type == 'link')
                                                Link (Yönlendirme)
                                            @endif
                                        )
                                    
                                    </h3>
                                </div>
                            </div>

                            <div class="form-group m-form__group row @if ($errors->has('existcontent')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    İçerik
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('existcontent'))
                                        <div id="existcontent-error" class="form-control-feedback">{{ $errors->first('existcontent') }}</div>
                                    @endif
                                
                                    <select style="width: 855px;" class="form-control m-select2" id="existcontent" name="existcontent"
                                        @if (Request::segment(4) == 'delete')
                                            disabled="disabled" 
                                        @endif
                                    >
                                        <option value="null">Seçiniz...</option>

                                        @php
                                            $typeText = '';
                                            $typeSlug = '';

                                        @endphp

                                        @foreach ($contents as $con)
                                                @php
                                                @endphp
                                            @if ($con->type == 'text')
                                                @php
                                                    $typeText = 'Metin & HTML';
                                                @endphp
                                            @elseif ($con->type == 'photo')
                                                @php
                                                    $typeText = 'Fotoğraf';
                                                @endphp
                                            @elseif ($con->type == 'photogallery')
                                                @php
                                                    $typeText = 'Foto Galeri';
                                                @endphp
                                            @elseif ($con->type == 'link')
                                                @php
                                                    $typeText = 'Button & Link';
                                                @endphp
                                            @elseif ($con->type == 'group')
                                                @php
                                                    $typeText = 'Menü Öğesi';
                                                @endphp
                                            @elseif ($con->type == 'seperator')
                                                @php
                                                    $typeText = 'Seperatör';
                                                @endphp
                                            @elseif ($con->type == 'slide')
                                                @php
                                                    $typeText = 'Slide';
                                                @endphp
                                            @elseif ($con->type == 'form')
                                                @php
                                                    $typeText = 'Form';
                                                @endphp
                                            @elseif ($con->type == 'code')
                                                @php
                                                    $typeText = 'Code';
                                                @endphp
                                            @endif


                                            @if ($typeSlug != $con->type)
                                                @if (!$loop->first)
                                                    </optgroup>
                                                @endif
                                                <optgroup label="{{ $typeText }}">
                                                @php $typeSlug = $con->type; @endphp
                                            @endif
                                            @if(!empty( $con->title ))
                                                <option value="{{ $con->content_id }}"> {{ $con->title  }} </option>
                                            @endif
                                            
                                            @if ($loop->last)
                                                </optgroup>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-7">
                                            <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                                Ekle
                                            </button>
                                            &nbsp;&nbsp;
                                            <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                Vazgeç
                                            </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
               </div>
                </div>
             </div>
            </div>
        @else    
            <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/content/save') }}" id="contentForm">
            
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(4) }}">
                <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="top_content_id" value="{{ (!is_null($ths)) ? $ths->top_id : '' }}">
                <input type="hidden" name="content_ths_id" value="{{ (!is_null($ths)) ? $ths->id : '' }}">
                <input type="hidden" name="content_id" value="{{ (!is_null($content)) ? $content->id : '' }}">

                <input type="hidden" name="lang" value="{{ Request::segment(6) }}">
                <input type="hidden" id="formdata" name="formdata"> 
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                <div class="m-portlet__body">
                    
                    <div class="form-group m-form__group row">
                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">

                                @if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')
                                    Yeni Form Ekle
                                @else

                                    @if (!is_null(Request::segment(6))) 
                                        [{{ Request::segment(6) }}] 
                                    @endif

                                    @if (Request::segment(4) == 'add')
                                        İçerik Ekle
                                    @elseif (Request::segment(4) == 'edit')
                                        @if (is_null(Request::segment(6)))
                                            İçerik Detayları
                                        @else
                                            İçerik Düzenle
                                        @endif
                                    @elseif (Request::segment(4) == 'delete')
                                        İçerik Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                                    @elseif (Request::segment(4) == 'galup')
                                        Galeri Düzenle
                                    @endif

                                    @if (Request::segment(4) != 'add')
                                        <small> (
                                            @if (starts_with($content->type ,'group'))
                                                İçerik Grubu
                                            @elseif ($content->type == 'text')
                                                Metin & HTML
                                            @elseif ($content->type == 'photo')
                                                Fotoğraf
                                            @elseif ($content->type == 'photogallery')
                                                Foto Galeri
                                            @elseif ($content->type == 'link')
                                                Button & Link
                                            @elseif ($content->type == 'video')
                                                Video  
                                            @elseif ($content->type == 'slide')
                                                Slide
                                            @elseif ($content->type == 'form')
                                                Form
                                            @elseif ($content->type == 'seperator')
                                                Seperatör
                                            @elseif ($content->type == 'mapturkey')
                                                Türkiye Haritası
                                            @elseif ($content->type == 'rssfeed')
                                                RSS Feed
                                            @endif
                                        ) </small>
                                    @endif

                                @endif

                            </h3>
                        </div>
                    </div>
             


                        @include('crudpartials.title')

                        @include('crudpartials.slug')

                        
                        @include('crudpartials.type')   

                        @include('crudpartials.order')   

                        @include('crudpartials.status')
                        
                        @include('crudpartials.tag')

                        @include('crudpartials.category')

                        @include('crudpartials.short_content')

                        @include('crudpartials.content')

                        @if(starts_with($menu->type,'list'))

                            @include('crudpartials.content_form')

                        @endif
                        
                        @include('crudpartials.code')

                        @include('crudpartials.props')

                        @include('crudpartials.form')

                </div>
                @php
                //dd($ths);
                @endphp
                

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">

                                @if (Request::segment(4) == 'add' || Request::segment(4) == 'edit')

                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;

                                    @if(is_null($menu) && Request::segment(3) == 0)
                                        <a href="{{ url('form/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                            Vazgeç
                                        </a>
                                    @else
                                        @if(starts_with($menu->type, 'list'))
                                            @if(!is_null($ths))
                                                @if(($ths->top_model=='Content'))
                                                   <a href="{{ url('menu/content/') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                @else
                                                    <a href="{{ url('menu/content/') }}/{{Request::segment(3)}}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                 @endif
                                            @else 
                                               <a href="{{ url('menu/content/') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">   
                                            @endif       
                                                Vazgeç
                                            </a>
                                        @else
                                            @if(!empty($ths->top_id))
                                               <a href="{{ url('menu/content/') }}/{{Request::segment(3)}}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                            @else
                                                <a href="{{ url('menu/content/') }}{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                             @endif   
                                                Vazgeç
                                            </a>
                                        @endif    
                                    @endif

                                @elseif (Request::segment(4) == 'delete')

                                    <div class="alert alert-danger" role="alert">
                                        <strong> Siliyorsunuz... </strong>
                                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                    </div>
                                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydı Sil
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>

                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        @endif    
			
        </div>

        @include('crudpartials.dropzone')

	</div>

@endsection


@section('inline-scripts')
    @include('crudpartials.formpartialsjs')

    @include('crudpartials.contentjs')

    @include('crudpartials.formjs')

    @include('crudpartials.dropzonejs')



<script type="text/javascript">

    
    $(document).ready(function(){

        @if (Request::segment(4) == 'add')
            $('#type').select2({
                placeholder: "Seçiniz..."
            });
             $('#existmenu').select2({
            placeholder: "Seçiniz..."
            });

            $('#existcontent').select2({
                placeholder: "Seçiniz..."
            });
          
        @endif
        
        @if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))

            @if ($content->type == 'form')
                var formBuilder = $('#fb-editor').formBuilder(options);
            @endif

        @endif

        @if (Request::segment(4) == 'edit' )

            @if ($content->type == 'text'||starts_with($content->type , 'section') || $content->type == 'rssfeed' || $content->type == 'mapturkey' || $content->type == 'form' || $content->type == 'seperator' || $content->type == 'link' || $content->type == 'photo' || $content->type == 'photogallery' || $content->type == 'slide' || $content->type == 'code' || $content->type == 'video'|| starts_with($content->type , 'group'))
                $('#props_section').select2({
                    placeholder: "Seçiniz..."
                });
                $('#props_colortheme').select2({
                    placeholder: "Seçiniz..."
                });
                $('#props_colvalue').select2({
                    placeholder: "Seçiniz..."
                });
                $('#stafftype').select2({
                    placeholder: "Seçiniz..."
                });
                $('#section_props_section').select2({
                    placeholder: "Seçiniz..."
                });
                $('#stock').select2({
                    placeholder: "Seçiniz..."
                });
                $('#show_type').select2({
                    placeholder: "Seçiniz..."
                });
                $('#sale_status').select2({
                    placeholder: "Seçiniz..."
                });
            @endif

            @if ($content->type == 'seperator' || $content->type == 'photo' || $content->type == 'photogallery')
                $('#props_type').select2({
                    placeholder: "Seçiniz..."
                });
            @endif

            @if (starts_with($content->type ,'group') || $content->type == 'text' || $content->type == 'photogallery')
                $('#tag').select2({
                    placeholder: "Seçiniz..."
                });
                $('#category').select2({
                    placeholder: "Seçiniz..."
                });
            @endif

        @endif

        $('#postBtn').click(function(e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($content->variableLang(Request::segment(6)))))
                        title: {
                            required: true
                        },
                    @endif
                    @if (Request::segment(4) == 'add')
                        type: {
                            required: true
                        },
                    @endif
                    order: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            @if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))
                @if ($content->type == 'form')
                    $('#formdata').val(formBuilder.actions.getData('json'));
                @endif
            @endif
            
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
        });
    });

</script>

<style type="text/css">
    .m-portlet__body > .form-group{
        display: flex;
    }
</style>
@endsection
