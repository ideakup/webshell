@php use Carbon\Carbon; @endphp
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <!-- If you delete this meta tag, the ground will open and swallow you. -->
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>From Send Mail</title>
    <style type="text/css">
       
    </style>
</head>

<body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
    

    @php
    	$order=App\ShoppingOrder::where('order_no',$data->order_no)->first();
    	$card_total_amount=0;
    @endphp
    <div class="card">
        <div class="card-body">
            <table class="table order">
                @if($order->status=='Hazırlanıyor')<span class="badge badge-warning">Hazırlanıyor</span>
                @elseif($order->status=='Kargoda')<span  class="badge badge-info">Kargoda</span>
                @elseif($order->status=='teslim_edildi')<span class="badge badge-success">Teslim Edildi</span>
                @endif
        <thead>
            <tr>
                <th class="order-date">{{$order->created_at}}</th>
                <th class="order-product-name">Ürün</th>
                <th class="order-product-quantity">Miktar</th>
                <th class="order-product-price">Fiyat</th>
                <th class="order-product-subtotal">Tutar</th>
            </tr>
        </thead>
        <tbody>
            @foreach(json_decode($order->order_content) as $row)
    
                <tr class="order_item">
                    <td class="order-product-thumbnail">
                        @php
                          $allcontent=$allcontent->where('content_id',$row->id)->first();
                        @endphp
                        @php
                            $group_content=json_decode($allcontent->content);
                        @endphp
                        <img width="64" height="64" src="{{ url('upload/medium/'. $group_content->photo) }}">
                     </td>
                     <td class="order-product-name">
                       {{$row->name}}
                     </td>
                     <td class="order-product-quantity">
                        <span>1x{{ $row->qty}} </span>  
                    </td>
                    <td class="cart-product-price">
                      <span class="price">{{number_format((float)$row->price, 2, '.', '')}}</span>
                  </td>
                    @php
                      $rowtotal=$row->price+(($row->price)*($row->options->tax_rate)/100);
                      @endphp

                        <td class="order-product-subtotal">
                            <span class="amount">{{number_format((float)$rowtotal, 2, '.', '')}}</span>
                            @php
                            $card_total_amount=$card_total_amount+($rowtotal*$row->qty);
                            @endphp

                       </td>
                   </td>
                </tr>
            @endforeach
        </tbody>
        </table>
        <div class="fright">
            <tr class="cart_item">
            <td class="cart-product-name">
              <strong>Toplam:</strong>
            </td>
            <td class="cart-product-name">
              <span  class=" order-total"><strong>{{number_format((float)$card_total_amount, 2, '.', '')}}</strong></span>
            </td>
          </tr>
        </div>
        
        </div>
    </div>

</body>

</html>
