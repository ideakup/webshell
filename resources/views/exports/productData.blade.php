@php
    
    //dd($products);
    
@endphp
<table>
    <thead>
        <tr>
            <th>ID sutunlarına dokunmayınız</th>
        </tr>
        <tr>
            <th>Stock sütünuna sadece "var" yada "yok" yazılmalıdır</th>  
        </tr>
        <tr>
            <th>Sütünların yerlerini Kesinlikle değiştimeyiniz</th>       
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
            <th>
                Ths ID
            </th>
            
            <th>
                Content ID
            </th>
            <th>
                Ürün İsmi
            </th>
            <th>
                Stock Durumu    
            </th>
            <th>
                Vergi Oranı   
            </th>
            <th>
               Vergisiz Fiyat      
            </th>
            <th>
                İndirimli(vergisiz) Fiyat   
            </th>
            <th>
                Son Fiyatı
            </th>

        </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
            @php
                $product_content=json_decode($product->content);
            @endphp
            <tr>
                <td>
                    {{$product->id}}
                </td>
                <td>
                    {{$product->sub_id}}
                </td>
                <td>
                    {{$product->title}}
                </td>
                <td>
                    {{$product->stock}}
                </td>
                <td>
                    {{$product_content->tax_rate}} 
                </td>
                <td>
                   {{number_format((float)$product_content->price, 2, '.', '')}} 
                </td>
                <td>
                    {{$product_content->discounted_price}}
                </td>
                <td>
                   {{number_format((float)$product_content->final_price, 2, '.', '')}}
                </td>


            </tr>
        @endforeach
    </tbody>
</table>
@php
    
    //dd("asdlasklas");
    
@endphp

