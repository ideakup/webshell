@php
    
    $formvariable = $form->variable()->first();
    $formvariableArr = json_decode($formvariable->content);
    
@endphp
<table>
    <thead>
        <tr>
            
            <th>
                ID
            </th>

            @foreach ($formvariableArr as $field)
                @if(isset($field->name))
                    <th>
                        {{ $field->label }}
                    </th>
                @endif
            @endforeach

            <th>
                Kaynak
            </th>

        </tr>
    </thead>
    <tbody>
        @foreach($formData as $data)
            <tr>
                <td>{{ $data->id }}</td>
                @php
                    $formDataArr = json_decode($data->data);
                @endphp
                @foreach($formDataArr as $fieldname => $value)

                        @if(starts_with($fieldname, 'date'))
                            <td>{{ Carbon\Carbon::parse($value)->format('d.m.Y') }}</td>
                        @else
                            <td>{{ $value }}</td>
                        @endif

                @endforeach

                @php
                    $sourceData = '';
                @endphp

                @if($data->source_type == 'calendar')
                    <td>{{ 'Etkinlik - '.App\Calendar::find($data->source_id)->variable->title }}</td>
                @elseif($data->source_type == 'menu')
                    <td>{{ 'Menü - '.App\Menu::find($data->source_id)->variable->name }}</td>
                @endif

            </tr>
        @endforeach
    </tbody>
</table>