

@extends('layouts.webshell')

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Dükkan
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator"> - </li>
                <li class="m-nav__item">
                    <span class="m-nav__link-text">
                        Ürünler Listesi
                    </span>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Ürünler Listesi
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ url('store/settings') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="fas fa-cogs"></i>
                                <span>
                                    Sipariş Ayarları
                                </span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            


                <div class="form-group m-form__group row">
                  
                    <div class="col-6 ">
                        <form class="m-form m-form--fit m-form--label-align-right float-right" method="POST" action="{{ url('product/export') }}" id="ProductDataForm">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <button type="button" id="exportProduct" class="btn btn-info">Dışarı Aktar</button>
                        </form>
                        
                    </div>
                    <div class="col-6">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('product/import') }}" id="ProductDataForm" enctype="multipart/form-data">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <input  type="file" id="file" name="product_file" required>
                            <button type="submit" id="importProduct" class="btn btn-info">İçe Aktar</button>
                        </form>

                    </div>
                </div>
            
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="orderListDataTable">
                <thead>
                    <tr>
                        <th width="30">
                           ID
                        </th>
                        <th width="30">
                           Ürün
                        </th>
                        <th width="300">
                            Ürün ismi
                        </th>
                        <th width="50">
                            Stock Durumu
                        </th>
                        <th width="50">
                           Vergi Oranı
                        </th>
                        <th width="50">
                            Vergisiz Fiyat
                        </th>
                        <th width="50">
							İndirimli(vergisiz) Fiyat
                        </th>
                        <th width="50">
							Son Fiyatı
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('inline-scripts')
<script type="text/javascript">

    $(document).ready(function(){
        
        var table = $('#orderListDataTable').DataTable({
            responsive: true,
            dom: `<'row'<'col-sm-12'tr>> <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ ",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
            },
            searching: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                    url: "/getStore", // ajax source
                    type: "POST",
                    headers: { 'X-CSRF-TOKEN': $('#token').val() },
                    data: {
                        // parameters for custom backend script demo
                        columnsDef: ['id','product_img','product_name', 'stock', 'tax','price','d_price','final_price'],
                    },
                },
                columns: [
                {name: 'id'},
                {name: 'product_img'},
                {name: 'product_name'},
                {name: 'stock'},
                {name: 'tax'},
                {name: 'price'},
                {name: 'd_price'},
                {name: 'final_price'}
                ],
                columnDefs: [
                {
                    targets: 0,
                    orderable: false,
                    visible: false,
                },
                {
                    targets: 1,
                    orderable: false,

                    render: function(data, type, full, meta) {

                            return '<img width="64" height="64" src="{{ url('upload/medium/')}}/'+full[1]+'">';
                        },
                },
                
                {
                    targets: 2,
                    orderable: false,
                },
                {
                    targets: 3,
                    orderable: false,
                },
                {
                    targets: 4,
                    orderable: false,
                },
                {
                    targets: 5,
                    orderable: false,
                },
                {
                    targets: 6,
                    orderable: false,
                },
                {
                    targets: 7,
                    orderable: false,
                },
    
             
             
            ],
        });
        $('#exportProduct').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            
            form.submit();
        });
        $('#importProduct').click(function(e) {
            var valid = this.form.checkValidity(); // for demonstration purposes only, will always b "true" here, in this case, since HTML5 validation will block this "click" event if form invalid (i.e. if "required" field "foo" is empty)
            $("#file").html(valid);
            if (valid) {
            event.preventDefault(); 
                var btn = $(this);
                var form = $(this).closest('form');
                
                form.submit();
        }
        });

    });

</script>
@endsection
