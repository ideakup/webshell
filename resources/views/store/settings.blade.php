@php
//dd(json_decode($shop_settigs)->extra);
@endphp

@extends('layouts.webshell')

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Dükkan
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator"> - </li>
                <li class="m-nav__item">
                    <span class="m-nav__link-text">
                        Sipariş Ayarları
                    </span>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Sipariş Ayarları
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
        	<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('store/save') }}" id="storeForm">
                {{ csrf_field() }}

                <div class="form-group m-form__group row @if ($errors->has('payment_method')) has-danger @endif">
                    <label for="example-text-input" class="col-2 col-form-label">Ödeme Yöntemi</label>
                    <div class="col-7">
                        <div class="m-radio-inline">
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="payment_method" value="iyzico" @if (json_decode($shop_settigs->extra)->payment_method == 'iyzico' ) {{ 'checked="checked"' }} @endif> İyzico
                                <span></span>
                            </label>
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="payment_method" value="parampos" @if (json_decode($shop_settigs->extra)->payment_method == 'parampos' ) {{ 'checked="checked"' }} @endif> Parampos
                                <span></span>
                            </label>
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="payment_method" value="halk_bankası" @if (json_decode($shop_settigs->extra)->payment_method == 'halk_bankası' ) {{ 'checked="checked"' }} @endif> Halk Bankası
                                <span></span>
                            </label>
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="payment_method" value="closed" @if (json_decode($shop_settigs->extra)->payment_method == 'closed' ) {{ 'checked="checked"' }} @endif> Kapalı
                                <span></span>
                            </label>
                        </div>
                    </div>
                    @if ($errors->has('payment_method'))
                    <div id="payment_method-error" class="form-control-feedback">{{ $errors->first('payment_method') }}</div>
                    @endif
                </div>

                <div class="form-group m-form__group row @if ($errors->has('data1 ')) has-danger @endif">
                    <label for="example-text-input" class="col-2 col-form-label">
                        Data 1
                    </label>
                    <div class="col-7">
                        <input class="form-control m-input" type="text"  id="data1" name="data1" 
                         value="{{ json_decode($shop_settigs->extra)->data1}}"  autofocus>
                        @if ($errors->has('data1'))
                        <div id="data1-error" class="form-control-feedback">{{ $errors->first('data1') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group m-form__group row @if ($errors->has('data2 ')) has-danger @endif">
                    <label for="example-text-input" class="col-2 col-form-label">
                        Data 2
                    </label>
                    <div class="col-7">
                        <input class="form-control m-input" type="text"  id="data2" name="data2" 
                            value="{{ json_decode($shop_settigs->extra)->data2}}"  autofocus>
                        @if ($errors->has('data2'))
                        <div id="data2-error" class="form-control-feedback">{{ $errors->first('data2') }}</div>
                        @endif
                    </div>
                </div>
                <div class="form-group m-form__group row @if ($errors->has('data3 ')) has-danger @endif">
                    <label for="example-text-input" class="col-2 col-form-label">
                        Data 3
                    </label>
                    <div class="col-7">
                        <input class="form-control m-input" type="text"  id="data3" name="data3" 
                            value="{{ json_decode($shop_settigs->extra)->data3}}"  autofocus>
                        @if ($errors->has('data3'))
                        <div id="data3-error" class="form-control-feedback">{{ $errors->first('data3') }}</div>
                        @endif
                    </div>
                </div>
                <div class="form-group m-form__group row @if ($errors->has('url ')) has-danger @endif">
                    <label for="example-text-input" class="col-2 col-form-label">
                        Url
                    </label>
                    <div class="col-7">
                        <input class="form-control m-input" type="text"  id="url" name="url" 
                            value="{{ json_decode($shop_settigs->extra)->url}}"  autofocus>
                        @if ($errors->has('url'))
                        <div id="url-error" class="form-control-feedback">{{ $errors->first('url') }}</div>
                        @endif
                    </div>
                </div>

                
                <div class="m-portlet__body">
                	<div class="m-form__group form-group row">
					    <label for="example-text-input" class="col-2 col-form-label">
					        Kargo Ücreti Yok/Var
					    </label>
					    <div class="col-3">
					        <span class="m-switch">
					            <label>
					                <input type="checkbox" @if ($shop_settigs->shipping_cost == 'active' ) {{ 'checked="checked"' }} @endif  id="shipping_cost" name="shipping_cost" value="active" 
					                
					                />
					                <span></span>
					            </label>
					        </span>
					    </div>
					</div>
					<div class="form-group m-form__group row @if ($errors->has('shipping_type')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Kargo Tipi
                            </label>
                            <div class="col-7">
                                @if ($errors->has('shipping_type'))
                                	<div id="shipping_type-error" class="form-control-feedback">{{ $errors->first('shipping_type') }}</div>
                                @endif
                                <select class="form-control m-select2" id="shipping_type" name="shipping_type">
                       
                                
                               @foreach (config('webshell.shipping_type') as $shipping_type)
                                    <option value="{{ $shipping_type['value'] }}" @if($shop_settigs->shipping_type== $shipping_type['value'] ) selected @endif> {{ $shipping_type['title'] }} </option>
                                @endforeach
                               
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group row @if ($errors->has('shipping_cost_price	')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Kargo Ücreti
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="number" min="1" max="100000" id="shipping_cost_price" name="shipping_cost_price" 
							 value="{{ $shop_settigs->shipping_cost_price }}" required autofocus>
                            @if ($errors->has('shipping_cost_price'))
                            <div id="shipping_cost_price-error" class="form-control-feedback">{{ $errors->first('shipping_cost_price') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group row @if ($errors->has('min_shippingcost	')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Minumum Kargo Ücreti
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="number" min="1" max="100000" id="min_shippingcost	" name="min_shippingcost" 
							 value="{{ $shop_settigs->min_shippingcost }}" required autofocus>
                            @if ($errors->has('min_shippingcost'))
                            <div id="min_shippingcost-error" class="form-control-feedback">{{ $errors->first('min_shippingcost') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group" style="padding-bottom: 0;">
					    <label>Satış Metni</label>
					</div>
			        @php
			        @endphp
					<div class="form-group m-form__group @if ($errors->has('shopping_text')) has-danger @endif">
					    <textarea class="form-control m-input" id="shopping_text" name="shopping_text" rows="3">{!! $shop_settigs->shopping_text !!}</textarea>

					    @if ($errors->has('shopping_text'))
					        <div id="shopping_text-error" class="form-control-feedback">{{ $errors->first('shopping_text') }}</div>
					    @endif
					</div>



                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('store/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a> 
                            </div>
                        </div>
                    </div>
                </div>

            </form>
         
        </div>
    </div>
</div>
@endsection

@section('inline-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#shipping_type').select2({
            placeholder: "Seçiniz..."
        });




    });
</script>

<script type="text/javascript">

            
            tinymce.init({
                selector: 'textarea#shopping_text',

                @if (Request::segment(4) == 'delete')
                    readonly : 1,
                @endif
                
                //plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
                //Color Picker, Context Menu, MoxieManager, noneditable, pageembed,  Text Color documentation
                //, linkchecker TEKRAR BAK
                //codesample, directionality, fullpage, help, pagebreak, paste, print, template, textpattern, toc, visualchars

                plugins: 'advlist lists anchor autolink autoresize charmap code fullscreen hr image imagetools insertdatetime link media nonbreaking preview searchreplace table visualblocks wordcount importcss paste template',
                
                // table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol
                // formatselect styleselect fontselect fontsizeselect
                toolbar: 'forMoreCustomButton | template | undo redo | formatselect | bold italic underline strikethrough subscript superscript | removeformat | forecolor backcolor | alignleft aligncenter alignright alignjustify alignnone | numlist bullist outdent indent | link image media | anchor charmap hr insertdatetime nonbreaking | searchreplace | visualblocks code fullscreen',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",

                branding: false,
                min_height: 400,

                relative_urls: false,
                convert_urls: false,

                images_upload_url: '{{ url('/uploadFile') }}',
                images_upload_base_path: '{{ url('/') }}',
                images_upload_credentials: true,
                images_upload_handler: function (blobInfo, success, failure) {
                   var xhr, formData;
                   xhr = new XMLHttpRequest();
                   xhr.withCredentials = false;
                   xhr.open('POST', '{{ url('/uploadFile') }}');
                   var token = '{{ csrf_token() }}';
                   xhr.setRequestHeader("X-CSRF-Token", token);
                   xhr.onload = function() {
                       var json;
                       if (xhr.status != 200) {
                           failure('HTTP Error: ' + xhr.status);
                           return;
                       }
                       json = JSON.parse(xhr.responseText);

                       if (!json || typeof json.location != 'string') {
                           failure('Invalid JSON: ' + xhr.responseText);
                           return;
                       }
                       success(json.location);
                   };
                   formData = new FormData();
                   formData.append('file', blobInfo.blob(), blobInfo.filename());
                   formData.append('uploadType', 'editor');
                   xhr.send(formData);
                },
                extended_valid_elements: "formore",
                custom_elements: "formore",
                setup: function (editor) {
                    editor.ui.registry.addButton('forMoreCustomButton', {
                        icon: 'flip-vertically',
                        onAction: function (_) {
                            editor.insertContent('<formore></formore>');
                        }
                    });
                },
                content_css: "/css/tinymce_inline.css",
                templates: [
                    {title: 'Yorum Paneli', description: 'Yorum şablonunu kullanmak için seçiniz...', content: '<div class="yorum-cerceve"><p class="tinymce-yorum-baslik">Adı Soyadı</p><p class="tinymce-yorum-tarih">01.01.2020</p><p class="tinymce-yorum-metin">Yorum metnini buraya ekleyiniz</p></div>'}
                ],

            });
            


        </script>
@endsection