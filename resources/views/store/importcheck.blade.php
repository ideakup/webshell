@php
//dump($before_products);
//dd($after_products['0']);
//dd("asdasdas");
@endphp




@extends('layouts.webshell')

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Dükkan
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator"> - </li>
                <li class="m-nav__item">
                    <span class="m-nav__link-text">
                        Ürünler Listesi
                    </span>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Ürünler Listesi
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ url('store/settings') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="fas fa-cogs"></i>
                                <span>
                                    Sipariş Ayarları
                                </span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            


                <div class="form-group m-form__group row">
                  
                    <div class="col-6 ">
                
                        
                    </div>
                    <div class="col-6">
                        <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('product/import') }}" id="ProductDataForm" enctype="multipart/form-data">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <input  type="file" id="file" name="product_file" required>
                            <button type="submit" id="importProduct" class="btn btn-info">İçe Aktar</button>
                        </form>

                    </div>
                </div>
            
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="orderListDataTable">
                <thead>
                    <tr>
                     
                        <th width="30">
                           Ürün
                        </th>
                        <th width="300">
                            Ürün ismi
                        </th>
                        <th width="50">
                            Stock Durumu
                        </th>
                        <th width="50">
                           Vergi Oranı
                        </th>
                        <th width="50">
                            Vergisiz Fiyat
                        </th>
                        <th width="50">
							İndirimli(vergisiz) Fiyat
                        </th>
                        <th width="50">
							Son Fiyatı
                        </th>
                    </tr>
                </thead>
                <tbody>
                	@php
                	$error=0;
                	@endphp
                	@for($i=0; $i<count($after_products); $i++)
                		@for($i=0; $i<count($before_products); $i++)
                			<tr>
                			@php
	                			$after_product_content=json_decode($after_products[$i]->content);
	                			$before_products_content=json_decode($before_products[$i]->content);
	                		@endphp
	                		<td>
	                			
	                			<img width="64" height="64" src="{{ url('upload/medium/')}}/{{$after_product_content->photo}}">
	                		</td>
	                		<td>
	                			@if($after_products[$i]->title != $before_products[$i]->title)
	                				<del class="text-warning">{{$before_products[$i]->title}}</del><br>
	                				<p class="text-primary"> {{$after_products[$i]->title}}</p>	
	                			@else
	                				{{$after_products[$i]->title}}
	                			@endif		
	                		</td>
	                		<td class="text-center">
	                			@php
	                			@endphp
	                			@if($after_products[$i]->stock =='var' || $after_products[$i]->stock=='yok')
		                			@if($after_products[$i]->stock != $before_products[$i]->stock)
		                				<del class="text-warning">{{$before_products[$i]->stock}}</del><br>
		                					<p class="text-primary">{{$after_products[$i]->stock}}</p>
		                			@else
		                				{{$after_products[$i]->stock}}
		                			@endif
		                		@else
			                		@php
				                		$error++;
				                	@endphp
	                				<i class="fas fa-times-circle fa-2x text-danger"></i>
	                				<p class="text-danger">Yanlış veri girilmiş</p>	
		                		@endif		
	                		</td>
	                		<td class="text-center">

	                			@if($after_product_content->tax_rate != $before_products_content->tax_rate)
	                				<del class="text-warning" >{{$before_products_content->tax_rate}}</del><br>
	                					<p class="text-primary">{{$after_product_content->tax_rate}}</p>
	                			@else
	                				{{$after_product_content->tax_rate}}
	                			@endif
	                			
	                		</td>	
	                		<td class="text-center">
	                			@if(is_numeric($after_product_content->price))
		                			@if($after_product_content->price != $before_products_content->price)
		                				<del class="text-warning">{{$before_products_content->price}}</del><br>
		                					<p class="text-primary">{{$after_product_content->price}}</p>
		                			@else
		                				{{$after_product_content->price}}
		                			@endif
		                		@else
		                			@php
				                		$error++;
				                	@endphp
		                			<i class="fas fa-times-circle fa-2x text-danger"></i>
	                				<p class="text-danger">Uygun olmayan veri girilmiş</p>
	                			@endif		
	                		</td>
	                		<td class="text-center">
		                			@if($after_product_content->discounted_price != $before_products_content->discounted_price)
		                				<del class="text-warning">{{$before_products_content->discounted_price}}</del><br>
		                					<p class="text-primary">{{$after_product_content->discounted_price}}</p>
		                			@else
		                				{{$after_product_content->discounted_price}}
		                			@endif
		                			
	                			
	                		</td>
	                		<td class="text-center">
	                			{{$after_products[$i]->final_price}}
	                		</td>
                		</tr>

                		@endfor
                		
                		
                	@endfor
                	
                </tbody>
            </table>
        @php

        @endphp
            
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
	        <div class="m-form__actions">
	            <div class="d-flex justify-content-center">
	                
			
	                    @if($error==0)
			        		<form style="padding: 30px;" class="m-form m-form--fit m-form--label-align-right " method="POST" action="{{ url('store/importconfirm') }}" id="ProductDataForm">
			                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
			                        <input type="hidden" name="after_products[]" id="after_products" value="{{collect($after_products)  }}">
			                        <button type="submit" id="exportProduct" class="btn btn-danger m-btn m-btn--air m-btn--custom">KAYDET</button> 
			            	</form>
			            @else
			            	<div class="alert alert-danger" role="alert">
			            		<i class="fas fa-exclamation-triangle "></i>
							  	Uygun Olmayan veri yada veriler var. Dosyanızı tabloya göre kontrol edip yeniden yükleyiniz !
							</div>
			            @endif

	                 
	                
	            </div>
	        </div>
	    </div>
    </div>
</div>




@endsection

@section('inline-scripts')
<script type="text/javascript">

	$('#importProduct').click(function(e) {
        var valid = this.form.checkValidity(); // for demonstration purposes only, will always b "true" here, in this case, since HTML5 validation will block this "click" event if form invalid (i.e. if "required" field "foo" is empty)
        $("#file").html(valid);
        if (valid) {
        event.preventDefault(); 
            var btn = $(this);
            var form = $(this).closest('form');
            
            form.submit();
    }
    });

</script>
@endsection
