@extends('layouts.webshellauth')

@section('content')

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
        
        <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
            <div class="m-stack m-stack--hor m-stack--desktop">
                <div class="m-stack__item m-stack__item--fluid">
                    <div class="m-login__wrapper">

                        <div class="m-login__logo">
                            <img src="{{ asset('images/ws-logo.png') }}" class="img-fluid">
                        </div>

                        <div class="m-login__signin">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                    Giriş Yap
                                </h3>
                            </div>

                            <form class="m-login__form m-form" method="POST" action="{{ route('login') }}" id="loginForm">
                                {{ csrf_field() }}
                                
                                @if ($errors->has('email'))
                                    <strong>{{ $errors->first('email') }}</strong>
                                @endif

                                @if ($errors->has('password'))
                                    <br><strong>{{ $errors->first('password') }}</strong>
                                @endif

                                <div class="m-card-profile" id="user_card_div" style="display: none;">
                                    <div class="m-card-profile__pic">
                                        <div class="m-card-profile__pic-wrapper" style="margin-top: 0;">   
                                            <img src="{{ asset('images/avatars/default.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="m-card-profile__details">
                                        <span class="m-card-profile__name" id="user_name"></span>
                                        <span class="m-card-profile__email" id="user_email"></span>
                                    </div>
                                </div>

                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="email" id="email" name="email" placeholder="E-posta" value="{{ old('email') }}" required autofocus>    
                                </div>

                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last" type="password" id="password" name="password" placeholder="Şifre" required>
                                </div>

                                <div class="row m-login__form-sub">
                                    <div class="col m--align-left">
                                        <label class="m-checkbox m-checkbox--focus" id="remember_label">
                                            <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            Beni Hatırla
                                            <span></span>
                                        </label>
                                        <div id="change_user_div" style="display: none;">
                                            <a href="javascript:;" id="change_user" class="m-link">
                                                Başka hesap ?
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col m--align-right">
                                        <a href="javascript:;" id="m_login_forget_password" class="m-link">
                                            Şifremi Unuttum ?
                                        </a>
                                    </div>
                                </div>

                                <div class="m-login__form-action">
                                    <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                        Giriş Yap
                                    </button>
                                </div>
                            </form>
                        </div>
                        
                        <div class="m-login__forget-password">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                    Şifremi Unuttum ?
                                </h3>
                                <div class="m-login__desc">
                                    Şifrenizi yenilemek için e-postanızı girin:
                                </div>
                            </div>
                            <form class="m-login__form m-form" method="POST" action="{{ route('password.email') }}" id="passForm">
                                {{ csrf_field() }}
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="email" placeholder="E-posta" name="email" id="m_email" autocomplete="off">
                                </div>
                                <div class="m-login__form-action">
                                    <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                        Gönder
                                    </button>
                                    <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                                        Vazgeç
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        @include('partials.auth.info')
        
    </div>
</div>
<!-- end:: Page -->
@endsection
