

@extends('layouts.webshell')

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                 Etkinlik 
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator"> </li>
                <li class="m-nav__item">
                    <span class="m-nav__link-text">
                    	Etkinlik-Seans Düzenle 
                    </span>
                </li>
                
            </ul>
        </div>
        <div class="m-portlet__head-tools">
            <a href="{{ url('eventsession/delete') }}/{{ $event_session->id }}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="Kategori'yü Sil">
                <i class="fa fa-trash"></i>
            </a>
		</div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('/eventsession/save') }}" id="orderForm">
                {{ csrf_field() }}
                <input type="hidden" name="eventsession_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
                
                <div class="form-group m-form__group row @if ($errors->has('section_props_section')) has-danger @endif">
		            <label for="example-text-input" class="col-2 col-form-label">
		                Etkinlik
		            </label>
		            <div class="col-7">
		                <select  class="form-control m-select2" id="event" name="event">
		                	<option value="{{$event_session_ths->topElementForContent->id}}" >{{$event_session_ths->topElementForContent->variable->title}}</option>
			                @foreach ($events as $key => $event)
			                <option value="{{$event->id }}" 
			               
			                > {{$event->variable->title }} </option>
			                @endforeach
		            	</select>
		        	</div>
        		</div>

                <div class="form-group m-form__group row @if ($errors->has('type')) has-danger @endif">
                	<label for="example-text-input" class="col-2 col-form-label">Gösteri Tipi</label>
                    <div class="col-7">
                        <div class="m-radio-inline">
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="show_type" value="sahne"@if($event_session->show_type=='sahne') checked @endif required> sahne
                                <span></span>
                            </label>
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="show_type" value="online"@if($event_session->show_type=='online') checked @endif required> online
                                <span></span>
                            </label>
                        </div>
                    </div>
		    	</div>
		    	<div class="form-group m-form__group row @if ($errors->has("eventsession_date")) has-danger @endif">
				    <label for="example-text-input" class="col-2 col-form-label">
				        Gösteri Tarihi
				    </label>
				   
				    <div class="col-7">
				       <input class="form-control m-input" type="datetime-local" id="eventsession_date" name="eventsession_date" value="{{$dt->toDateString()}}T{{$dt->format('H:i')}}" required >

				        @if ($errors->has("eventsession_date"))
				            <div id="eventsession_date-error" class="form-control-feedback">{{ $errors->first(data_get("eventsession_date")) }}</div>
				        @endif
				    </div>
				</div>
				<div class="form-group m-form__group row @if ($errors->has('type')) has-danger @endif">
					<label for="example-text-input" class="col-2 col-form-label"> Satış Durumu</label>
                    <div class="col-7">
                        <div class="m-radio-inline">
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="sale_status" value="on_sale"@if($event_session->sale_status=='on_sale') checked @endif required> Satışta
                                <span></span>
                            </label>
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="sale_status" value="soon"@if($event_session->sale_status=='soon') checked @endif required > Yakında
                                <span></span>
                            </label>
                        </div>
                    </div>
		    	</div>
		    	<div class="form-group m-form__group row @if ($errors->has("capacity")) has-danger @endif">
				    <label for="example-text-input" class="col-2 col-form-label">
				        Kapasite
				    </label>
				   
				    <div class="col-7">
				       <input class="form-control m-input" type="number" id="capacity" name="capacity" 
				       @if(!is_null($event_session->capacity))
				       		value="{{$event_session->capacity}}"
				       
				       @endif
 							>
				        @if ($errors->has("capacity"))
				            <div id="capacity-error" class="form-control-feedback">{{ $errors->first(data_get("capacity")) }}</div>
				        @endif
				    </div>
				</div>
				<div class="form-group m-form__group row @if ($errors->has("capacity")) has-danger @endif">
				    <label for="example-text-input" class="col-2 col-form-label">
				        Vergi Oranı
				    </label>
				   
				    <div class="col-7">
				       <input class="form-control m-input" type="number" id="tax_rate" name="tax_rate" value="{{$price_attribute->tax_rate}}" >

				        @if ($errors->has("tax_rate"))
				            <div id="tax_rate-error" class="form-control-feedback">{{ $errors->first(data_get("tax_rate")) }}</div>
				        @endif
				    </div>
				</div>
				<div class="form-group m-form__group row @if ($errors->has("price")) has-danger @endif">
				    <label for="example-text-input" class="col-2 col-form-label">
				        Vergisiz Fiyat
				    </label>
				   
				    <div class="col-7">
				       <input class="form-control m-input" type="number" id="price" name="price" value="{{$price_attribute->price}}"  >

				        @if ($errors->has("price"))
				            <div id="price-error" class="form-control-feedback">{{ $errors->first(data_get("price")) }}</div>
				        @endif
				    </div>
				</div>
				<div class="form-group m-form__group row @if ($errors->has("discounted_price")) has-danger @endif">
				    <label for="example-text-input" class="col-2 col-form-label">
				        İndirimli(vergisiz) Fiyat
				    </label>
				   
				    <div class="col-7">
				       <input class="form-control m-input" type="number" id="discounted_price" name="discounted_price" value="{{$price_attribute->discounted_price}}" >

				        @if ($errors->has("discounted_price"))
				            <div id="discounted_price-error" class="form-control-feedback">{{ $errors->first(data_get("discounted_price")) }}</div>
				        @endif
				    </div>
				</div>
				<div class="form-group m-form__group row @if ($errors->has("display_info")) has-danger @endif">
				    <label for="example-text-input" class="col-2 col-form-label">
				        Fiyat Bilgilendirme
				    </label>
				    @php 
				     

				    @endphp
				    <div class="col-7">
				    	Vergi Oranı: <span style="font-size: 14px" id="taxrate" class="badge badge-info"> 0</span>
				    	Vergi Tutarı: <span style="font-size: 14px" id="taxtprice" class="badge badge-info"> 0</span>
				    	Toplam Fiyat: <span style="font-size: 14px" id="totalcost"class="badge badge-info">0 ₺ </span>
						<button type="button" class="btn btn-primary" onclick="update()">Hesapla</button>
				    </div>
				</div>
             
                 <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                    Kaydet
                                </button>
                                &nbsp;&nbsp;
                                <a href="{{ url('/eventsession/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                    Vazgeç
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>   
        </div>
    </div>
</div>

@endsection

@section('inline-scripts')
	<script type="text/javascript">
	 	$(document).ready(function(){
            $('#event').select2({
	            placeholder: " Seçiniz..."
	        });
         });
		function update() {
     		var tax_rate=document.getElementById("tax_rate").value;
     		var price=document.getElementById("price").value;
     		var discounted_price=document.getElementById("discounted_price").value;

     		if(discounted_price){
     			var current_price=discounted_price;
     		}
     		else{
     			var current_price=price;
     		}
     		console.log(current_price);

     		document.getElementById("taxrate").innerHTML ="%"+tax_rate;
     		var tax_price=(tax_rate*current_price)/100;
     		document.getElementById("taxtprice").innerHTML =tax_price+"₺";
     		document.getElementById("totalcost").innerHTML =parseFloat(tax_price)+parseFloat(current_price)+"₺";


		}
	</script>

@endsection
