

@extends('layouts.webshell')

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Etkinlik 
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator"> - </li>
                <li class="m-nav__item">
                    <span class="m-nav__link-text">
                        Etkinlik Takvimi
                    </span>
                </li>
            </ul>
        </div>

    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       Etkinlik Takvimi
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ url('eventsession/add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="fa fa-plus"></i>
                                <span>
                                    Yeni Seans Ekle
                                </span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="eventsessionListDataTable">
                <thead>
                    <tr>
                        <th width="30">
                            ID
                        </th>
                        <th width="30">
                            Gösteri Tarihi
                        </th>
                        <th width="30">
                            Etkinlik
                        </th>
                        <th width="50">
                            Gösteri Tipi
                        </th>
                        <th width="50">
                            Satış Durumu
                        </th>
                        <th width="50">
                            Kapasite
                        </th>
                        <th width="50">
                            İşlemler
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('inline-scripts')

    <script type="text/javascript">
         var table = $('#eventsessionListDataTable').DataTable({
            responsive: true,
            dom: `<'row'<'col-sm-12'tr>> <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ ",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
            },
            searching: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                    url: "/getEventsession", // ajax source
                    type: "POST",
                    headers: { 'X-CSRF-TOKEN': $('#token').val() },
                    data: {
                        // parameters for custom backend script demo
                        columnsDef: ['id','eventsession_date','event','show_type', 'sale_status', 'capacity','actions'],
                    },
                },
                columns: [
                {name: 'id'},
                {name: 'eventsession_date'},
                {name: 'event'},
                {name: 'show_type'},
                {name: 'sale_status'},
                {name: 'capacity'},
                {name: 'actions'}
                ],
                columnDefs: [
                {
                    targets: 0,
                    orderable: false,
                    visible: false,
                },
                
                {
                    targets: 2,
                    orderable: false,
                },
                {
                    targets: 3,
                    orderable: false,
                     render: function(data, type, full, meta) {
                            var status = {
                                'online': {'title': 'Online ', 'class': 'm-badge--primary'},
                                'sahne': {'title': 'Sahne', 'class': 'm-badge--info'},
                                
                            };
                            if (typeof status[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="m-badge ' + status[data].class + ' m-badge--wide">' + status[data].title + '</span>';
                        },
                },
                {
                    targets: 4,
                    orderable: false,
                    render: function(data, type, full, meta) {
                            var status = {
                                'soon': {'title': 'Yakında', 'class': 'm-badge--success'},
                                'on_sale': {'title': 'Satışta', 'class': 'm-badge--warning'},
                                
                            };
                            if (typeof status[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="m-badge ' + status[data].class + ' m-badge--wide">' + status[data].title + '</span>';
                        },
                },
                {
                    targets: -1,
                    title: 'İşlemler',
                    orderable: false,
                    render: function(data, type, full, meta) {   

                        var editButtons = '';
                        

                        return editButtons+`
                        <a href="edit/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Seans düzenleme">
                        <i class="fas fa-edit"></i>
                        </a>
                     `;

                    },
                },
            ],
        });
    </script>

@endsection
