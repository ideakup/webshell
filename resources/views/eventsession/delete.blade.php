

@extends('layouts.webshell')

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                 Etkinlik 
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator"> </li>
                <li class="m-nav__item">
                    <span class="m-nav__link-text">
                    	Etkinlik-Seans Sil 
                    </span>
                </li>
                
            </ul>
        </div>
        
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('/eventsession/save') }}" id="orderForm">
                {{ csrf_field() }}
                <input type="hidden" name="eventsession_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
                
                <div class="form-group m-form__group row @if ($errors->has('section_props_section')) has-danger @endif">
		            <label for="example-text-input" class="col-2 col-form-label">
		                Etkinlik
		            </label>
			        <div class="col-7">    
			            <input class="form-control m-input" type="text" id="eventsession_date" name="eventsession_date" value="{{$event_session_ths->topElementForContent->variable->title}}" disabled >
			        </div>   
        		</div>

                
		    	<div class="form-group m-form__group row @if ($errors->has("eventsession_date")) has-danger @endif">
				    <label for="example-text-input" class="col-2 col-form-label">
				        Gösteri Tarihi
				    </label>
				   
				    <div class="col-7">
				       <input class="form-control m-input" type="datetime-local" id="eventsession_date" name="eventsession_date" value="{{$dt->toDateString()}}T{{$dt->format('H:i')}}" disabled >

				        @if ($errors->has("eventsession_date"))
				            <div id="eventsession_date-error" class="form-control-feedback">{{ $errors->first(data_get("eventsession_date")) }}</div>
				        @endif
				    </div>
				</div>
				
             
                 <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                <div class="alert alert-danger" role="alert">
                                        <strong> Siliyorsunuz... </strong>
                                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                    </div>
                                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydı Sil
                                    </button>
                                &nbsp;&nbsp;
                                <a href="{{ url('/eventsession/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                    Vazgeç
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>   
        </div>
    </div>
</div>

@endsection

