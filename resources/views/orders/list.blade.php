

@extends('layouts.webshell')

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Siparişler
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator"> - </li>
                <li class="m-nav__item">
                    <span class="m-nav__link-text">
                        Siparişler Listesi
                    </span>
                </li>
            </ul>
        </div>

    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Siparişler Listesi
                    </h3>
                </div>
            </div>
            
        </div>

        <div class="m-portlet__body">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="orderListDataTable">
                <thead>
                    <tr>
                        <th width="30">
                           ID
                        </th>
                        <th width="30">
                           Tarih
                        </th>
                        <th width="50">
                            İsim Soyisim
                        </th>
                        <th width="50">
                            Sipariş No
                        </th>
                        <th width="50">
                           Durum
                        </th>
                        <th width="50">
                            İşlemler
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('inline-scripts')
<script type="text/javascript">

    $(document).ready(function(){
        
        var table = $('#orderListDataTable').DataTable({
            responsive: true,
            dom: `<'row'<'col-sm-12'tr>> <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ ",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
            },
            searching: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                    url: "/getOrders", // ajax source
                    type: "POST",
                    headers: { 'X-CSRF-TOKEN': $('#token').val() },
                    data: {
                        // parameters for custom backend script demo
                        columnsDef: ['id','created_at','customer_name', 'order_no', 'status','actions'],
                    },
                },
                columns: [
                {name: 'id'},
                {name: 'created_at'},
                {name: 'customer_name'},
                {name: 'order_no'},
                {name: 'status'},
                {name: 'actions'}
                ],
                columnDefs: [
                {
                    targets: 0,
                    orderable: false,
                    visible: false,
                },
                
                {
                    targets: 2,
                    orderable: false,
                },
                {
                    targets: 3,
                    orderable: false,
                },
                {
                    targets: 4,
                    orderable: false,
                    render: function(data, type, full, meta) {
                            var status = {
                                'prepare': {'title': 'Hazırlanıyor', 'class': 'm-badge--warning'},
                                'shipping': {'title': 'Kargoda', 'class': 'm-badge--info'},
                                'delivered': {'title': 'Teslim Edildi', 'class': 'm-badge--success'},
                                'canceled': {'title': 'İptal Edildi', 'class': 'm-badge--danger'},
                                
                            };
                            if (typeof status[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="m-badge ' + status[data].class + ' m-badge--wide">' + status[data].title + '</span>';
                        },
                },
                {
                    targets: -1,
                    title: 'İşlemler',
                    orderable: false,
                    render: function(data, type, full, meta) {   

                        var editButtons = '';
                        

                        return editButtons+`
                        <a href="detail/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Sipariş Detayı">
                        <i class="fas fa-box-open"></i>
                        </a>
                     `;

                    },
                },
            ],
        });

    });
</script>
@endsection
