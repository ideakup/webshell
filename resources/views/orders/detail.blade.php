

@extends('layouts.webshell')

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Siparişler
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator"> </li>
                <li class="m-nav__item">
                    <span class="m-nav__link-text">
                        {{$customer_name}}
                    </span>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Sipariş İçeriği - [{{$order->order_no}}]
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="card">
                @php
                   // dd($order->address_information);
                @endphp
                <div class="card-body">
                    <h3>Adres Bilgileri[{{$customer_name}}]</h3>
                    <p>Adres: {{$adress}} <strong>{{$town_city}}</strong></p>
                    <p>Telefon No: {{$phone}}</p>
                </div>
            </div>         
            <div class="card">
                @php
                    $card_total_amount=0;
                @endphp
                <div class="card-body">
                    <table class="table order">
                        <thead>
                            <tr>
                                <th class="order-product-name">Ürün</th>
                                <th class="order-product-quantity">Miktar</th>
                                <th class="order-product-price">Fiyat</th>
                                <th class="order-product-subtotal">Tutar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(json_decode($order->order_content) as $row)
                    
                                <tr class="order_item">
                                    <td class="order-product-thumbnail">
                                        @php
                                          $allcontent=$allcontent->where('content_id',$row->id)->first();
                                        @endphp
                                        @php
                                            $group_content=json_decode($allcontent->content);
                                        @endphp
                                        <img width="64" height="64" src="{{ url('upload/medium/'.$group_content->photo) }}">
                                     </td>
                                     <td class="order-product-name">
                                       {{$row->name}}
                                     </td>
                                     <td class="order-product-quantity">
                                        <span>1x{{ $row->qty}} </span>  
                                    </td>
                                    <td class="cart-product-price">
                                      <span class="price">{{number_format((float)$row->price, 2, '.', '')}}</span>
                                  </td>
                                    <td class="order-product-subtotal">
                                        <span class="amount">{{number_format((float)$row->subtotal, 2, '.', '')}}</span>
                                        @php
                                            $card_total_amount=$card_total_amount+($row->price*$row->qty);
                                        @endphp

                                   </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div style="float:right;">
                        <tr class="cart_item">
                        <td class="cart-product-name">
                          <strong>Toplam:</strong>
                        </td>
                        <td class="cart-product-name">
                          <span  class=" order-total"><strong>{{number_format((float)$card_total_amount, 2, '.', '')}}</strong></span>
                        </td>
                      </tr>
                    </div>
                </div>
            </div>
        </div>
        @php
            //dd( $order->status)
        @endphp
        <div class="m-portlet__body">
            <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('/orders/save') }}" id="orderForm">
                {{ csrf_field() }}
                <input type="hidden" name="order_id" value="{{ Request::segment(3) }}">

               <div class="form-group m-form__group row @if ($errors->has('status')) has-danger @endif">
                    <label for="example-text-input" class="col-2 col-form-label">Sipariş Durumu</label>
                    <div class="m-radio-inline">
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="orderstatus" value="prepare" @if ( $order->status == 'prepare') {{ 'checked="checked"' }} @endif> Hazırlanıyor
                                <span></span>
                            </label>
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="orderstatus" value="shipping" @if ( $order->status == 'shipping') {{ 'checked="checked"' }} @endif> Kargoda
                                <span></span>
                            </label>
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="orderstatus" value="delivered"
                                 @if ( $order->status =='delivered') {{ 'checked="checked"' }} @endif> Teslim Edildi
                                <span></span>
                            </label>
                            <label class="m-radio m-radio--solid m-radio--state-brand">
                                <input type="radio" name="orderstatus" value="canceled"
                                 @if ( $order->status =='canceled') {{ 'checked="checked"' }} @endif> İptal Edildi
                                <span></span>
                            </label>
                        </div>
                    @if ($errors->has('status'))
                    <div id="status-error" class="form-control-feedback">{{ $errors->first('status') }}</div>
                    @endif
                </div>
                 <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                    Kaydet
                                </button>
                                &nbsp;&nbsp;
                                <a href="{{ url('/orders/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                    Vazgeç
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>   
        </div>
    </div>
</div>

@endsection

@section('inline-scripts')

@endsection
