<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapTables extends Migration
{
    
    public function up()
    {

        Schema::create('map_country', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',16);

            $table->string('latitude', 191)->nullable();
            $table->string('longitude', 191)->nullable();

            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::create('map_countryvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->string('lang_code',6);
            $table->string('name', 191);
            $table->timestamps();
        });

        Schema::table('map_countryvariable', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('map_country');
        });

        Schema::create('map_city', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();

            $table->string('latitude', 191)->nullable();
            $table->string('longitude', 191)->nullable();

            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('map_city', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('map_country');
        });

        Schema::create('map_cityvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();
            $table->string('lang_code',6);

            $table->string('name', 191);
            $table->string('slug', 191);
            $table->timestamps();
        });

        Schema::table('map_cityvariable', function (Blueprint $table) {
            $table->foreign('city_id')->references('id')->on('map_city');
        });

        Schema::create('map_county', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();

            $table->string('latitude', 191)->nullable();
            $table->string('longitude', 191)->nullable();

            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('map_county', function (Blueprint $table) {
            $table->foreign('city_id')->references('id')->on('map_city');
        });

        Schema::create('map_countyvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('county_id')->unsigned();
            $table->string('lang_code',6);

            $table->string('name', 191);
            $table->string('slug', 191);
            $table->timestamps();
        });

        Schema::table('map_countyvariable', function (Blueprint $table) {
            $table->foreign('county_id')->references('id')->on('map_county');
        });

    }

    public function down()
    {

        Schema::dropIfExists('map_countyvariable');
        Schema::dropIfExists('map_county');
        Schema::dropIfExists('map_cityvariable');
        Schema::dropIfExists('map_city');
        Schema::dropIfExists('map_countryvariable');
        Schema::dropIfExists('map_country');
        
    }
}
