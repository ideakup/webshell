<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingOrderSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_order_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('shipping_cost', ['active', 'passive'])->default('passive');
            $table->string('shipping_type')->nullable();
            $table->integer('shipping_cost_price')->nullable();
            $table->integer('min_shippingcost')->nullable();
            $table->mediumText('shopping_text')->nullable();
            $table->mediumText('extra')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_order_settings');
    }
}
