<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTables extends Migration
{
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 50)->default('content')->comment('menuitem, content, list, link, photogallery, calendar');
            $table->timestamps();
        });


        Schema::create('menuvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->string('lang_code',6);
            $table->string('name',191);
            $table->string('slug',191)->nullable();
            $table->string('menutitle',191)->nullable();
            $table->string('title',191)->nullable();
            $table->mediumText('stvalue')->nullable();
            $table->timestamps();
        });

    }

    public function down()
    {   
        Schema::dropIfExists('menuvariable');
        Schema::dropIfExists('menu');
    }
}
