<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTopHasSubTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_top_has_sub', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('top_id')->unsigned()->nullable();
            $table->string('top_model')->nullable();
            $table->integer('sub_id')->unsigned()->nullable();
            $table->string('sub_model')->nullable();
            $table->text('props')->nullable();
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->integer('order');
            $table->timestamps();

            /*
            $table->string('top_type', 16)->default('menu')->comment('menu, content');
            $table->integer('top_id')->unsigned()->nullable();
            $table->string('sub_type', 16)->default('content')->comment('menu, content');
            $table->integer('sub_id')->unsigned()->nullable();
            */
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists('new_top_has_sub');

    }
}
