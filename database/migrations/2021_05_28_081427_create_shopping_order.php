<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_order', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_no',30);
            $table->integer('user_id')->unsigned()->nullable();;
            $table->string('email',30);
            $table->longText('order_content');
            $table->longText('address_information');
            $table->string('status');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_order');

    }
}
