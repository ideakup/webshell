<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserLoginModuleOnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->where('type','module-user')->delete();
        DB::table('menuvariable')->where('name','user')->delete();
        DB::table('new_top_has_sub')->where('order',2000000)->delete();

        DB::table('content')->where('type','module-user')->delete();
        DB::table('contentvariable')->where('title','profile')->delete();
        DB::table('contentvariable')->where('title','login')->delete();
        DB::table('contentvariable')->where('title','orders')->delete();
        DB::table('contentvariable')->where('title','forgotpassword')->delete();

       	$menu_id=DB::table('menu')->insertGetId([
			'type' => 'module-user',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('new_top_has_sub')->insert([
            'top_id' => null,
            'top_model' => null,  
            'sub_id' => $menu_id,    
            'sub_model' => 'Module',
            'props' => '{"description":null,"position":"none","headertheme":"light","slidertype":"no","listtype":"normal","dropdowntype":"normal","breadcrumbvisible":"no","asidevisible":"yes"}',
            'status' => 'active',
            'deleted' => 'no',
            'order' => 2000000,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('menuvariable')->insert([
        	'id' => $menu_id,
        	'menu_id' => $menu_id,	
        	'lang_code' => 'tr',	
			'name' => 'user',
			'slug' => 'user',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $login=DB::table('content')->insertGetId([      
            'type' => 'module-user',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('contentvariable')->insert([
            'id' =>  $login,     
            'content_id' =>  $login,
            'lang_code' => 'tr',    
            'slug' => 'login',
            'title' => 'login',
            'content' => 'user-login',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        $profile=DB::table('content')->insertGetId([      
            'type' => 'module-user',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('contentvariable')->insert([
            'id' => $profile,     
            'content_id' => $profile,
            'lang_code' => 'tr',    
            'slug' => 'profile',
            'title' => 'profile',
            'content' => 'user-profile',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        $orders=DB::table('content')->insertGetId([      
            'type' => 'module-user',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('contentvariable')->insert([
            'id' => $orders,     
            'content_id' => $orders,
            'lang_code' => 'tr',    
            'slug' => 'orders',
            'title' => 'orders',
            'content' => 'user-orders',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        $forgotpassword=DB::table('content')->insertGetId([      
            'type' => 'module-user',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('contentvariable')->insert([
            'id' => $forgotpassword,     
            'content_id' => $forgotpassword,
            'lang_code' => 'tr',    
            'slug' => 'forgotpassword',
            'title' => 'forgotpassword',
            'content' => 'user-forgotpassword',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->command->info('User Modulü Açıldı');
    }
}
