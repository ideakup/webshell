<?php

use Illuminate\Database\Seeder;

class ShopModuleOffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->where('type','module-shop')->delete();
        DB::table('menuvariable')->where('name','shop')->delete();
        DB::table('new_top_has_sub')->where('order',1000000)->delete();

        DB::table('content')->where('type','module-shop')->delete();
        DB::table('contentvariable')->where('title','cart')->delete();
        DB::table('contentvariable')->where('title','checkout')->delete();
        DB::table('contentvariable')->where('title','islogin')->delete();
        DB::table('contentvariable')->where('title','addressform')->delete();
        DB::table('contentvariable')->where('title','payment')->delete();

        $this->command->info('Shop Modulü Kapatıldı');

    }
}
