-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1:3306
-- Üretim Zamanı: 17 Şub 2021, 10:32:06
-- Sunucu sürümü: 10.4.10-MariaDB
-- PHP Sürümü: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `default_webshell`
--

--
-- Tablo döküm verisi `content`
--

INSERT INTO `content` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'text', '2021-02-12 08:58:09', '2021-02-12 08:58:09'),
(2, 'text', '2021-02-12 08:59:13', '2021-02-12 08:59:13'),
(3, 'text', '2021-02-12 09:01:40', '2021-02-12 09:01:40'),
(4, 'text', '2021-02-12 09:02:46', '2021-02-12 09:02:46'),
(5, 'text', '2021-02-12 09:03:52', '2021-02-12 09:03:52'),
(6, 'photo', '2021-02-12 09:09:44', '2021-02-12 09:09:44'),
(7, 'photo', '2021-02-12 09:12:58', '2021-02-12 09:12:58'),
(8, 'photo', '2021-02-12 09:13:40', '2021-02-12 09:13:40'),
(9, 'photo', '2021-02-12 09:13:50', '2021-02-12 09:13:50'),
(10, 'photo', '2021-02-12 09:13:59', '2021-02-12 09:13:59'),
(11, 'photogallery', '2021-02-12 09:17:05', '2021-02-12 09:17:05'),
(12, 'photogallery', '2021-02-12 09:18:39', '2021-02-12 09:18:39'),
(13, 'photogallery', '2021-02-12 09:20:14', '2021-02-12 09:20:14'),
(14, 'link', '2021-02-12 09:35:55', '2021-02-12 09:35:55'),
(15, 'link', '2021-02-12 09:36:46', '2021-02-12 09:36:46'),
(16, 'slide', '2021-02-12 09:38:22', '2021-02-12 09:38:22'),
(17, 'seperator', '2021-02-12 09:40:26', '2021-02-12 09:40:26'),
(18, 'seperator', '2021-02-12 10:06:34', '2021-02-12 10:06:34'),
(19, 'seperator', '2021-02-12 10:07:03', '2021-02-12 10:07:03'),
(20, 'seperator', '2021-02-12 10:08:08', '2021-02-12 10:08:08'),
(21, 'mapturkey', '2021-02-12 10:15:12', '2021-02-12 10:15:12'),
(22, 'form', '2021-02-12 10:19:53', '2021-02-12 10:19:53'),
(23, 'code', '2021-02-12 10:31:37', '2021-02-12 10:31:37'),
(24, 'section', '2021-02-12 10:38:38', '2021-02-12 10:38:38'),
(25, 'section', '2021-02-12 10:39:55', '2021-02-12 10:39:55'),
(26, 'photo', '2021-02-12 10:41:35', '2021-02-12 10:41:35'),
(27, 'text', '2021-02-12 10:44:07', '2021-02-12 10:44:07'),
(28, 'seperator', '2021-02-12 10:45:36', '2021-02-12 10:45:36'),
(29, 'photo', '2021-02-12 10:46:28', '2021-02-12 10:46:28'),
(30, 'text', '2021-02-12 10:47:13', '2021-02-12 10:47:13'),
(31, 'seperator', '2021-02-12 10:48:45', '2021-02-12 10:48:45'),
(32, 'section', '2021-02-12 10:49:22', '2021-02-12 10:49:22'),
(33, 'text', '2021-02-12 10:55:48', '2021-02-12 10:55:48'),
(34, 'text', '2021-02-15 04:43:35', '2021-02-15 04:43:35'),
(35, 'photo', '2021-02-15 04:44:54', '2021-02-15 04:44:54'),
(36, 'group', '2021-02-15 04:52:21', '2021-02-15 04:52:21'),
(37, 'group', '2021-02-15 04:52:38', '2021-02-15 04:52:38'),
(38, 'group', '2021-02-15 04:52:50', '2021-02-15 04:52:50'),
(39, 'text', '2021-02-15 05:14:18', '2021-02-15 05:14:18'),
(40, 'section', '2021-02-15 05:15:27', '2021-02-15 05:15:27'),
(41, 'section', '2021-02-15 05:22:22', '2021-02-15 05:22:22'),
(42, 'text', '2021-02-15 05:23:13', '2021-02-15 05:23:13'),
(43, 'group', '2021-02-15 06:21:53', '2021-02-15 06:21:53'),
(44, 'group', '2021-02-15 06:22:03', '2021-02-15 06:22:03'),
(45, 'group', '2021-02-15 06:22:12', '2021-02-15 06:22:12'),
(46, 'group', '2021-02-15 06:22:22', '2021-02-15 06:22:22'),
(47, 'text', '2021-02-15 06:27:34', '2021-02-15 06:27:34'),
(48, 'section', '2021-02-15 06:30:56', '2021-02-15 06:30:56'),
(49, 'photo', '2021-02-15 06:31:23', '2021-02-15 06:31:23'),
(50, 'text', '2021-02-15 06:40:35', '2021-02-15 06:40:35'),
(51, 'group', '2021-02-15 06:47:39', '2021-02-15 06:47:39'),
(52, 'group', '2021-02-15 06:47:52', '2021-02-15 06:47:52'),
(53, 'group', '2021-02-15 06:48:04', '2021-02-15 06:48:04'),
(54, 'group', '2021-02-15 08:16:41', '2021-02-15 08:16:41'),
(55, 'group', '2021-02-15 08:16:53', '2021-02-15 08:16:53'),
(56, 'group', '2021-02-15 08:17:00', '2021-02-15 08:17:00'),
(57, 'group', '2021-02-15 08:17:10', '2021-02-15 08:17:10'),
(58, 'group', '2021-02-15 08:17:18', '2021-02-15 08:17:18'),
(59, 'group', '2021-02-15 08:17:27', '2021-02-15 08:17:27'),
(60, 'group', '2021-02-15 08:17:37', '2021-02-15 08:17:37'),
(61, 'group', '2021-02-15 08:17:47', '2021-02-15 08:17:47'),
(62, 'group', '2021-02-15 08:17:56', '2021-02-15 08:17:56'),
(63, 'group', '2021-02-15 08:18:06', '2021-02-15 08:18:06'),
(64, 'seperator', '2021-02-15 08:30:20', '2021-02-15 08:30:20'),
(65, 'section', '2021-02-15 08:32:32', '2021-02-15 08:32:32'),
(66, 'text', '2021-02-15 08:35:38', '2021-02-15 08:35:38'),
(67, 'group', '2021-02-15 09:50:38', '2021-02-15 09:50:38'),
(68, 'group', '2021-02-15 09:50:56', '2021-02-15 09:50:56'),
(69, 'group', '2021-02-15 09:51:11', '2021-02-15 09:51:11'),
(70, 'group', '2021-02-15 11:04:19', '2021-02-15 11:04:19'),
(71, 'group', '2021-02-15 11:04:31', '2021-02-15 11:04:31'),
(72, 'group', '2021-02-15 11:05:48', '2021-02-15 11:05:48'),
(73, 'group', '2021-02-15 11:05:59', '2021-02-15 11:05:59'),
(111, 'section', '2021-02-17 05:51:38', '2021-02-17 05:51:38'),
(110, 'text', '2021-02-17 05:19:58', '2021-02-17 05:19:58'),
(109, 'text', '2021-02-16 11:37:47', '2021-02-16 11:37:47');

--
-- Tablo döküm verisi `contentvariable`
--

INSERT INTO `contentvariable` (`id`, `content_id`, `lang_code`, `title`, `slug`, `short_content`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'tr', 'metin', '', NULL, '<p><strong>COL-12</strong></p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam molestie mi sed augue tempor, ac cursus nisl efficitur. Donec eu odio arcu. Sed libero metus, sodales quis eros in, aliquam fringilla mauris. Cras viverra dolor ut sagittis placerat. Curabitur eget erat id est luctus laoreet sit amet at arcu. Donec magna mi, rutrum id dui in, condimentum egestas massa. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc dignissim eros sed risus ultrices tempor blandit vehicula magna. Aenean imperdiet quis leo eu pretium. Nullam et magna sit amet purus commodo vestibulum. Nullam eu nisl mi. Nam fringilla vel est ut mattis.</p>\r\n<p>Cras arcu quam, euismod quis mi et, molestie posuere est. Sed molestie sit amet leo eget condimentum. Quisque suscipit orci lobortis volutpat cursus. Donec id ligula at ex facilisis bibendum. Sed aliquet libero quis consectetur interdum. Aliquam hendrerit, diam id tempor imperdiet, urna mi vestibulum magna, at facilisis nisi ante ut sapien. Duis sem tortor, commodo at posuere eu, auctor a tortor. Fusce tincidunt urna est, facilisis semper elit imperdiet ut. Vestibulum lectus nunc, viverra id eros in, pulvinar vehicula nulla. Phasellus at consequat elit. Suspendisse consequat ipsum vitae odio efficitur ultricies. Pellentesque fermentum sagittis sem, ut feugiat leo gravida at.</p>\r\n<p>Donec vel faucibus augue, a ornare risus. Nunc consectetur massa in purus volutpat lobortis. Aliquam eleifend sapien sit amet sodales dapibus. Nullam sed suscipit purus. In vel nunc tristique, fringilla augue ac, dignissim arcu. Sed eu magna viverra, tempor nulla eu, molestie nulla. Nulla facilisi. Maecenas convallis lacus quis fermentum accumsan.</p>\r\n<p>&nbsp;</p>', '2021-02-12 08:58:09', '2021-02-12 09:01:12'),
(4, 4, 'tr', 'METİN 1/3', '', NULL, '<p><strong>COL 1/3</strong></p>\r\n<p>Donec laoreet dolor ut orci rhoncus rhoncus. Vestibulum dictum neque dolor, ac sagittis dui convallis ut. Quisque ac varius est. Cras lacus nisl, euismod nec lacus eu, sollicitudin consectetur dui. Fusce ut posuere purus, ac consectetur turpis. Sed ex ex, luctus nec purus sit amet, scelerisque tristique ante. Duis luctus elit in iaculis commodo. Fusce vel rhoncus lectus, vel vehicula nisi. Duis quis lacus a massa laoreet laoreet. Praesent faucibus diam at ullamcorper dignissim. Maecenas lobortis congue lorem sed vestibulum.</p>\r\n<p>Nam malesuada eros sed orci bibendum, dignissim vestibulum mauris ultrices. Etiam imperdiet enim risus, eget efficitur dolor pulvinar sed.&nbsp;</p>', '2021-02-12 09:02:46', '2021-02-12 09:03:35'),
(2, 2, 'tr', 'metin2', '', NULL, '<p><strong>COL-6</strong></p>\r\n<p>Donec laoreet dolor ut orci rhoncus rhoncus. Vestibulum dictum neque dolor, ac sagittis dui convallis ut. Quisque ac varius est. Cras lacus nisl, euismod nec lacus eu, sollicitudin consectetur dui. Fusce ut posuere purus, ac consectetur turpis. Sed ex ex, luctus nec purus sit amet, scelerisque tristique ante. Duis luctus elit in iaculis commodo. Fusce vel rhoncus lectus, vel vehicula nisi. Duis quis lacus a massa laoreet laoreet. Praesent faucibus diam at ullamcorper dignissim. Maecenas lobortis congue lorem sed vestibulum.</p>\r\n<p>Nam malesuada eros sed orci bibendum, dignissim vestibulum mauris ultrices. Etiam imperdiet enim risus, eget efficitur dolor pulvinar sed. Vivamus bibendum tellus ac erat malesuada, vel rhoncus est blandit. Etiam sem ipsum, tristique a maximus eget, tincidunt a risus. Fusce faucibus posuere est fringilla volutpat. Duis orci eros, auctor nec bibendum in, ultricies dapibus dui. Nulla facilisi. Aenean eleifend magna et ligula sagittis vehicula. Duis eget mauris vitae ex bibendum fringilla. Morbi vitae nisl varius, ultricies orci eget, lobortis ipsum. Praesent vel nisl ut diam ornare fringilla sit amet ut tortor. Pellentesque et orci ut lorem tincidunt hendrerit. Phasellus et nibh urna. Suspendisse cursus sit amet velit a feugiat. Vestibulum id eleifend mi, eu aliquam felis. Praesent vehicula molestie tortor, ac rutrum lacus fringilla at.</p>', '2021-02-12 08:59:13', '2021-02-12 09:00:26'),
(3, 3, 'tr', 'METİN COL-3/2', '', NULL, '<p><strong>COL-2/3</strong></p>\r\n<p>Donec laoreet dolor ut orci rhoncus rhoncus. Vestibulum dictum neque dolor, ac sagittis dui convallis ut. Quisque ac varius est. Cras lacus nisl, euismod nec lacus eu, sollicitudin consectetur dui. Fusce ut posuere purus, ac consectetur turpis. Sed ex ex, luctus nec purus sit amet, scelerisque tristique ante. Duis luctus elit in iaculis commodo. Fusce vel rhoncus lectus, vel vehicula nisi. Duis quis lacus a massa laoreet laoreet. Praesent faucibus diam at ullamcorper dignissim. Maecenas lobortis congue lorem sed vestibulum.</p>\r\n<p>Nam malesuada eros sed orci bibendum, dignissim vestibulum mauris ultrices. Etiam imperdiet enim risus, eget efficitur dolor pulvinar sed. Vivamus bibendum tellus ac erat malesuada, vel rhoncus est blandit. Etiam sem ipsum, tristique a maximus eget, tincidunt a risus. Fusce faucibus posuere est fringilla volutpat. Duis orci eros, auctor nec bibendum in, ultricies dapibus dui. Nulla facilisi. Aenean eleifend magna et ligula sagittis vehicula. Duis eget mauris vitae ex bibendum fringilla. Morbi vitae nisl varius, ultricies orci eget, lobortis ipsum. Praesent vel nisl ut diam ornare fringilla sit amet ut tortor. Pellentesque et orci ut lorem tincidunt hendrerit. Phasellus et nibh urna. Suspendisse cursus sit amet velit a feugiat. Vestibulum id eleifend mi, eu aliquam felis. Praesent vehicula molestie tortor, ac rutrum lacus fringilla at.</p>', '2021-02-12 09:01:40', '2021-02-12 09:02:18'),
(5, 5, 'tr', 'COL -6 2', '', NULL, '<p><strong>COL-6</strong></p>\r\n<p>Donec laoreet dolor ut orci rhoncus rhoncus. Vestibulum dictum neque dolor, ac sagittis dui convallis ut. Quisque ac varius est. Cras lacus nisl, euismod nec lacus eu, sollicitudin consectetur dui. Fusce ut posuere purus, ac consectetur turpis. Sed ex ex, luctus nec purus sit amet, scelerisque tristique ante. Duis luctus elit in iaculis commodo. Fusce vel rhoncus lectus, vel vehicula nisi. Duis quis lacus a massa laoreet laoreet. Praesent faucibus diam at ullamcorper dignissim. Maecenas lobortis congue lorem sed vestibulum.</p>\r\n<p>Nam malesuada eros sed orci bibendum, dignissim vestibulum mauris ultrices. Etiam imperdiet enim risus, eget efficitur dolor pulvinar sed. Vivamus bibendum tellus ac erat malesuada, vel rhoncus est blandit. Etiam sem ipsum, tristique a maximus eget, tincidunt a risus. Fusce faucibus posuere est fringilla volutpat. Duis orci eros, auctor nec bibendum in, ultricies dapibus dui. Nulla facilisi. Aenean eleifend magna et ligula sagittis vehicula. Duis eget mauris vitae ex bibendum fringilla. Morbi vitae nisl varius, ultricies orci eget, lobortis ipsum. Praesent vel nisl ut diam ornare fringilla sit amet ut tortor. Pellentesque et orci ut lorem tincidunt hendrerit. Phasellus et nibh urna. Suspendisse cursus sit amet velit a feugiat. Vestibulum id eleifend mi, eu aliquam felis. Praesent vehicula molestie tortor, ac rutrum lacus fringilla at.</p>', '2021-02-12 09:03:52', '2021-02-12 09:04:19'),
(6, 6, 'tr', 'foto1', '', NULL, 'slider2_YWzyu.png', '2021-02-12 09:09:44', '2021-02-15 04:33:50'),
(7, 7, 'tr', 'foto2', '', NULL, '2_skhSo.png', '2021-02-12 09:12:58', '2021-02-12 09:13:25'),
(8, 8, 'tr', 'foto 3', NULL, NULL, '3_CTA5c.png', '2021-02-12 09:13:40', '2021-02-12 09:14:30'),
(9, 9, 'tr', 'foto 4', '', NULL, '4_vud2X.png', '2021-02-12 09:13:50', '2021-02-12 09:14:40'),
(10, 10, 'tr', 'foto 5', '', NULL, '5_4ZlNR.png', '2021-02-12 09:13:59', '2021-02-15 04:37:08'),
(11, 11, 'tr', '2 sütün', '', NULL, NULL, '2021-02-12 09:17:05', '2021-02-12 09:19:56'),
(12, 12, 'tr', '3 sütün', '', NULL, NULL, '2021-02-12 09:18:39', '2021-02-12 09:20:02'),
(13, 13, 'tr', '4 sütün', '', NULL, NULL, '2021-02-12 09:20:14', '2021-02-12 09:20:39'),
(14, 14, 'tr', 'dahili link', '', NULL, '{\"content_url\":\"https:\\/\\/www.google.com\\/\",\"content_target\":\"internal\"}', '2021-02-12 09:35:55', '2021-02-12 09:36:28'),
(15, 15, 'tr', 'internal button', '', NULL, '{\"content_url\":\"https:\\/\\/www.google.com\\/\",\"content_target\":\"external\"}', '2021-02-12 09:36:46', '2021-02-12 09:36:54'),
(16, 16, 'tr', 'slide 1', '', NULL, NULL, '2021-02-12 09:38:22', '2021-02-12 09:38:27'),
(17, 17, 'tr', 'sep1', NULL, NULL, NULL, '2021-02-12 09:40:26', '2021-02-12 09:40:26'),
(18, 18, 'tr', 'sep2', NULL, NULL, NULL, '2021-02-12 10:06:34', '2021-02-12 10:06:34'),
(19, 19, 'tr', 'sep3', NULL, NULL, NULL, '2021-02-12 10:07:03', '2021-02-12 10:07:03'),
(20, 20, 'tr', 'sep4', NULL, NULL, NULL, '2021-02-12 10:08:08', '2021-02-12 10:08:08'),
(21, 21, 'tr', 'Türkiye Haritası', 'turkiye-haritasi-tr', NULL, NULL, '2021-02-12 10:15:12', '2021-02-12 10:15:12'),
(22, 22, 'tr', 'form example', '', NULL, '[{\"type\":\"header\",\"subtype\":\"h1\",\"label\":\"örnek form\"},{\"type\":\"paragraph\",\"subtype\":\"p\",\"label\":\"form açıklama\"},{\"type\":\"text\",\"required\":true,\"label\":\"Metin\",\"placeholder\":\"name\",\"className\":\"sm-form-control\",\"name\":\"text-1613136024115\",\"subtype\":\"text\"},{\"type\":\"text\",\"required\":true,\"label\":\"Metin\",\"placeholder\":\"email\",\"className\":\"sm-form-control\",\"name\":\"text-1613136038413\",\"subtype\":\"text\"},{\"type\":\"textarea\",\"required\":true,\"label\":\"Metin Alanı\",\"placeholder\":\"mesajınız\",\"className\":\"sm-form-control\",\"name\":\"textarea-1613136045413\",\"subtype\":\"textarea\",\"rows\":3},{\"type\":\"date\",\"required\":true,\"label\":\"Tarih Alanı\",\"placeholder\":\"doğum tarihi\",\"className\":\"form-control\",\"name\":\"date-1613136066943\"}]', '2021-02-12 10:19:53', '2021-02-12 10:22:31'),
(23, 23, 'tr', 'code', '', NULL, '<iframe style=\"width:100%; height:700px;\" src=\"temel-icerikler-tr\" ></iframe>', '2021-02-12 10:31:37', '2021-02-12 10:36:11'),
(24, 24, 'tr', 'section fullwitdh', '', NULL, '{}', '2021-02-12 10:38:38', '2021-02-12 10:40:09'),
(25, 25, 'tr', 'section container', 'section-container-tr', NULL, '{}', '2021-02-12 10:39:55', '2021-02-12 10:39:55'),
(26, 26, 'tr', 'foto', '', NULL, 'imac_E6HBB.png', '2021-02-12 10:41:35', '2021-02-12 10:44:58'),
(27, 27, 'tr', 'metin', '', NULL, '<div class=\"heading-block topmargin\">\r\n<h2>RETINA DEVICE READY.</h2>\r\nFabulously Sharp &amp; Intuitive on your HD Devices.</div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus deserunt, nobis quae eos provident quidem. Quaerat expedita dignissimos perferendis, nihil quo distinctio eius architecto reprehenderit maiores.</p>\r\n<p><a class=\"button button-border button-rounded button-large noleftmargin topmargin-sm\" href=\"#\">EXPERIENCE MORE</a></p>', '2021-02-12 10:44:07', '2021-02-12 10:44:29'),
(28, 28, 'tr', 'sep', NULL, NULL, NULL, '2021-02-12 10:45:36', '2021-02-12 10:45:36'),
(29, 29, 'tr', 'fullwidthfoto', '', NULL, 'imac_nDc7l.png', '2021-02-12 10:46:28', '2021-02-12 10:47:44'),
(30, 30, 'tr', 'full metin', '', NULL, '<div class=\"heading-block topmargin\">\r\n<h2>RETINA DEVICE READY.</h2>\r\nFabulously Sharp &amp; Intuitive on your HD Devices.</div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus deserunt, nobis quae eos provident quidem. Quaerat expedita dignissimos perferendis, nihil quo distinctio eius architecto reprehenderit maiores.</p>\r\n<p><a class=\"button button-border button-rounded button-large noleftmargin topmargin-sm\" href=\"#\">EXPERIENCE MORE</a></p>', '2021-02-12 10:47:13', '2021-02-12 10:47:29'),
(31, 31, 'tr', 'sep', NULL, NULL, NULL, '2021-02-12 10:48:45', '2021-02-12 10:48:45'),
(32, 32, 'tr', 'section back img', 'section-back-img-tr', NULL, '{\"section_bg_img\":\"slider_5_qj7NM.png\"}', '2021-02-12 10:49:22', '2021-02-12 10:55:06'),
(33, 33, 'tr', 'as', '', NULL, '<div class=\"heading-block topmargin-sm\">\r\n<h3 style=\"text-align: center;\">OPTIMIZED FOR MOBILE &amp; TOUCH ENABLED DEVICES.</h3>\r\n</div>\r\n<p style=\"text-align: center;\"><strong>Nam vehicula rutrum ultricies. Aenean non mi in nisl pellentesque dapibus a sed purus. Curabitur id fermentum odio, vitae vehicula orci. Nulla accumsan hendrerit dictum. Morbi nec felis vel leo consequat aliquam quis non ligula. Vestibulum laoreet pretium varius. Integer fermentum, nulla et viverra rutrum, nisl mauris laoreet sapien, eu hendrerit justo lacus sit amet odio. Proin vitae urna semper, efficitur felis ut, cursus ante. Curabitur molestie arcu at interdum rhoncus. Praesent nisl eros, sodales quis condimentum ut, varius ac dolor. Aenean bibendum lectus eget lectus ullamcorper scelerisque. Nam lacinia, sapien ac posuere pulvinar, mi urna porta dui, nec ornare est purus ut urna. Donec molestie purus a mi facilisis tempor. Praesent sodales tortor in iaculis suscipit.</strong></p>\r\n<p style=\"text-align: center;\"><a class=\"button button-border button-dark button-rounded button-large noleftmargin topmargin-sm\" href=\"#\">LEARN MORE</a></p>', '2021-02-12 10:55:48', '2021-02-17 05:26:38'),
(34, 34, 'tr', 'html', '', NULL, '<div class=\"heading-block topmargin\">\r\n<h1>Lorem ipsum dolor sit amet.</h1>\r\n</div>\r\n<p class=\"lead\">Consectetur adipiscing elit. Etiam ullamcorper justo vel viverra porttitor. Mauris sagittis non erat ac tempor.</p>', '2021-02-15 04:43:35', '2021-02-15 04:48:31'),
(35, 35, 'tr', 'foto', '', NULL, '6_3_OdDNg.png', '2021-02-15 04:44:54', '2021-02-15 04:47:48'),
(36, 36, 'tr', 'King', 'icon1-tr', NULL, '{\"short_content\":\"Duis placerat nibh nunc, varius vehicula nisl euismod id.Phasellus semper ligula at augue bibendum bibendum.\",\"icon\":\"fas fa-chess-king\"}', '2021-02-15 04:52:21', '2021-02-15 06:53:24'),
(37, 37, 'tr', 'Queen', 'icon2-tr', NULL, '{\"short_content\":\"Sed blandit odio ante, nec commodo ligula dignissim sed. Integer id mi sit amet urna ultrices condimentum bibendum.\",\"icon\":\"fas fa-chess-queen\"}', '2021-02-15 04:52:38', '2021-02-15 06:53:52'),
(38, 38, 'tr', 'Knight', 'knight', NULL, '{\"short_content\":\"Nam nulla nulla, porta eget facilisis sit amet, maximus vitae nibh. Fusce aliquet vitae elit quis dictum.\",\"icon\":\"fas fa-chess-knight\"}', '2021-02-15 04:52:50', '2021-02-15 07:00:41'),
(39, 39, 'tr', 'patlama', NULL, NULL, NULL, '2021-02-15 05:14:18', '2021-02-15 05:14:18'),
(40, 40, 'tr', 'section 1 anasayfa', 'section-1-anasayfa-tr', NULL, '{}', '2021-02-15 05:15:27', '2021-02-15 05:15:27'),
(41, 41, 'tr', 'section1 anasayfa', 'section1-anasayfa-tr', NULL, '{\"section_bg_img\":\"home_testi_bg_Rv7qo.png\"}', '2021-02-15 05:22:22', '2021-02-15 06:13:56'),
(42, 42, 'tr', 'sectipn content', '', NULL, '<h2 style=\"text-align: center; color: white;\">Vestibulum congue eros ex?</h2>\r\n<p style=\"text-align: center; color: white;\">\"Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!\"</p>', '2021-02-15 05:23:13', '2021-02-15 06:00:38'),
(43, 43, 'tr', 'blog 1', 'blog-1-tr', NULL, '{\"photo\":\"1_kYBqT.png\",\"short_content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ullamcorper justo vel viverra porttitor.\"}', '2021-02-15 06:21:53', '2021-02-15 08:09:42'),
(44, 44, 'tr', 'blog 2', 'blog-2-tr', NULL, '{\"photo\":\"2_jL4VF.png\",\"short_content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ullamcorper justo vel viverra porttitor.\"}', '2021-02-15 06:22:03', '2021-02-15 08:10:05'),
(45, 45, 'tr', 'blog 3', 'blog-3-tr', NULL, '{\"photo\":\"22_2mUKI.png\",\"short_content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ullamcorper justo vel viverra porttitor.\"}', '2021-02-15 06:22:12', '2021-02-15 08:10:13'),
(46, 46, 'tr', 'blog 4', 'blog-4-tr', NULL, '{\"photo\":\"13_7KVT0.png\",\"short_content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ullamcorper justo vel viverra porttitor.\"}', '2021-02-15 06:22:22', '2021-02-15 08:10:23'),
(59, 59, 'tr', 'marka 6', 'marka-6-tr', NULL, '{\"photo\":\"6_aP99w.png\",\"img_url\":\"https:\\/\\/www.google.com\\/\"}', '2021-02-15 08:17:27', '2021-02-15 08:19:59'),
(57, 57, 'tr', 'marka 4', 'marka-4-tr', NULL, '{\"photo\":\"4_N7ZU2.png\",\"img_url\":\"https:\\/\\/www.google.com\\/\"}', '2021-02-15 08:17:10', '2021-02-15 08:19:46'),
(58, 58, 'tr', 'marka 5', 'marka-5-tr', NULL, '{\"photo\":\"5_ucY15.png\",\"img_url\":\"https:\\/\\/www.google.com\\/\"}', '2021-02-15 08:17:18', '2021-02-15 08:19:34'),
(56, 56, 'tr', 'marka 3', 'marka-3-tr', NULL, '{\"photo\":\"3_ftgUu.png\",\"img_url\":\"https:\\/\\/www.google.com\\/\"}', '2021-02-15 08:17:00', '2021-02-15 08:19:22'),
(54, 54, 'tr', 'marka1', 'marka1-tr', NULL, '{\"photo\":\"1_Utdb4.png\",\"img_url\":\"https:\\/\\/www.google.com\\/\"}', '2021-02-15 08:16:41', '2021-02-15 08:21:07'),
(55, 55, 'tr', 'marka 2', 'marka-2-tr', NULL, '{\"photo\":\"2_kUmOW.png\",\"img_url\":\"https:\\/\\/www.google.com\\/\"}', '2021-02-15 08:16:53', '2021-02-15 08:19:10'),
(47, 47, 'tr', 'blog 1 metin', '', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ullamcorper justo vel viverra porttitor. Mauris sagittis non erat ac tempor. Donec laoreet quis nulla ut sodales. Fusce iaculis libero non viverra sodales. Duis rutrum a augue in porttitor. Aenean commodo lacus at sem luctus, quis tempor velit vehicula. Mauris tempor aliquam cursus. Sed turpis neque, maximus nec euismod sed, consequat eget mauris. Sed pharetra, risus ac tincidunt mollis, enim urna fringilla lacus, quis ultrices mauris risus et odio. Fusce sollicitudin mauris ac tempor lobortis. Nam ut dignissim nisi. Aenean vel laoreet lorem. Vestibulum finibus pulvinar nisi, eget rhoncus mi fringilla quis. Praesent sit amet turpis vel nisl maximus interdum eget eget lacus. Nam iaculis eu nisi id luctus.</p>\r\n<p>Vivamus quis mauris augue. Aliquam vulputate condimentum nunc a lobortis. Donec rhoncus, massa in varius dapibus, ante justo elementum ipsum, et tincidunt velit felis eget augue. Maecenas vitae sem et sem aliquet ultrices in eget elit. Sed et tortor odio. Proin tincidunt tempor arcu sagittis hendrerit. Integer lacinia arcu at augue feugiat, eget feugiat ex convallis. Nam in justo eleifend, bibendum erat at, tincidunt felis. Vivamus finibus scelerisque enim, nec lobortis nunc placerat nec.</p>\r\n<p>Duis placerat nibh nunc, varius vehicula nisl euismod id. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris at quam id dolor dictum iaculis ut in libero. Praesent vel urna tortor. Nam nulla nulla, porta eget facilisis sit amet, maximus vitae nibh. Fusce aliquet vitae elit quis dictum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam porta velit ut commodo fringilla. Nullam ultrices massa et nisi pretium, at tincidunt metus sollicitudin. Vestibulum malesuada scelerisque nisl, sit amet scelerisque ipsum mattis id.</p>\r\n<p>Ut at pharetra libero. Pellentesque dignissim mauris eu convallis posuere. Fusce sed tortor at nisl commodo mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc quis massa et augue porta imperdiet quis at neque. Nullam accumsan pellentesque sem nec mattis. Nam porta lorem eu magna tristique, vel aliquam sapien aliquet. Aliquam maximus eros non finibus sagittis. Nullam eleifend dui eget efficitur porta. Vestibulum congue eros ex, vel cursus nulla interdum ac. Nulla non lobortis libero, id vestibulum ante. Suspendisse pretium elit ultricies odio commodo, eget egestas dolor semper. Cras sed tortor vitae purus pellentesque volutpat.</p>\r\n<p>Sed blandit odio ante, nec commodo ligula dignissim sed. Integer id mi sit amet urna ultrices condimentum sed quis sem. Sed gravida tellus metus, eget facilisis tellus tristique sit amet. Pellentesque eget cursus nisi. Vestibulum placerat aliquam massa, nec lacinia dui tempus id. Mauris cursus blandit sapien vitae tristique. Phasellus semper ligula at augue bibendum bibendum. Vivamus malesuada varius lorem, sit amet convallis dui. Aliquam viverra consequat lacus id blandit.</p>', '2021-02-15 06:27:34', '2021-02-15 06:28:07'),
(48, 48, 'tr', 'section2', 'section2-tr', NULL, '{}', '2021-02-15 06:30:56', '2021-02-15 06:30:56'),
(49, 49, 'tr', 'section foto', '', NULL, 'modal1_8aTuN.png', '2021-02-15 06:31:23', '2021-02-15 06:45:15'),
(50, 50, 'tr', 'section html', '', NULL, '<h4 style=\"text-align: center;\">&nbsp;</h4>\r\n<h4 style=\"text-align: center;\">&nbsp;</h4>\r\n<h4 style=\"text-align: center;\"><strong>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. </strong></h4>\r\n<h4 style=\"text-align: center;\"><strong>Etiam ullamcorper justo vel viverra porttitor.Mauris sagittis non erat ac tempor.</strong></h4>\r\n<h4 style=\"text-align: center;\"><strong> Donec laoreet quis nulla ut sodales. Fusce iaculis libero non viverra sodales.\"</strong></h4>\r\n<h4 style=\"text-align: center;\">&nbsp;</h4>', '2021-02-15 06:40:35', '2021-02-15 06:45:43'),
(51, 51, 'tr', 'Bishop', 'icon-4-tr', NULL, '{\"short_content\":\"Duis placerat nibh nunc, varius vehicula nisl euismod id. Class aptent taciti sociosqu ad litora torquent per conubia nostra.\",\"icon\":\"fas fa-chess-bishop\"}', '2021-02-15 06:47:39', '2021-02-15 06:54:48'),
(52, 52, 'tr', 'Rook', 'icon-5-tr', NULL, '{\"short_content\":\"Mauris at quam id dolor dictum iaculis ut in libero. Praesent vel urna tortor. Nam nulla nulla, porta eget facilisis sit amet.\",\"icon\":\"fas fa-chess-rook\"}', '2021-02-15 06:47:52', '2021-02-15 06:55:21'),
(53, 53, 'tr', 'Pawn', 'icon-6-tr', NULL, '{\"short_content\":\"Ut at pharetra libero. Pellentesque dignissim mauris eu convallis posuere. Fusce sed tortor at nisl commodo mollis.\",\"icon\":\"fas fa-chess-pawn\"}', '2021-02-15 06:48:04', '2021-02-15 06:55:41'),
(60, 60, 'tr', 'marka 7', 'marka-7-tr', NULL, '{\"photo\":\"7_A1sUm.png\",\"img_url\":\"https:\\/\\/www.google.com\\/\"}', '2021-02-15 08:17:37', '2021-02-15 08:20:11'),
(61, 61, 'tr', 'marka 8', 'marka-8-tr', NULL, '{\"photo\":\"8_Js8vZ.png\",\"img_url\":\"https:\\/\\/www.google.com\\/\"}', '2021-02-15 08:17:47', '2021-02-15 08:20:22'),
(62, 62, 'tr', 'marka  9', 'marka-9-tr', NULL, '{\"photo\":\"9_e4Hpu.png\",\"img_url\":\"https:\\/\\/www.google.com\\/\"}', '2021-02-15 08:17:56', '2021-02-15 08:20:33'),
(63, 63, 'tr', 'marka 10', 'marka-10-tr', NULL, '{\"photo\":\"10_rbn91.png\",\"img_url\":\"https:\\/\\/www.google.com\\/\"}', '2021-02-15 08:18:06', '2021-02-15 08:20:49'),
(64, 64, 'tr', 'sep', NULL, NULL, NULL, '2021-02-15 08:30:20', '2021-02-15 08:30:20'),
(65, 65, 'tr', 'section 3 anasayfa', 'section-3-anasayfa-tr', NULL, '{\"section_bg_img\":\"6_QO9ue.png\"}', '2021-02-15 08:32:32', '2021-02-15 08:49:33'),
(66, 66, 'tr', 'paralax html', '', NULL, '<h2 style=\"text-align: center;\"><span style=\"color: #000000;\"><strong>Vestibulum congue eros ex?</strong></span></h2>\r\n<h4 style=\"text-align: center;\"><span style=\"color: #000000;\">\"Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!\"</span></h4>', '2021-02-15 08:35:38', '2021-02-15 08:50:58'),
(67, 67, 'tr', 'person 1', 'person1-tr', NULL, '{\"photo\":\"2_fMuyv.png\",\"stafftype\":\"kurucu_avukat\",\"emp_title\":\"Kurucu Avukat\",\"cv\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>&nbsp;<\\/p>\"}', '2021-02-15 09:50:38', '2021-02-15 09:58:55'),
(68, 68, 'tr', 'person 2', 'person-2-tr', NULL, '{\"photo\":\"3_fI9IO.png\",\"stafftype\":\"avukat\",\"emp_title\":\"Avukat\",\"cv\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>&nbsp;<\\/p>\"}', '2021-02-15 09:50:56', '2021-02-15 09:59:01'),
(69, 69, 'tr', 'person 3', 'person-3-tr', NULL, '{\"photo\":\"4_1ejOC.png\",\"stafftype\":\"personel\",\"emp_title\":\"Koordinat\\u00f6r\",\"cv\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit<\\/p>\\r\\n<p>&nbsp;<\\/p>\\r\\n<p>&nbsp;<\\/p>\"}', '2021-02-15 09:51:11', '2021-02-15 09:59:52'),
(72, 72, 'tr', 'product 3', 'product-3-tr', NULL, '{\"photo\":\"1_1_GZ6xM.png\",\"short_content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\"price\":\"99.99\",\"discounted_price\":\"80.00\"}', '2021-02-15 11:05:48', '2021-02-15 11:10:28'),
(71, 71, 'tr', 'product 2', 'product-2-tr', NULL, '{\"photo\":\"2_1_brJux.png\",\"short_content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\"price\":\"55.77\",\"discounted_price\":null}', '2021-02-15 11:04:31', '2021-02-15 11:09:49'),
(70, 70, 'tr', 'product1', 'product1-tr', NULL, '{\"photo\":\"1_X4j5o.png\",\"short_content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\"price\":\"40.99\",\"discounted_price\":null}', '2021-02-15 11:04:19', '2021-02-15 11:09:28'),
(73, 73, 'tr', 'product 4', 'product-4-tr', NULL, '{\"photo\":\"12_cG89K.png\",\"short_content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\",\"price\":\"250.00\",\"discounted_price\":\"189.00\"}', '2021-02-15 11:05:59', '2021-02-15 11:11:14'),
(110, 110, 'tr', 'patlama', NULL, NULL, NULL, '2021-02-17 05:19:58', '2021-02-17 05:19:58'),
(111, 111, 'tr', 'drction', 'drction-tr', NULL, '{}', '2021-02-17 05:51:38', '2021-02-17 05:51:38'),
(109, 109, 'tr', 'patlama', NULL, NULL, NULL, '2021-02-16 11:37:47', '2021-02-16 11:37:47');

--
-- Tablo döküm verisi `content_photogalleryvariable`
--

INSERT INTO `content_photogalleryvariable` (`id`, `content_id`, `lang`, `name`, `description`, `url`, `order`, `status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 11, 'tr', NULL, NULL, '1_Sf8eZ.png', 1000, 'active', 'no', '2021-02-12 09:17:16', '2021-02-12 09:17:16'),
(2, 11, 'tr', NULL, NULL, '2_JP0wl.png', 1000, 'active', 'no', '2021-02-12 09:17:20', '2021-02-12 09:17:20'),
(3, 11, 'tr', NULL, NULL, '3_JIs2k.png', 1000, 'active', 'no', '2021-02-12 09:17:24', '2021-02-12 09:17:24'),
(4, 11, 'tr', NULL, NULL, '4_WgebG.png', 1000, 'active', 'no', '2021-02-12 09:17:28', '2021-02-12 09:17:28'),
(5, 11, 'tr', NULL, NULL, '5_PemUj.png', 1000, 'active', 'no', '2021-02-12 09:17:32', '2021-02-12 09:17:32'),
(6, 12, 'tr', NULL, NULL, '5_ZwVGT.png', 1000, 'active', 'no', '2021-02-12 09:18:48', '2021-02-12 09:18:48'),
(7, 12, 'tr', NULL, NULL, '4_mkNwZ.png', 1000, 'active', 'no', '2021-02-12 09:18:52', '2021-02-12 09:18:52'),
(8, 12, 'tr', NULL, NULL, '3_wpzkU.png', 1000, 'active', 'no', '2021-02-12 09:18:56', '2021-02-12 09:18:56'),
(9, 12, 'tr', NULL, NULL, '2_t0nvW.png', 1000, 'active', 'no', '2021-02-12 09:19:00', '2021-02-12 09:19:00'),
(10, 12, 'tr', NULL, NULL, '1_vSQgk.png', 1000, 'active', 'no', '2021-02-12 09:19:04', '2021-02-12 09:19:04'),
(11, 13, 'tr', NULL, NULL, '1_dLncg.png', 1000, 'active', 'no', '2021-02-12 09:20:23', '2021-02-12 09:20:23'),
(12, 13, 'tr', NULL, NULL, '2_SaumE.png', 1000, 'active', 'no', '2021-02-12 09:20:27', '2021-02-12 09:20:27'),
(13, 13, 'tr', NULL, NULL, '3_sK6yW.png', 1000, 'active', 'no', '2021-02-12 09:20:31', '2021-02-12 09:20:31'),
(14, 13, 'tr', NULL, NULL, '4_4cTYc.png', 1000, 'active', 'no', '2021-02-12 09:20:34', '2021-02-12 09:20:34'),
(15, 13, 'tr', NULL, NULL, '5_6zb7O.png', 1000, 'active', 'no', '2021-02-12 09:20:38', '2021-02-12 09:20:38');

--
-- Tablo döküm verisi `content_slidevariable`
--

INSERT INTO `content_slidevariable` (`id`, `content_id`, `lang_code`, `type`, `theme`, `align`, `title`, `description`, `button_text`, `button_url`, `image_url`, `order`, `status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 16, 'tr', 'img', 'light', 'left', '', NULL, NULL, NULL, 'slider2_Sts79.png', 1000, 'active', 'no', '2021-02-12 09:38:42', '2021-02-12 09:38:42'),
(2, 16, 'tr', 'img', 'light', 'left', '', NULL, NULL, NULL, 'slider3_fKnSB.png', 1000, 'active', 'no', '2021-02-12 09:38:51', '2021-02-12 09:38:51'),
(3, 16, 'tr', 'img', 'light', 'left', '', NULL, NULL, NULL, 'slider_5_VtvnV.png', 1000, 'active', 'no', '2021-02-12 09:39:00', '2021-02-12 09:39:00');

--
-- Tablo döküm verisi `formdata`
--

INSERT INTO `formdata` (`id`, `form_id`, `source_type`, `source_id`, `lang_code`, `data`, `visible`, `created_at`, `updated_at`) VALUES
(1, 22, 'menu', 9, 'tr', '{\"text-1613136024115\":\"uygar\",\"text-1613136038413\":\"ayd\\u0131n\",\"textarea-1613136045413\":\"alsdkaskdal\\u015fskdla\\u015fsd\",\"date-1613136066943\":\"2021-02-19\"}', 'no', '2021-02-12 10:28:30', '2021-02-12 10:28:30');

--
-- Tablo döküm verisi `map_staff`
--

INSERT INTO `map_staff` (`id`, `content_id`, `district_id`, `country_id`, `city_id`, `county_id`, `order`, `status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 21, NULL, NULL, 6, 64, 10, 'active', 'no', '2021-02-12 10:16:50', '2021-02-12 10:16:50');

--
-- Tablo döküm verisi `map_staffvariable`
--

INSERT INTO `map_staffvariable` (`id`, `staff_id`, `lang_code`, `name`, `slug`, `decription`, `title`, `photo_url`, `address`, `email`, `phone`, `gsm`, `created_at`, `updated_at`) VALUES
(1, 1, 'tr', 'XXXXX XXXXX', 'xxxxx-xxxxx', NULL, 'bolgemuduru', '', 'ANKARA', 'xxxx@gmail.com', NULL, NULL, '2021-02-12 10:16:50', '2021-02-12 10:16:50');

--
-- Tablo döküm verisi `menu`
--

INSERT INTO `menu` (`id`, `top_id`, `type`, `description`, `position`, `headertheme`, `slidertype`, `listtype`, `dropdowntype`, `breadcrumbvisible`, `asidevisible`, `order`, `status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, NULL, 'content', NULL, 'top', 'light', 'full-slider', NULL, '', 'no', 'no', 10, 'active', 'no', '2021-02-12 08:56:33', '2021-02-12 09:24:27'),
(2, 1, 'content', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 11, 'active', 'no', '2021-02-12 08:57:35', '2021-02-12 08:57:35'),
(3, 1, 'content', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 12, 'active', 'no', '2021-02-12 09:07:25', '2021-02-12 09:07:25'),
(4, 1, 'content', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 13, 'active', 'no', '2021-02-12 09:16:40', '2021-02-12 09:16:40'),
(5, 1, 'content', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 14, 'active', 'no', '2021-02-12 09:35:34', '2021-02-12 09:35:34'),
(6, 1, 'content', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 15, 'active', 'no', '2021-02-12 09:38:07', '2021-02-12 09:38:07'),
(7, 1, 'content', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 16, 'active', 'no', '2021-02-12 09:40:11', '2021-02-12 09:40:11'),
(8, 1, 'content', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 17, 'active', 'no', '2021-02-12 10:14:41', '2021-02-12 10:14:41'),
(9, 1, 'content', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 18, 'active', 'no', '2021-02-12 10:19:18', '2021-02-12 10:19:18'),
(10, 1, 'content', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 19, 'active', 'no', '2021-02-12 10:30:29', '2021-02-12 10:30:29'),
(11, 1, 'content', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 20, 'active', 'no', '2021-02-12 10:38:15', '2021-02-12 10:38:15'),
(12, NULL, 'list-icon', NULL, 'top', 'light', 'no', 'col-3x', '', 'no', 'no', 100, 'active', 'no', '2021-02-15 04:52:01', '2021-02-16 10:14:19'),
(13, NULL, 'list-default', NULL, 'top', 'light', 'no', 'col-4x', '', 'no', 'no', 150, 'active', 'no', '2021-02-15 06:21:22', '2021-02-15 06:21:39'),
(14, NULL, 'list-photo', NULL, 'top', 'light', 'no', 'col-4x', '', 'no', 'no', 200, 'active', 'no', '2021-02-15 08:16:21', '2021-02-15 12:12:11'),
(15, NULL, 'list-ekip', NULL, 'top', 'light', 'no', 'normal', 'normal', 'no', 'no', 250, 'active', 'no', '2021-02-15 09:43:10', '2021-02-15 09:43:10'),
(16, NULL, 'list-product', NULL, 'top', 'light', 'no', 'col-4x', '', 'no', 'no', 300, 'active', 'no', '2021-02-15 11:01:11', '2021-02-15 11:12:05');

--
-- Tablo döküm verisi `menuvariable`
--

INSERT INTO `menuvariable` (`id`, `menu_id`, `lang_code`, `name`, `slug`, `menutitle`, `title`, `stvalue`, `created_at`, `updated_at`) VALUES
(1, 1, 'tr', 'temel içerikler', 'temel-icerikler-tr', 'temel içerikler', NULL, NULL, '2021-02-12 08:56:33', '2021-02-15 04:48:48'),
(2, 2, 'tr', 'metin&HTML', 'metinhtml-tr', 'Metin &HTML', 'Metin & HTML', NULL, '2021-02-12 08:57:35', '2021-02-12 09:06:23'),
(3, 3, 'tr', 'Fotoğraf', 'fotograf-tr', 'Fotoğraf', 'Fotoğraf', NULL, '2021-02-12 09:07:25', '2021-02-12 09:09:20'),
(4, 4, 'tr', 'Foto Galeri', 'foto-galeri-tr', 'Foto Galeri', 'Foto Galeri', NULL, '2021-02-12 09:16:40', '2021-02-12 09:16:40'),
(5, 5, 'tr', 'Button & Link', 'button-link-tr', 'Button & Link', 'Button & Link', NULL, '2021-02-12 09:35:34', '2021-02-12 09:35:34'),
(6, 6, 'tr', 'Slide', 'slide-tr', 'Slide', 'Slide', NULL, '2021-02-12 09:38:07', '2021-02-12 09:38:07'),
(7, 7, 'tr', 'seperatör', 'seperator-tr', 'seperatör', 'seperatör', NULL, '2021-02-12 09:40:11', '2021-02-12 09:40:11'),
(8, 8, 'tr', 'Türkiye Haritası', 'turkiye-haritasi-tr', 'Türkiye Haritası', 'Türkiye Haritası', NULL, '2021-02-12 10:14:41', '2021-02-12 10:14:41'),
(9, 9, 'tr', 'Form', 'form-tr', 'Form', 'Form', NULL, '2021-02-12 10:19:18', '2021-02-12 10:19:18'),
(10, 10, 'tr', 'Code', 'code-tr', 'Code', 'Code', NULL, '2021-02-12 10:30:29', '2021-02-12 10:30:29'),
(11, 11, 'tr', 'Section', 'section-tr', 'Section', 'Section', NULL, '2021-02-12 10:38:15', '2021-02-12 10:38:15'),
(12, 12, 'tr', 'icon listesi', 'icon-listesi-tr', 'icon listesi', NULL, '6_rUpvW.png', '2021-02-15 04:52:01', '2021-02-16 10:13:56'),
(13, 13, 'tr', 'liste blog', 'liste-blog-tr', 'liste blog', NULL, NULL, '2021-02-15 06:21:22', '2021-02-15 06:21:22'),
(14, 14, 'tr', 'liste marka', 'liste-marka-tr', 'liste marka', NULL, NULL, '2021-02-15 08:16:21', '2021-02-15 08:16:21'),
(15, 15, 'tr', 'liste ekip', 'liste-ekip-tr', 'liste ekip', NULL, NULL, '2021-02-15 09:43:10', '2021-02-15 09:43:10'),
(16, 16, 'tr', 'liste ürünler', 'liste-urunler-tr', 'liste ürünler', NULL, NULL, '2021-02-15 11:01:11', '2021-02-15 11:01:11');



-- Tablo döküm verisi `slider`
--

INSERT INTO `slider` (`id`, `menu_id`, `type`, `theme`, `align`, `order`, `status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'img', 'light', 'center:center', 10, 'active', 'no', '2021-02-12 09:24:53', '2021-02-15 04:41:51'),
(2, 1, 'video', 'light', 'bottom:left', 20, 'passive', 'yes', '2021-02-12 09:28:51', '2021-02-12 09:34:28'),
(3, 1, 'video', 'light', 'center:center', 20, 'active', 'no', '2021-02-15 12:02:12', '2021-02-15 12:02:12');

--
-- Tablo döküm verisi `slidervariable`
--

INSERT INTO `slidervariable` (`id`, `slider_id`, `lang_code`, `title`, `description`, `button_text`, `button_url`, `image_url`, `created_at`, `updated_at`) VALUES
(1, 1, 'tr', 'WELCOME TO WEBSHELL', 'Create just what you need for your Perfect Website. Choose from a wide range of Elements & simply put them on your own Canvas.', NULL, NULL, 'slider1_BLqUU.png', '2021-02-12 09:24:53', '2021-02-15 04:41:39'),
(2, 2, 'tr', '.', NULL, NULL, NULL, NULL, '2021-02-12 09:28:51', '2021-02-12 09:28:51'),
(3, 3, 'tr', 'WELCOME TO WEBSHELL', NULL, NULL, NULL, 'explore_CLvFo.mp4', '2021-02-15 12:02:12', '2021-02-15 12:07:01');

--
-- Tablo döküm verisi `top_has_sub`
--

INSERT INTO `top_has_sub` (`id`, `top_menu_id`, `top_content_id`, `sub_menu_id`, `sub_content_id`, `props`, `status`, `deleted`, `order`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL, 1, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 1, '2021-02-12 08:58:09', '2021-02-12 08:58:09'),
(2, 2, NULL, NULL, 2, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\"}', 'active', 'no', 20, '2021-02-12 08:59:13', '2021-02-12 09:00:37'),
(3, 2, NULL, NULL, 3, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-8\"}', 'active', 'no', 30, '2021-02-12 09:01:40', '2021-02-12 09:02:05'),
(4, 2, NULL, NULL, 4, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-4\"}', 'active', 'no', 40, '2021-02-12 09:02:46', '2021-02-12 09:03:14'),
(5, 2, NULL, NULL, 5, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\"}', 'active', 'no', 25, '2021-02-12 09:03:52', '2021-02-12 09:04:30'),
(6, 3, NULL, NULL, 6, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\",\"props_type\":\"responsive\"}', 'active', 'no', 10, '2021-02-12 09:09:44', '2021-02-15 04:32:18'),
(7, 3, NULL, NULL, 7, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\",\"props_type\":\"responsive\"}', 'active', 'no', 20, '2021-02-12 09:12:58', '2021-02-12 09:13:09'),
(8, 3, NULL, NULL, 8, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\",\"props_type\":\"responsive\"}', 'active', 'no', 30, '2021-02-12 09:13:40', '2021-02-12 09:14:07'),
(9, 3, NULL, NULL, 9, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-4\",\"props_type\":\"responsive\"}', 'active', 'no', 70, '2021-02-12 09:13:50', '2021-02-15 04:32:51'),
(10, 3, NULL, NULL, 10, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-8\",\"props_type\":\"responsive\"}', 'active', 'no', 50, '2021-02-12 09:13:59', '2021-02-12 09:14:22'),
(11, 4, NULL, NULL, 11, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\",\"props_type\":\"col-2x\"}', 'active', 'no', 10, '2021-02-12 09:17:05', '2021-02-12 09:18:13'),
(12, 4, NULL, NULL, 12, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\",\"props_type\":\"col-3x\"}', 'active', 'no', 20, '2021-02-12 09:18:39', '2021-02-12 09:18:39'),
(13, 4, NULL, NULL, 13, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\",\"props_type\":\"col-4x\"}', 'active', 'no', 40, '2021-02-12 09:20:14', '2021-02-12 09:20:54'),
(14, 5, NULL, NULL, 14, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-12 09:35:55', '2021-02-12 09:35:55'),
(15, 5, NULL, NULL, 15, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 20, '2021-02-12 09:36:46', '2021-02-12 09:36:46'),
(16, 6, NULL, NULL, 16, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-12 09:38:22', '2021-02-12 09:38:22'),
(17, 7, NULL, NULL, 17, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\",\"props_type\":\"normal\"}', 'active', 'no', 10, '2021-02-12 09:40:26', '2021-02-12 09:40:26'),
(18, 7, NULL, NULL, 18, '{\"props_section\":\"container\",\"props_colortheme\":\"dark\",\"props_colvalue\":\"col-lg-12\",\"props_type\":\"normal\"}', 'active', 'no', 20, '2021-02-12 10:06:34', '2021-02-12 10:06:42'),
(19, 7, NULL, NULL, 19, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\",\"props_type\":\"normal\"}', 'active', 'no', 30, '2021-02-12 10:07:03', '2021-02-12 10:07:09'),
(20, 7, NULL, NULL, 20, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\",\"props_type\":\"normal\"}', 'active', 'no', 40, '2021-02-12 10:08:08', '2021-02-12 10:08:14'),
(21, 8, NULL, NULL, 21, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-12 10:15:12', '2021-02-12 10:15:12'),
(22, 9, NULL, NULL, 22, '{\"props_eposta\":\"uygar@ideakup.com\",\"props_buttonname\":\"G\\u00f6nder\",\"props_comment_status\":null,\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-12 10:19:53', '2021-02-12 10:28:14'),
(23, 10, NULL, NULL, 23, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-12 10:31:37', '2021-02-12 10:31:37'),
(24, 11, NULL, NULL, 24, '{\"props_section\":\"section\",\"props_colortheme\":\"dark\",\"props_colvalue\":\"null\",\"props_fullwidth\":\"Full-Width\",\"bg_img\":\"false\"}', 'active', 'no', 20, '2021-02-12 10:38:38', '2021-02-12 10:48:16'),
(25, 11, NULL, NULL, 25, '{\"props_section\":\"section\",\"props_colortheme\":\"light\",\"props_colvalue\":\"null\",\"props_fullwidth\":\"Container\",\"bg_img\":\"false\"}', 'active', 'no', 25, '2021-02-12 10:39:55', '2021-02-12 11:00:01'),
(26, 11, 25, NULL, 26, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\",\"props_type\":\"responsive\"}', 'active', 'no', 10, '2021-02-12 10:41:35', '2021-02-12 10:44:37'),
(27, 11, 25, NULL, 27, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\"}', 'active', 'no', 20, '2021-02-12 10:44:07', '2021-02-12 10:44:14'),
(28, 11, 25, NULL, 28, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\",\"props_type\":\"normal\"}', 'passive', 'yes', 30, '2021-02-12 10:45:36', '2021-02-17 05:27:42'),
(29, 11, 24, NULL, 29, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\",\"props_type\":\"responsive\"}', 'active', 'no', 10, '2021-02-12 10:46:28', '2021-02-12 10:46:42'),
(30, 11, 24, NULL, 30, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\"}', 'active', 'no', 20, '2021-02-12 10:47:13', '2021-02-12 10:47:22'),
(31, 11, 24, NULL, 31, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\",\"props_type\":\"normal\"}', 'passive', 'yes', 30, '2021-02-12 10:48:45', '2021-02-17 05:27:15'),
(32, 11, NULL, NULL, 32, '{\"props_section\":\"section\",\"props_colortheme\":\"light\",\"props_colvalue\":\"null\",\"props_fullwidth\":\"Full-Width\",\"bg_img\":\"true\"}', 'active', 'no', 30, '2021-02-12 10:49:22', '2021-02-12 10:49:33'),
(33, 11, 32, NULL, 33, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-12 10:55:48', '2021-02-12 10:55:48'),
(34, 1, NULL, NULL, 34, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\"}', 'active', 'no', 20, '2021-02-15 04:43:35', '2021-02-16 11:35:59'),
(35, 1, NULL, NULL, 35, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\",\"props_type\":\"responsive\"}', 'active', 'no', 30, '2021-02-15 04:44:54', '2021-02-15 04:47:55'),
(36, 12, NULL, NULL, 36, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-15 04:52:21', '2021-02-15 04:52:21'),
(37, 12, NULL, NULL, 37, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 20, '2021-02-15 04:52:38', '2021-02-15 04:52:38'),
(38, 12, NULL, NULL, 38, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 30, '2021-02-15 04:52:50', '2021-02-15 04:52:50'),
(41, 1, NULL, NULL, 40, '{\"props_section\":\"section\",\"props_colortheme\":\"light\",\"props_colvalue\":\"null\",\"props_fullwidth\":\"Full-Width\",\"bg_img\":\"false\"}', 'passive', 'yes', 40, '2021-02-15 05:15:27', '2021-02-15 05:18:32'),
(40, 1, NULL, NULL, 39, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'passive', 'yes', 100000, '2021-02-15 05:14:18', '2021-02-16 11:31:54'),
(44, 1, NULL, 12, NULL, '{\"props_sum_type\":\"menusum_icon\",\"props_sum_count\":\"3\",\"props_sum_colvalue\":\"col-lg-12\"}', 'active', 'no', 40, '2021-02-15 05:19:23', '2021-02-15 05:19:34'),
(45, 1, NULL, NULL, 41, '{\"props_section\":\"section\",\"props_colortheme\":\"light\",\"props_colvalue\":\"null\",\"props_fullwidth\":\"true\",\"bg_img\":\"true\"}', 'active', 'no', 35, '2021-02-15 05:22:22', '2021-02-15 05:22:22'),
(46, 1, 41, NULL, 42, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-15 05:23:13', '2021-02-15 05:23:13'),
(47, 13, NULL, NULL, 43, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-15 06:21:53', '2021-02-15 06:21:53'),
(48, 13, NULL, NULL, 44, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 20, '2021-02-15 06:22:03', '2021-02-15 06:22:03'),
(49, 13, NULL, NULL, 45, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 30, '2021-02-15 06:22:12', '2021-02-15 06:22:12'),
(50, 13, NULL, NULL, 46, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 40, '2021-02-15 06:22:22', '2021-02-15 06:22:22'),
(51, 1, NULL, 13, NULL, '{\"props_sum_type\":\"menusum_default\",\"props_sum_count\":\"4\",\"props_sum_colvalue\":\"col-lg-12\"}', 'active', 'no', 50, '2021-02-15 06:25:14', '2021-02-15 06:25:30'),
(52, 13, 43, NULL, 47, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-15 06:27:34', '2021-02-15 06:27:34'),
(53, 1, NULL, NULL, 48, '{\"props_section\":\"section\",\"props_colortheme\":\"light\",\"props_colvalue\":\"null\",\"props_fullwidth\":\"Full-Width\",\"bg_img\":\"false\"}', 'active', 'no', 45, '2021-02-15 06:30:56', '2021-02-15 06:31:07'),
(54, 1, 48, NULL, 49, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\",\"props_type\":\"responsive\"}', 'active', 'no', 10, '2021-02-15 06:31:23', '2021-02-15 06:40:13'),
(55, 1, 48, NULL, 50, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-6\"}', 'active', 'no', 20, '2021-02-15 06:40:35', '2021-02-15 06:41:23'),
(56, 12, NULL, NULL, 51, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 40, '2021-02-15 06:47:39', '2021-02-15 06:47:39'),
(57, 12, NULL, NULL, 52, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 50, '2021-02-15 06:47:52', '2021-02-15 06:47:52'),
(58, 12, NULL, NULL, 53, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 60, '2021-02-15 06:48:04', '2021-02-15 06:48:04'),
(59, 14, NULL, NULL, 54, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-15 08:16:41', '2021-02-15 08:16:41'),
(60, 14, NULL, NULL, 55, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 20, '2021-02-15 08:16:53', '2021-02-15 08:16:53'),
(61, 14, NULL, NULL, 56, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 30, '2021-02-15 08:17:00', '2021-02-15 08:17:00'),
(62, 14, NULL, NULL, 57, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 40, '2021-02-15 08:17:10', '2021-02-15 08:17:10'),
(63, 14, NULL, NULL, 58, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 50, '2021-02-15 08:17:18', '2021-02-15 08:17:18'),
(64, 14, NULL, NULL, 59, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 60, '2021-02-15 08:17:27', '2021-02-15 08:17:27'),
(65, 14, NULL, NULL, 60, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 70, '2021-02-15 08:17:37', '2021-02-15 08:17:37'),
(66, 14, NULL, NULL, 61, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 80, '2021-02-15 08:17:47', '2021-02-15 08:17:47'),
(67, 14, NULL, NULL, 62, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 90, '2021-02-15 08:17:56', '2021-02-15 08:17:56'),
(68, 14, NULL, NULL, 63, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 100, '2021-02-15 08:18:06', '2021-02-15 08:18:06'),
(69, 1, NULL, 14, NULL, '{\"props_sum_type\":\"menusum_photo\",\"props_sum_count\":\"4\",\"props_sum_colvalue\":\"col-lg-12\"}', 'active', 'no', 60, '2021-02-15 08:21:52', '2021-02-15 08:29:22'),
(70, 1, NULL, NULL, 64, '{\"props_section\":\"container\",\"props_colortheme\":\"dark\",\"props_colvalue\":\"col-lg-12\",\"props_type\":\"normal\"}', 'passive', 'yes', 55, '2021-02-15 08:30:20', '2021-02-15 08:32:04'),
(71, 1, NULL, NULL, 65, '{\"props_section\":\"section\",\"props_colortheme\":\"dark\",\"props_colvalue\":\"null\",\"props_fullwidth\":\"Full-Width\",\"bg_img\":\"true\"}', 'active', 'no', 55, '2021-02-15 08:32:32', '2021-02-15 08:38:43'),
(72, 1, 65, NULL, 66, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-15 08:35:38', '2021-02-15 08:35:38'),
(73, 15, NULL, NULL, 67, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-15 09:50:38', '2021-02-15 09:50:38'),
(74, 15, NULL, NULL, 68, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 20, '2021-02-15 09:50:56', '2021-02-15 09:50:56'),
(75, 15, NULL, NULL, 69, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 30, '2021-02-15 09:51:11', '2021-02-15 09:51:11'),
(76, 16, NULL, NULL, 70, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 10, '2021-02-15 11:04:19', '2021-02-15 11:04:19'),
(77, 16, NULL, NULL, 71, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 20, '2021-02-15 11:04:31', '2021-02-15 11:04:31'),
(78, 16, NULL, NULL, 72, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 30, '2021-02-15 11:05:48', '2021-02-15 11:05:48'),
(79, 16, NULL, NULL, 73, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'active', 'no', 40, '2021-02-15 11:05:59', '2021-02-15 11:05:59'),
(83, 1, NULL, NULL, 110, '{\"props_section\":\"container\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'passive', 'yes', 99999, '2021-02-17 05:19:58', '2021-02-17 05:54:52'),
(82, 1, NULL, NULL, 109, '{\"props_section\":\"footer-stick\",\"props_colortheme\":\"light\",\"props_colvalue\":\"col-lg-12\"}', 'passive', 'yes', 99999, '2021-02-16 11:37:47', '2021-02-16 11:44:47'),
(84, 1, NULL, NULL, 111, '{\"props_section\":\"section\",\"props_colortheme\":\"light\",\"props_colvalue\":\"null\",\"props_fullwidth\":\"true\",\"bg_img\":\"true\"}', 'active', 'no', 99999, '2021-02-17 05:51:38', '2021-02-17 05:51:38');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
