<?php

use Illuminate\Database\Seeder;

class UserLoginModuleOffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('menu')->where('type','module-user')->delete();
        DB::table('menuvariable')->where('name','user')->delete();
        DB::table('new_top_has_sub')->where('order',2000000)->delete();

        DB::table('content')->where('type','module-user')->delete();
        DB::table('contentvariable')->where('title','profile')->delete();
        DB::table('contentvariable')->where('title','login')->delete();
        DB::table('contentvariable')->where('title','orders')->delete();
        DB::table('contentvariable')->where('title','forgotpassword')->delete();

        $this->command->info('UserLogin Modulü Kapatıldı');

    }
}
