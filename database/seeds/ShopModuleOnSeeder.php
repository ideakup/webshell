<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ShopModuleOnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
      public function run(){
        DB::table('menu')->where('type','module-shop')->delete();
        DB::table('menuvariable')->where('name','shop')->delete();
        DB::table('new_top_has_sub')->where('order',1000000)->delete();

        DB::table('content')->where('type','module-shop')->delete();
        DB::table('contentvariable')->where('title','cart')->delete();
        DB::table('contentvariable')->where('title','checkout')->delete();
        DB::table('contentvariable')->where('title','islogin')->delete();
        DB::table('contentvariable')->where('title','addressform')->delete();
        DB::table('contentvariable')->where('title','payment')->delete();

       	$menu_id=DB::table('menu')->insertGetId([
			'type' => 'module-shop',		
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('new_top_has_sub')->insert([
            'top_id' => null,
            'top_model' => null,  
            'sub_id' => $menu_id,    
            'sub_model' => 'Module',
            'props' => '{"description":null,"position":"none","headertheme":"light","slidertype":"no","listtype":"normal","dropdowntype":"normal","breadcrumbvisible":"no","asidevisible":"yes"}',
            'status' => 'active',
            'deleted' => 'no',
            'order' => 1000000,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('menuvariable')->insert([
        	'id' => $menu_id,
        	'menu_id' => $menu_id,	
        	'lang_code' => 'tr',	
			'name' => 'shop',
			'slug' => 'shop',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);



        $shop_cart=DB::table('content')->insertGetId([		
			'type' => 'module-shop',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('contentvariable')->insert([
        	'id' => $shop_cart,		
			'content_id' => $shop_cart,
			'lang_code' => 'tr',	
			'slug' => 'cart',
			'title' => 'cart',
			'content' => 'shopping-cart',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $shop_checkout=DB::table('content')->insertGetId([		
			'type' => 'module-shop',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('contentvariable')->insert([
        	'id' => $shop_checkout,		
			'content_id' => $shop_checkout,
			'lang_code' => 'tr',	
			'slug' => 'checkout',
			'title' => 'checkout',
			'content' => 'shopping-checkout',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);


        $shop_islogin=DB::table('content')->insertGetId([		
			'type' => 'module-shop',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('contentvariable')->insert([
        	'id' => $shop_islogin,		
			'content_id' => $shop_islogin,
			'lang_code' => 'tr',	
			'slug' => 'islogin',
			'title' => 'islogin',
			'content' => 'shopping-islogin',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);


        $shop_addressform=DB::table('content')->insertGetId([		
			'type' => 'module-shop',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('contentvariable')->insert([
        	'id' => $shop_addressform,		
			'content_id' => $shop_addressform,
			'lang_code' => 'tr',	
			'slug' => 'addressform',
			'title' => 'addressform',
			'content' => 'shopping-addressform',
			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);


        $shop_payment=DB::table('content')->insertGetId([      
            'type' => 'module-shop',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('contentvariable')->insert([
            'id' => $shop_payment,     
            'content_id' => $shop_payment,
            'lang_code' => 'tr',    
            'slug' => 'payment',
            'title' => 'payment',
            'content' => 'shopping-payment',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->command->info('Shop Modulü Açıldı');

    }
}
