<?php
Route::get('/uygar', 'HomeController@index' );



Route::redirect('/', '/home', 301);
Route::get('getusername/{email}' , 'ToolsController@getusername');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Auth::routes();

Route::get('/', 'AdminController@index');
Route::get('/dashboard', 'AdminController@index');

Route::get('/test', 'MenuController@test');

// AJAX ROUTES
Route::post('/getMenu', 'AdminController@getMenuAjax');
Route::post('/getContent', 'AdminController@getContentAjax');
Route::post('/getTag', 'AdminController@getTagAjax');
Route::post('/getCategory', 'AdminController@getCategoryAjax');
Route::post('/getContentAddons', 'AdminController@getContentAddonsAjax');
Route::post('/getSlider', 'AdminController@getSliderAjax');
Route::post('/getPhotoGallery', 'AdminController@getPhotoGalleryAjax');
Route::post('/getCalendar', 'AdminController@getCalendarAjax');
Route::post('/getForm', 'AdminController@getFormAjax');
Route::post('/getFormData', 'AdminController@getFormDataAjax');
Route::post('/uploadFile', 'AdminController@uploadFile');
Route::post('/cropThumbnail', 'AdminController@cropThumbnail');
Route::post('/getOrders', 'AdminController@getOrdersAjax');
Route::post('/getEventsession', 'AdminController@getEventsessionAjax');
Route::post('/getStore', 'AdminController@getStoreAjax');


Route::post('/getMapMarker', 'AdminController@getMapMarkerAjax');
// AJAX ROUTES

Route::get('/profile', 'ProfileController@index');
Route::post('/changePassword', 'ProfileController@change_password');

/**/Route::get('/sitesettings', 'SiteSettingsController@index');
/**/Route::post('/sitesettings_save', 'SiteSettingsController@save');
/**/Route::post('/sitesettings_imageCleaner', 'SiteSettingsController@imageCleaner');
/**/Route::post('/sitesettings_cacheCleaner', 'SiteSettingsController@cacheCleaner');


/**/Route::get('/language', 'LanguageController@index');
/**/Route::post('/language_save', 'LanguageController@language_save');

/**/Route::get('/photogallery', 'PhotoGalleryController@list');

Route::group(['prefix' => '/menu'], function () {

	Route::get('/list', 'MenuController@list');
	Route::get('/add', 'MenuController@add_edit');
	Route::get('/edit/{id}', 'MenuController@add_edit');
	Route::get('/edit/{id}/{lang}', 'MenuController@add_edit');
	Route::get('/delete/{id}', 'MenuController@delete');
	Route::post('/save', 'MenuController@save');
	//Route::get('/add', 'MenuController@mapmarker_add');

	Route::group(['prefix' => '/content'], function () {
		
		Route::get('/{id}', 'MenuController@content');
		Route::get('/{id}/add', 'MenuController@content_add');
		Route::get('/{id}/add/{cid}', 'MenuController@content_add');

		Route::get('/{id}/edit/{cid}/', 'MenuController@content_edit');
		Route::get('/{id}/edit/{cid}/{lang}', 'MenuController@content_edit');
		
		Route::get('/{id}/sectionbgimage/{cid}/', 'MenuController@section_bg');

		Route::get('/{id}/delete/{cid}', 'MenuController@content_delete');
		Route::post('/save', 'MenuController@content_save');

		Route::get('/{id}/addexist', 'MenuController@content_addexist');
		Route::get('/{id}/addexist/{cid}', 'MenuController@content_addexist');
		Route::post('/saveexist', 'MenuController@content_save_exist');

		Route::group(['prefix' => '/{id}/mapmarker/{cid}'], function () {
			Route::get('/', 'MenuController@mapmarker');
			Route::get('/add', 'MenuController@mapmarker_add');
			Route::get('/edit/{sid}', 'MenuController@mapmarker_edit');
			Route::get('/edit/{sid}/{lang}', 'MenuController@mapmarker_edit');
			Route::get('/delete/{sid}', 'MenuController@mapmarker_delete');
			Route::post('/save', 'MenuController@mapmarker_save');
		});

		Route::group(['prefix' => '/{id}/edit/{cid}'], function () {
			Route::get('/photogallery_edit/{fid}/{lang}', 'MenuController@photogalleryitem');
			Route::get('/photogallery_delete/{fid}/{lang}', 'MenuController@photogalleryitem_delete');
			Route::post('/photogallery_save', 'MenuController@photogalleryitem_save');
		});

		Route::group(['prefix' => '/{id}/edit/{cid}'], function () {
			Route::get('/slide_edit/{fid}/{lang}', 'MenuController@slideitem');
			Route::get('/slide_delete/{fid}/{lang}', 'MenuController@slideitem_delete');
			Route::post('/slide_save', 'MenuController@slideitem_save');
		});

		Route::group(['prefix' => '/{id}/subcontent/{cid}'], function () {
			Route::get('/', 'MenuController@content');
			/*
			Route::get('/{id}/addons/{cid}/add', 'MenuController@addon_add_edit');
			Route::get('/{id}/addons/{cid}/edit/{aid}', 'MenuController@addon_add_edit');
			Route::get('/{id}/addons/{cid}/edit/{aid}/{lang}', 'MenuController@addon_add_edit');
			//Route::get('/{id}/addons/{cid}/galup/{aid}', 'MenuController@addon_add_edit');
			Route::get('/{id}/addons/{cid}/delete/{aid}', 'MenuController@addon_delete');
			Route::post('/{id}/addons/{cid}/save', 'MenuController@addon_save');
			*/
		});

		Route::get('/{id}/addons/{cid}', 'MenuController@addons');
		Route::get('/{id}/addons/{cid}/add', 'MenuController@addon_add_edit');
		Route::get('/{id}/addons/{cid}/edit/{aid}', 'MenuController@addon_add_edit');
		Route::get('/{id}/addons/{cid}/edit/{aid}/{lang}', 'MenuController@addon_add_edit');
		//Route::get('/{id}/addons/{cid}/galup/{aid}', 'MenuController@addon_add_edit');
		Route::get('/{id}/addons/{cid}/delete/{aid}', 'MenuController@addon_delete');
		Route::post('/{id}/addons/{cid}/save', 'MenuController@addon_save');

		/*
		Route::group(['prefix' => '/{id}/addons/{cid}/edit/{aid}'], function () {
			Route::get('/photogallery_edit/{fid}', 'MenuController@addon_photogalleryitem');
			Route::get('/photogallery_delete/{fid}', 'MenuController@addon_photogalleryitem_delete');
			Route::post('/photogallery_save', 'MenuController@addon_photogalleryitem_save');
		});
		*/

	});

	Route::group(['prefix' => '/stslider'], function () {
		Route::get('/{id}', 'MenuController@stslider');
		Route::get('/{id}/add', 'MenuController@stslider_add_edit');
		Route::get('/{id}/edit/{sid}', 'MenuController@stslider_add_edit');
		Route::get('/{id}/edit/{sid}/{lang}', 'MenuController@stslider_add_edit');
		Route::get('/{id}/delete/{sid}', 'MenuController@stslider_delete');
		Route::post('/save', 'MenuController@stslider_save');
	});

	Route::get('/stimage/{id}', 'MenuController@stimage');
});

Route::group(['prefix' => '/tag'], function () {
	Route::get('/', 'MenuController@tag');
	Route::get('/add', 'MenuController@tag_add_edit');
	Route::get('/edit/{tid}', 'MenuController@tag_add_edit');
	Route::get('/edit/{tid}/{lang}', 'MenuController@tag_add_edit');
	Route::get('/delete/{tid}', 'MenuController@tag_delete');
	Route::get('/content/{tid}', 'MenuController@tag_content');
	Route::post('/save', 'MenuController@tag_save');
});

Route::group(['prefix' => '/category'], function () {
	Route::get('/', 'MenuController@category');
	Route::get('/add', 'MenuController@category_add_edit');
	Route::get('/edit/{cid}', 'MenuController@category_add_edit');
	Route::get('/edit/{cid}/{lang}', 'MenuController@category_add_edit');
	Route::get('/delete/{cid}', 'MenuController@category_delete');
	Route::get('/content/{tid}', 'MenuController@category_content');
	Route::post('/save', 'MenuController@category_save');
});

Route::group(['prefix' => '/calendar'], function () {
	
	Route::get('/', 'CalendarController@index');
	Route::get('/list', 'CalendarController@list');
	Route::get('/add', 'CalendarController@add_edit');
	Route::get('/edit/{id}', 'CalendarController@add_edit');
	Route::get('/edit/{id}/{lang}', 'CalendarController@add_edit');
	Route::get('/delete/{id}', 'CalendarController@delete');
	Route::post('/save', 'CalendarController@save');
	
	Route::get('/reservations', 'CalendarController@reservations');

});
Route::group(['prefix' => '/orders'], function () {
	Route::get('/list', 'MenuController@orders');
	Route::get('/detail/{oid}', 'MenuController@orderdetail');
	Route::post('/save', 'MenuController@order_save');

});
Route::group(['prefix' => '/store'], function () {
	Route::get('/list', 'MenuController@store');
	Route::get('/settings', 'MenuController@store_settings');
	Route::post('/importconfirm', 'MenuController@importconfirm');
	Route::post('/save', 'MenuController@store_save');
});
Route::group(['prefix' => '/eventsession'], function () {
	Route::get('/list', 'EventSessionController@eventsession');
	Route::get('/add', 'EventSessionController@add');
	Route::get('/edit/{esid}', 'EventSessionController@edit');
	Route::get('/delete/{esid}', 'EventSessionController@delete');
	Route::post('/save', 'EventSessionController@save');
	

});

Route::group(['prefix' => '/form'], function () {

	Route::get('/list', 'MenuController@form_list');
	Route::get('/detail/{formid}', 'MenuController@form_detail');
	Route::get('/detail/{formid}/changevisible/{dataid}', 'MenuController@form_change_visible');

	//Route::get('content/0/add', 'MenuController@content_add');
	//Route::get('/{id}/edit/{cid}', 'MenuController@content_add_edit');
	//Route::get('/{id}/edit/{cid}/{lang}', 'MenuController@content_add_edit');

});

Route::get('/calendar/export', 'ExportController@calendarExport');
Route::post('/formdata/export', 'ExportController@formDataExport');
Route::post('/product/export', 'ExportController@productDataExport');
Route::post('/product/import', 'İmportController@productDataİmport');