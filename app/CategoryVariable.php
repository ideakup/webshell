<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryVariable extends Model
{
    protected $table = 'categoryvariable';

    public function CategoryTopHasSub()
	    {
	        return $this->hasmany('App\CategoryTopHasSub', 'category_id', 'category_id');
	    }
}
