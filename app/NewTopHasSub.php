<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;

class NewTopHasSub extends Model
{
    protected $table = 'new_top_has_sub';
	

    public function thatElementForMenu()
    {	
        return $this->hasOne('App\Menu', 'id', 'sub_id');
    }
    public function topElementForMenu()
    {	
        return $this->hasOne('App\Menu', 'id', 'top_id');
    }
    public function thatElementForContent()
    {	
        return $this->hasOne('App\Content', 'id', 'sub_id');
    }
    public function topElementForContent()
    {	
        return $this->hasOne('App\Content', 'id', 'top_id');
    }

    public function thatElementForMenusum()
    {   
        return $this->hasOne('App\Menu', 'id', 'sub_id');
    }
    public function topThs()
    {   
        return $this->hasOne('App\NewTopHasSub', 'sub_id', 'top_id');
    }

}
