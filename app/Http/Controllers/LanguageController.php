<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Language;

class LanguageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$languages = Language::orderBy('order', 'asc')->get();
        return view('language', array('languages' => $languages));
    }

	public function language_save(Request $request)
    {	
    	$languages = Language::all();
    	foreach ($languages as $lang) {
    		$lang->status = 'passive';
    		$lang->save();
    	}
    	foreach ($request->input() as $key => $value) {
    		$language = Language::where('code', $key)->first();
    		if (!is_null($language)) {
    			$language->status = 'active';
    			$language->save();
    		}
    	}
    	$text = 'Başarıyla Kaydedildi...';
		return redirect('language')->with('message', array('text' => $text, 'status' => 'success'));
    }
}
