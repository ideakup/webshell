<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\ContentVariable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\NewTopHasSub;



class HomeController extends Controller
{

	public function __construct()
	{
        //$this->middleware('auth');
	}

	public function index()

	{

		//dump('asd');
		$orderBys = array();
		$orderBys[] = array('id', 'asc');
		$arr = $this->menus(null, $orderBys);
		
		dump($arr);
		die;
	}

	public function menus($param, $order = null){
		$returnArr = array();
		$menus = NewTopHasSub::where('sub_model', 'Menu')->where('deleted', 'no')->where('top_id', $param)->orderBy('order');

		if(!empty($order)){
			foreach ($order as $o) {
				$menus = $menus->orderBy($o[0], $o[1]);
			}
		}

		$menus = $menus->get();

		foreach ($menus as $menu) {
			dump($menu->id.'-'.$menu->top_id.'-'.$menu->sub_id.'-'.$menu->order);
			$returnArr[] = $menu;

			foreach ($this->menus($menu->sub_id, $order) as $value) {
				$returnArr[] = $value;
			}


		}
		return $returnArr;
	}

	public function menuss($_chts,$_array){
        
        foreach ($_chts as $_cht) {
            if($_cht->subCat->count() == 0){
                array_push($_array,$_cht->id);
            }else{
                $_array = array_merge($_array, $this->subcat2($_cht->subCat, $_array));
            }
        } 
        return array_unique($_array);
    } 
}
