<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use App\Language;
use App\User;
use App\Menu;
use App\MenuVariable;
use App\Content;
use App\ContentVariable;
use App\Tag;
use App\TagVariable;
use App\Category;
use App\CategoryVariable;
use App\CategoryTopHasSub;
use App\FormData;
use App\Form;

use App\District;
use App\DistrictVariable;
use App\Staff;
use App\StaffVariable;

use App\Country;
use App\CountryVariable;
use App\City;
use App\CityVariable;
use App\County;
use App\CountyVariable;

use App\MenuHasContent;
use App\TopHasSub;
use App\NewTopHasSub;
use App\ShoppingOrder;
use App\ShoppingOrderSettings;

use Mail;
use App\Mail\OrderShippedInfo;

use App\ContentHasTag;
use App\ContentHasCategory;
use App\ContentSlideVariable;

use App\ContentPhotoGalleryVariable;
use App\Slider;
use App\SliderVariable;
use App;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function test()
    {
        //dump(FormData::select('source_id', 'source_type', 'form_id')->distinct()->get());

        $uniqForms = FormData::select('source_id', 'source_type', 'form_id')->distinct()->get();

        echo 'test';
    }

    /** MENÜ **/
    public function list()
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        return view('menu.list', array('langs' => $langs));
    }

    public function add_edit($id = null, $lang = null,Request $request)
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $topmenus = NewTopHasSub::where('deleted', 'no')->where('top_id',null)->get();
        if(is_null($id)){
            $menu_new_ths = new NewTopHasSub();
            $menu=new Menu();
        }else{
            
            $menu_new_ths = NewTopHasSub::find($id);
            $menu=$menu_new_ths->thatElementForMenu;
        }
        $topCat=CategoryTopHasSub::where('top_category_id',null)->orderBy('order', 'asc')->get();
        return view('menu.crud', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs, 'topCat' => $topCat, 'menu_new_ths' => $menu_new_ths));
    }

    public function delete($id = null)
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $topmenus = NewTopHasSub::where('deleted', 'no')->where('top_id',null)->get();
        $menu_new_ths = NewTopHasSub::find($id);
        $menu=$menu_new_ths->thatElementForMenu;        
        return view('menu.crud', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
    }

    public function save(Request $request)
    {
            //dd($request->input());
        $text = "";
        if ($request->crud == 'add') {
            $rules = array(
                'name' => 'required|max:255',
                'order' => 'numeric|min:1|max:100000'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
           }

           $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

           $menu = new Menu();
           $new_ths=new NewTopHasSub();
           if($request->topmenu != 'null') { 

                $new_ths->top_id = $request->topmenu;
                $new_ths->top_model="Menu";
            }
            $menu->type = $request->type;
            $menu->save();

            $new_ths->sub_id = $menu->id;
            $new_ths->sub_model="Menu";
            $_props = array();
            $_props['description'] = $request->description;
            $_props['position'] = $request->position;
            $_props['headertheme'] = "light";
            $_props['slidertype'] = "no";
            $_props['listtype'] = "normal";            
            $_props['dropdowntype'] = "normal"; 
            $_props['breadcrumbvisible'] = "no"; 
            $_props['asidebarside'] = "right";
            $_props['asidevisible'] = "no"; 
            $_props['tagbarvisible'] = "no";
            $_props['catbarvisible'] = "no";

            $new_ths->props = json_encode($_props);
            $new_ths->order = $request->order;
            $new_ths->status = ($request->status == 'active') ? 'active' : 'passive';

            $new_ths->save();


            foreach ($languages  as $language) {
            $menuVariable = new MenuVariable();
            $menuVariable->menu_id = $menu->id;
            $menuVariable->lang_code = $language->code;
            $menuVariable->name = $request->name;

                    $menuVariable->slug = str_slug($request->name.'-'.$language->code, '-'); //SLU-GGG
                    $slugCount = MenuVariable::where('slug',$menuVariable->slug)->count();
                    if($slugCount!=0){
                     $random = str_random(3);
                     $menuVariable->slug =$menuVariable->slug.$random;
                 }

                 $menuVariable->menutitle = $request->menutitle;
                 $menuVariable->title = $request->title;

                 if ($menu->type == 'link') {
                    $menuVariable->stvalue = json_encode(['link' => '#', 'target' => 'self']);
                }

                $menuVariable->save();
            }

            $text = 'Başarıyla Eklendi...';

        }
        else if($request->crud == 'edit'){
            //dd($request->input());
            $menu_new_ths = NewTopHasSub::find($request->menu_id);
            $menu=$menu_new_ths->thatElementForMenu;
            if(is_null($request->top_menu_id) && is_null($request->top_content_id)){

                if (is_null($request->lang)) {
                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                   }
                   
                    $menu_new_ths->order = $request->order;
                    $menu_new_ths->status = ($request->status == 'active') ? 'active' : 'passive';

                    //dd($request->catbarvisible);

                    $a=json_decode($menu_new_ths->props);
                    $a->description= $request->description;
                    $a->position = $request->position.'';
                    $a->headertheme = $request->headertheme.'';
                    $a->slidertype = $request->slidertype.'';
                    $a->breadcrumbvisible = $request->breadcrumbvisible.'';
                    $a->asidebarside = $request->asidebarside.'';
                    $a->asidevisible = $request->asidevisible.'';
                    $a->tagbarvisible = $request->tagbarvisible.'';
                    $a->catbarvisible = implode('-',$request->catbarvisible);
                    $a->dropdowntype = $request->dropdowntype.'';
                    if ( starts_with($menu_new_ths->thatElementForMenu->type, 'list') || $menu_new_ths->thatElementForMenu->type == 'photogallery'|| $menu_new_ths->thatElementForMenu->type == 'menuitem') {
                            $a->listtype = $request->listtype.'';
                    }else{
                            $a->listtype = null;
                    }
                    $menu_new_ths->props = json_encode($a);

                    $menu_new_ths->save();
                }else{

                            //dump($request->input());

                    $slugCount = MenuVariable::where('slug', str_slug($request->slug, '-'))->count();
                    $slugControl = true;
                    if($slugCount > 0){
                        $slugMenuVars = MenuVariable::where('slug', str_slug($request->slug, '-'))->get();
                        foreach ($slugMenuVars as $slugMenuVar) {
                            if($slugMenuVar->menu_id == $request->menu_id && $slugMenuVar->lang_code == $request->lang){
                            }else{
                                $slugControl = false;
                            }
                                    //dump($slugMenuVar);
                        }
                    }

                            //dump($slugControl);
                            //dd($slugCount);

                    $rules = array(
                        'name' => 'required|max:255'
                    );

                    $validator = Validator::make(Input::all(), $rules);

                    if (!$slugControl) {
                        $validator->after(function ($validator) {
                            $validator->errors()->add('slug', 'Bu alan benzersiz olmalı.');
                        });
                    }

                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                   }

                   $menuVariable = Menu::find($menu->id)->variableLang($request->lang);
                   if (!isset($menuVariable)) {
                        $menuVariable = new MenuVariable();
                        $menuVariable->menu_id = $request->menu_id;
                        $menuVariable->lang_code = $request->lang;
                    }
                    $menuVariable->name = $request->name;
                    $menuVariable->slug = str_slug($request->slug, '-'); //SLUGGG
                    $menuVariable->menutitle = $request->menutitle;
                    $menuVariable->title = $request->title;
                    if ($menuVariable->menu->type == 'link') {
                        $menuVariable->stvalue = json_encode(['link' => $request->stvalue_link, 'target' => $request->stvalue_target]);
                    }
                    $menuVariable->save();
                }

            }else if(!is_null($request->top_menu_id)){
                    //dd($request->input());
                    $top_has_sub = NewTopHasSub::where('id',$request->menu_id)->first();

                    $_props = array();
                    $_props['props_sum_type'] = $request->props_sum_type;
                    $_props['props_sum_count'] = $request->props_sum_count;
                    $_props['props_sum_colvalue'] = $request->props_sum_colvalue;
                    $_props['tag'] = implode('-',$request->tag);
                    $_props['display_type'] = $request->display_type;


                    $_props['props_section'] = 'container';
                    $_props['props_colortheme'] = 'light';

                    $top_has_sub->props = json_encode($_props);
                    $top_has_sub->order = $request->order;
                    $top_has_sub->status = ($request->status == 'active') ? 'active' : 'passive';

                    $top_has_sub->save();
                     return redirect('menu/content/'.$request->top_menu_id)->with('message', array('text' => $text, 'status' => 'success'));


            }else if(!is_null($request->top_content_id)){

                    //dd($request->input());
                    $top_has_sub = NewTopHasSub::where('id',$request->menu_id)->first();

                    $_props = array();
                    $_props['props_sum_type'] = $request->props_sum_type;
                    $_props['props_sum_count'] = $request->props_sum_count;
                    $_props['props_sum_colvalue'] = $request->props_sum_colvalue;
                    $_props['tag'] = implode('-',$request->tag);
                    $_props['display_type'] = $request->display_type;


                    $_props['props_section'] = 'container';
                    $_props['props_colortheme'] = 'light';

                    $top_has_sub->props = json_encode($_props);
                    $top_has_sub->order = $request->order;
                    $top_has_sub->status = ($request->status == 'active') ? 'active' : 'passive';

                    $top_has_sub->save();
                }

                $text = 'Başarıyla Düzenlendi...';

        }else if($request->crud == 'delete'){
                //dd($request->input());
                $menu_new_ths = NewTopHasSub::find($request->menu_id);
              
                $menu_new_ths->deleted = 'yes';
                $menu_new_ths->status = 'passive';
                $menu_new_ths->save();
                if( $menu_new_ths->sub_model=='Menu'){
                    foreach ($menu_new_ths->thatElementForMenu->variables as $menuVariable) {
                        $menuVariable->slug = null;
                        $menuVariable->save();
                    }
                }
                
                $text = 'Başarıyla Silindi...';

            }
            return redirect('menu/list')->with('message', array('text' => $text, 'status' => 'success'));
        }
        /** MENÜ **/

        /** CONTENT **/
        public function content($id, $cid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu_new_ths = NewTopHasSub::find($id);
            $menu=$menu_new_ths->thatElementForMenu;

            if(is_null($id)){
                $content = new Content();
            }else{
                $ths=NewTopHasSub::find($cid);
                if(!empty($ths)){
                   $content = $ths->thatElementForContent;  
                }
                else{
                    $content=null; 
                }
               
            }
             $menu_ids=array();
             $menuroots=array();
            function totheTop($cid,$menu_ids){
                $ths=NewTopHasSub::find($cid);
                //dd($ths->topThs);
                if($ths->top_model != 'Menu'){
                    array_push($menu_ids,$ths);
                    $menu_ids=totheTop($ths->topThs->id,$menu_ids);

                }
                else{
                   array_push($menu_ids,$ths);
 
                }

                return $menu_ids;
            }
            if (!is_null($cid)) {
                $menuroots=totheTop($cid,$menu_ids);
            }
            else{
                array_push($menuroots,$menu_new_ths);
            }
               $menuroots=array_reverse($menuroots);
               //dd($menuroots);
            return view('menu.content', array('menu' => $menu, 'content' => $content, 'langs' => $langs, 'menuroots' => $menuroots, 'menu_new_ths' => $menu_new_ths));
        }

        public function content_add($id, $cid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = CategoryTopHasSub::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            
            $menu_new_ths = NewTopHasSub::find($id);
            $menu=$menu_new_ths->thatElementForMenu;
            $ths = null;
            if(is_null($cid)){
                $content = null;
            }else{
                $ths=NewTopHasSub::find($cid);
                $content =$ths->thatElementForContent;
            }

            $menus = Menu::join('new_top_has_sub','new_top_has_sub.sub_id','=','menu.id')
                           ->join('menuvariable','menuvariable.menu_id','=','new_top_has_sub.sub_id')
                           ->whereNotIn('type', ['mixed', 'link', 'content'])->where('deleted', 'no')->where('sub_model', 'Menu')->where('status', 'active')->orderBy('type', 'desc')->orderBy('order', 'asc')->get();
            
            $contents = Content::join('new_top_has_sub','new_top_has_sub.sub_id','=','content.id')
                           ->join('contentvariable','contentvariable.content_id','=','content.id')
                           ->whereIn('type', ['text', 'photo', 'form', 'code', 'photogallery', 'link', 'slide'])->where('deleted', 'no')->where('status', 'active')->orderBy('type', 'desc')->orderBy('order', 'asc')->get();
            $contents=$contents->unique('sub_id');
       
            $form = Content::orderBy('id', 'asc')->get();

            return view('menu.contentcrud', array('menu' => $menu, 'ths' => $ths, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories, 'menus' => $menus, 'contents' => $contents, 'form' => $form));
        }
        public function section_bg($id, $cid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = CategoryTopHasSub::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            
            $menu_new_ths = NewTopHasSub::find($id);
            $menu=$menu_new_ths->thatElementForMenu;
            $ths = null;
            if(is_null($cid)){
                $content = null;
            }else{
                $ths=NewTopHasSub::find($cid);
                $content =$ths->thatElementForContent;
            }

            return view('menu.sectionbgimage', array('menu' => $menu, 'ths' => $ths, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }


        public function content_edit($id, $cid, $lang = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = CategoryTopHasSub::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

            $menu_new_ths = NewTopHasSub::find($id);
            $menu=$menu_new_ths->thatElementForMenu;
            $ths = NewTopHasSub::find($cid);
            $content=$ths->thatElementForContent;            
           /* $content = Content::find($cid);*/
            $contentvariable = ContentVariable::find($cid);
            if(!is_null($lang) && is_null($content->variableLang($lang))){
                $contentVariable = new ContentVariable();
                $contentVariable->content_id = $content->id;
                $contentVariable->lang_code = $lang;
                $contentVariable->title = '';
                $contentVariable->save();
            }
            
           /* if(!empty(Input::get('tcid'))){
                $ths = TopHasSub::where('top_menu_id', $id)->where('top_content_id', Input::get('tcid'))->where('sub_content_id', $cid)->first();
            }else{
                $ths = TopHasSub::where('top_menu_id', $id)->where('sub_content_id', $cid)->first();
            }*/

            

            //dd(array('menu' => $menu, 'ths' => $ths, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
            return view('menu.contentcrud', array('menu' => $menu, 'ths' => $ths, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }

        public function content_delete($id, $cid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = CategoryTopHasSub::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $ths = NewTopHasSub::find($cid);
            $menu_new_ths = NewTopHasSub::find($id);
            $menu=$menu_new_ths->thatElementForMenu;

            $content =$ths->thatElementForContent;
            return view('menu.contentcrud', array('menu' => $menu, 'ths' => $ths, 'content' => $content, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }

        public function content_save(Request $request)
        {
            //dd($request->input());
            $text = "";
            if($request->crud == 'add'){

                $rules = array(
                    'title' => 'required|max:255',
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
               }
               $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
               $menu_new_ths = NewTopHasSub::find($request->menu_id);
               $menu=$menu_new_ths->thatElementForMenu;

               $content = new Content();
               $content->type = $request->type.'';
               //$content->order = $request->order;
              // $content->status = ($request->status == 'active') ? 'active' : 'passive';
               $content->save();
               //NEW TOP HAS SUB

                    $new_ths = new NewTopHasSub();
                    if(is_null($request->content_id)){
                        $new_ths->top_id = $menu->id;
                        $new_ths->top_model = "Menu"; 
                    }
                    else{
                        $topcontentths=NewTopHasSub::find($request->content_ths_id);
                        $new_ths->top_id = $topcontentths->sub_id;
                        $new_ths->top_model = "Content";
                    }
                    
                    $new_ths->sub_id = $content->id;
                    $new_ths->sub_model="Content";
                    $new_ths->order = $request->order;
                    $new_ths->status = ($request->status == 'active') ? 'active' : 'passive';

                    $_props = array();
                    $_props['props_section'] = 'container';
                    $_props['props_colortheme'] = 'light';
                    $_props['props_colvalue'] = 'col-lg-12';

                    //DÜZENLENECEK çoğu CONTENTE ATANACAK

                    if ($content->type == 'form') {
                        $_props['props_eposta'] = '';
                        $_props['props_buttonname'] = 'Gönder'; // Dil ayrımı yapılmalı...
                        $_props['props_comment_status'] = '';
                    }
                    if ($content->type == 'seperator') {
                        $_props['props_type'] = 'normal';
                    }

                    if ($content->type == 'photo') {
                        $_props['props_type'] = 'responsive';

                    }

                    if ($content->type == 'photogallery') {
                        $_props['props_type'] = 'col-3x';
                        $_props['display_type'] = 'horizontal';
                    }
                    if (starts_with($content->type, 'section')) {
                        $_props['props_section'] = 'section';
                        $_props['props_colortheme'] = 'light';
                        $_props['props_colvalue'] = 'null';    
                        $_props['props_fullwidth'] = 'true';
                        $_props['section_props_section'] = '';

                        $_props['bg_img'] = 'false';
                                             
                    }
                    
                    $new_ths->props = json_encode($_props);

                    $new_ths->save();

               foreach ($languages as $language) {

                $contentVariable = new ContentVariable();
                $contentVariable->content_id = $content->id;
                $contentVariable->lang_code = $language->code;
                $contentVariable->title = $request->title;
                $contentVariable->save();


                if ($content->type == 'link') {
                    $_content = array();
                    $_content['content_url'] = '#';
                    $_content['content_target'] = 'internal' ;
                    $contentVariable->content = json_encode($_content);
                }
                if ($content->type == 'video') {
                    $_content = array();
                    $_content['embed_code'] = '#';
                    $_content['type'] = '' ;
                    $contentVariable->content = json_encode($_content);
                }

                if (is_null($request->menu_id)) {
                    $contentVariable->save();
                }else{




                        if ($content->type == 'mapturkey') {
                            $contentVariable->slug = str_slug($request->title.'-'.$language->code, '-');  //SLUGGG
                            $slugCount = ContentVariable::where('slug', $contentVariable)->count();
                            if($slugCount!=0){
                                $random = str_random(3);
                                $contentVariable->$contentVariable.$random;
                            }

                        }
                        if ($content->type == 'photogallery') {
                            $contentVariable->slug = str_slug($request->title.'-'.$language->code, '-');  //SLUGGG
                            $slugCount = ContentVariable::where('slug', $contentVariable)->count();
                            if($slugCount!=0){
                                $random = str_random(3);
                                $contentVariable->$contentVariable.$random;
                            }

                        }if ($content->type == 'mapturkey') {
                            $contentVariable->slug = str_slug($request->title.'-'.$language->code, '-');  //SLUGGG
                            $slugCount = ContentVariable::where('slug', $contentVariable)->count();
                            if($slugCount!=0){
                                $random = str_random(3);
                                $contentVariable->$contentVariable.$random;
                            }

                        }

                        if (starts_with($content->type , 'group'))  {


                            $contentVariable->slug = str_slug($request->title.'-'.$language->code, '-');  //SLUGGG
                            $slugCount = ContentVariable::where('slug',$contentVariable->slug)->count();
                            $contentVariable->content="{}";

                           // dd($slugCount);
                            if($slugCount!=0){
                                $random = str_random(3);
                                $contentVariable->slug=$contentVariable->slug.$random;
                            }
                        }
                        if ($content->type == 'group-product')  {

                            $contentVariable->slug = str_slug($request->title.'-'.$language->code, '-');  //SLUGGG
                            $slugCount = ContentVariable::where('slug',$contentVariable->slug)->count();
                            $contentVariable->content="{}";

                           // dd($slugCount);
                            if($slugCount!=0){
                                $random = str_random(3);
                                $contentVariable->slug=$contentVariable->slug.$random;
                            }
                            $contentVariable->save();

                            $subcontent = new Content();
                            $subcontent->type = "photogallery-product";
                            $subcontent->save();

                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $subcontent->id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = "product_photogallery";
                            $contentVariable->save();

                            $top_has_sub = new NewTopHasSub();
                            $top_has_sub->top_id = $content->id;
                            $top_has_sub->top_model = 'Content';
                            $top_has_sub->sub_id = $subcontent->id;
                            $top_has_sub->sub_model = 'Content';
                            $top_has_sub->props ='"props_section":"container","props_colortheme":"light","props_colvalue":"col-lg-12"';
                            $top_has_sub->status = "passive";
                            $top_has_sub->deleted = "no";
                            $top_has_sub->order = 10;
                            $top_has_sub->save();


                            //----------------------
                            $subcontent = new Content();
                            $subcontent->type = "text-product";
                            $subcontent->save();

                            $contentVariable = new ContentVariable();
                            $contentVariable->content_id = $subcontent->id;
                            $contentVariable->lang_code = $language->code;
                            $contentVariable->title = "product_description";
                            $contentVariable->save();

                            $top_has_sub = new NewTopHasSub();
                            $top_has_sub->top_id = $content->id;
                            $top_has_sub->top_model = 'Content';
                            $top_has_sub->sub_id = $subcontent->id;
                            $top_has_sub->sub_model = 'Content';
                            $top_has_sub->props ='"props_section":"container","props_colortheme":"light","props_colvalue":"col-lg-12"';
                            $top_has_sub->status = "passive";
                            $top_has_sub->deleted = "no";
                            $top_has_sub->order = 10;
                            $top_has_sub->save();

                            //----------------------

                            


                        }

                   
                        if (starts_with($content->type , 'section'))  {


                            $contentVariable->slug = str_slug($request->title.'-'.$language->code, '-');  //SLUGGG
                            $slugCount = ContentVariable::where('slug',$contentVariable->slug)->count();
                            $contentVariable->content="{}";

                           // dd($slugCount);
                            if($slugCount!=0){
                                $random = str_random(3);
                                $contentVariable->slug=$contentVariable->slug.$random;
                            }
                        }



                    }

                    $contentVariable->save();

                }


                $text = 'Başarıyla Eklendi...';
                
                if ($request->menu_id == 0 && is_null($request->content_id)) {
                    return redirect('form/list/')->with('message', array('text' => $text, 'status' => 'success'));
                }else{

                    if(!is_null($request->content_ths_id)){
                        return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success')); 
                    }else{
                        return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success')); 
                    }

                }

            }else if($request->crud == 'edit'){
                if (is_null($request->lang)) {
                   // dd($request->input());
                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                    }
                    $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

                    $menu_new_ths = NewTopHasSub::find($request->menu_id);
                    $menu=$menu_new_ths->thatElementForMenu;

            
                    $ths= NewTopHasSub::find($request->content_ths_id);
                   
                    

                    $ths->order=$request->order;
                    $ths->status = ($request->status == 'active') ? 'active' : 'passive';
                    
                    $content = $ths->thatElementForContent;
                    //dd($content);

                    $_props = array();

                    if ($content->type == 'form') {

                        $_props['props_eposta'] = $request->props_eposta;
                        $_props['props_buttonname'] = $request->props_buttonname;
                        $_props['props_comment_status'] = $request->props_comment_status;

                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;

                        if($request->props_comment_status == 'active'){
                            $_props['props_visible_elemenets'] = $request->formelements;
                        }
                        $ths->props = json_encode($_props);             
                        $ths->save();

                    }

                    if ($content->type == 'text') {
                        foreach ($languages as $language) {

                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);


                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';                                
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;
                           

                            $ths->props = json_encode($_props);
                            $ths->save();
                        }
                        
                    }

                    if ($content->type == 'slide') {
                        foreach ($languages as $language) {

                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';                                
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;


                            $ths->props = json_encode($_props);
                            $ths->save();
                        }
                        
                    }

                    if ($content->type == 'rssfeed') {
                        foreach ($languages as $language) {

                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';                                
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;
                        

                            $ths->props = json_encode($_props);
                            $ths->save();
                        }

                    }

                    if ($content->type == 'mapturkey') {
                        foreach ($languages as $language) {

                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';                                
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;
                            
                        

                            $ths->props = json_encode($_props);
                            $ths->save();
                        }

                    }

                    if ($content->type == 'form') {

                        foreach ($languages as $language) {
                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';
                                $contentVariable->save();
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;

                         
                        }

                    }

                    if ($content->type == 'seperator') {

                        foreach ($languages as $language) {
                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;

                            $_props['props_type'] = $request->props_type;
                  

                            $ths->props = json_encode($_props);
                            $ths->save();
                        }

                    }

                    if ($content->type == 'link') {
                        foreach ($languages as $language) {

                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;
                   
                            $ths->props = json_encode($_props);
                            $ths->save();
                        }

                    }
                    if ($content->type == 'video') {
                        foreach ($languages as $language) {

                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;
                   
                            $ths->props = json_encode($_props);
                            $ths->save();
                        }

                    }

                    if ($content->type == 'photo') {
                        foreach ($languages as $language) {

                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';                                
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;
                            $_props['props_type'] = $request->props_type;

                     
                            $ths->props = json_encode($_props);
                            $ths->save();
                        }
                        
                    }

                    if ($content->type == 'photogallery') {
                        foreach ($languages as $language) {

                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';                                
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;
                            $_props['props_type'] = $request->props_type;
                            $_props['display_type'] = $request->display_type;

                  
                            $ths->props = json_encode($_props);
                            $ths->save();

                        }
                        
                    }

                    if ($content->type == 'code') {
                        foreach ($languages as $language) {

                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';                                
                            }

                            $_props = array();
                            $_props['props_section'] = $request->props_section;
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = $request->props_colvalue;
                          
                            $ths->props = json_encode($_props);
                            $ths->save();
                        }

                    }
                    if (starts_with($content->type , 'section')) {
                        foreach ($languages as $language) {

                            $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                            if(is_null($contentVariable)){
                                $contentVariable = new ContentVariable();
                                $contentVariable->content_id = $request->content_id;
                                $contentVariable->lang_code = $language->code;
                                $contentVariable->title = '';                                
                            }

                            $_props = array();
                            $_props['props_section'] = "section";
                            $_props['props_colortheme'] = $request->props_colortheme;
                            $_props['props_colvalue'] = "null";
                            $_props['props_fullwidth'] = $request->props_fullwidth;
                            $_props['section_props_section'] = $request->section_props_section;

                            
                            $_props['bg_img'] = $request->bg_img;
                            $ths->props = json_encode($_props);
                            $ths->save();
                        }

                    }

                    if (starts_with($content->type , 'group')) {

                            foreach ($languages as $language) {

                                $contentVariable = Content::find($request->content_id)->variableLang($language->code);
                                if(is_null($contentVariable)){
                                    $contentVariable = new ContentVariable();
                                    $contentVariable->content_id = $request->content_id;
                                    $contentVariable->lang_code = $language->code;
                                    $contentVariable->title = '';                                
                                }

                                $_props = array();
                                $_props['props_section'] = $request->props_section;
                                $_props['props_colortheme'] = $request->props_colortheme;
                                $_props['props_colvalue'] = $request->props_colvalue;
                           

                              if(!empty(config('webshell.props.menu.'.$menu->type))){
                                foreach (config('webshell.props.menu.'.$menu->type) as $key => $field){
                                    $_props['props_'.$key] = $request['props_'.$key];
                                }
                            }

                                $ths->props = json_encode($_props);
                                $ths->save();
                        }

                    }
            $tagItems = ContentHasTag::where('content_id', $request->content_id)->delete();
            if (!is_null($request->tag)) {
                foreach ($request->tag as $tt) {
                    $tagItem = new ContentHasTag();
                    $tagItem->content_id = $ths->sub_id;
                    $tagItem->tag_id = $tt;
                    $tagItem->save();
                }
            }

            $categoryItems = ContentHasCategory::where('content_id', $request->content_id)->delete();
            if (!is_null($request->category)) {
                //dd($ths->sub_id);
                foreach ($request->category as $cc) {
                    $categoryItem = new ContentHasCategory();
                    $categoryItem->content_id = $ths->sub_id;
                    $categoryItem->category_id = $cc;
                    $categoryItem->save();
                }
            }

            $text = 'Başarıyla Düzenlendi...';

        }else{

            //dd($request->input());

            if(is_null($request->top_content_id)){
              $ths= NewTopHasSub::where('sub_id',$request->content_id)->where('top_id',$request->menu_id)->where('sub_model','Content')->where('deleted','no')->first();  
            }
            else{
              $ths= NewTopHasSub::where('sub_id',$request->content_id)->where('top_id',$request->top_content_id)->where('sub_model','Content')->where('deleted','no')->first();  
            }
            $slugCount = ContentVariable::where('slug', '!=', '')->whereNotNull('slug')->where('slug', str_slug($request->slug, '-'))->count();
            $slugControl = true;
            if($slugCount > 0){
                $slugContentVars = ContentVariable::where('slug', '!=', '')->whereNotNull('slug')->where('slug', str_slug($request->slug, '-'))->get();
                foreach ($slugContentVars as $slugContentVar) {
                    if($slugContentVar->content_id == $request->content_id && $slugContentVar->lang_code == $request->lang){
                    }else{
                        $slugControl = false;
                    }
                            //dump($slugContentVar);
                }
            }
                    //dump($slugControl);
                    //dd($slugCount);

            $rules = array(
                'title' => 'required|max:255'
            );
            $validator = Validator::make(Input::all(), $rules);

            if (!$slugControl) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('slug', 'Bu alan benzersiz olmalı.');
                });
            }

            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
           }

           $content = Content::find($request->content_id);

           $contentVariable = Content::find($request->content_id)->variableLang($request->lang);
           $contentVariable->title = $request->title;
                    $contentVariable->slug = str_slug($request->slug, '-'); //SLUGGG
                    //dd($contentVariable);
                    if (starts_with($content->type , 'text')) {
                        $contentVariable->short_content = $request->short_content;
                        $contentVariable->content = $request->content;
                    }

                    if ($content->type == 'rssfeed') {
                        $_content = array();
                        $_content['content_url'] = $request->content_url;
                        $_content['content_rowcount'] = $request->content_rowcount;
                        $contentVariable->content = json_encode($_content);
                    }

                    if ($content->type == 'form') {
                        $contentVariable->content = $request->formdata;
                    }

                    if ($content->type == 'seperator') {
                        $contentVariable->short_content = $request->short_content;
                    }

                    if ($content->type == 'video') {
                        $_content = array();
                        $_content['embed_code'] = $request->embed_code;
                        $_content['type'] = $request->type;
                        $contentVariable->content = json_encode($_content);
                    }
                     if ($content->type == 'link') {
                        $_content = array();
                        $_content['content_url'] = $request->content_url;
                        $_content['content_target'] = ($request->content_target == 'external') ? 'external' : 'internal' ;
                        $contentVariable->content = json_encode($_content);
                    }

                    if ($content->type == 'photo') {}


                        if ($content->type == 'photogallery') {
                            $contentVariable->short_content = $request->short_content;
                        }

                        if ($content->type == 'code') {
                            $contentVariable->short_content = $request->short_content;
                            $contentVariable->content = $request->code;
                        }


                        if (starts_with($content->type, 'group')) {
                            $_content = json_decode($contentVariable->content);
                            $menu_new_ths = NewTopHasSub::find($request->menu_id);
                            $menu=$menu_new_ths->thatElementForMenu;
                              if(!empty($content->type)){
                                if(starts_with($content->type,'group')){
                                    $list_type=explode("-",$menu->type);
                                    $content_type=explode("-",$content->type);
                                    if($content_type[0]=='section'){
                                       $menu_type=$content->type;
                                    }
                                    elseif($list_type[1]!=$content_type[1]){
                                        $menu_type=$list_type[0]."-".$content_type[1];
                                    }
                                    elseif($content_type[0]=='section'){
                                       $menu_type=$content->type;
                                    }
                                    else{
                                       $menu_type =$menu->type;
                                    }

                                }
                                else{
                                    $menu_type=$content->type;
                         
                                }
                            }
                             foreach (config('webshell.menu_types.'.$menu_type.'.content') as $key => $value){
                                    if($value['type']!='photo'){
                                        $_content->$key = $request->$key;
                                    }
                                    
                            }
                           //dd(collect($_content));
                            if($menu_type=="list-product"){
                                if(!is_null($_content->discounted_price)){
                                    $current_price=$_content->discounted_price;
                                }else{
                                    $current_price=$_content->price;
                                }
                                if(!is_null($_content->tax_rate)){
                                    $tax_rate=$_content->tax_rate;
                                }
                                else{
                                    $tax_rate=0;
                                }
                                if(!empty($_content->final_price)){
                                    $_content->final_price=(( $current_price/100)*$tax_rate)+$current_price;
                                }else{
                                    $_content=collect($_content);
                                    $_content->put('final_price',(( $current_price/100)*$tax_rate)+$current_price) ;
                                }
                            }
                            if(!empty($_content->stock) ){
                                $contentVariable->stock=$_content->stock;
                               
                            }
                            if(!empty($_content->final_price)){
                                 $contentVariable->final_price=$_content->final_price;
                            }
                            
                            $contentVariable->content = json_encode($_content);
                        }

                        $contentVariable->save();
                        $text = 'Başarıyla Düzenlendi...';
                    }

                    if ($request->menu_id == 0) {
                        return redirect('form/list/')->with('message', array('text' => $text, 'status' => 'success'));
                    }elseif(!is_null($request->top_content_id)){
                       //dd($request->top_content_id);
                        if($ths->top_model=="Menu"){
                            return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));         
                        }else{
                            return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));  
                        }

                                 
                    }else{
                        return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));         
                    }

                }else if($request->crud == 'delete'){
                    //dd($request->input());
                    $ths= NewTopHasSub::find($request->content_ths_id);
                    //dd($ths);
                        $ths->deleted = 'yes';
                        $ths->status = 'passive';
                        $ths->save();

                    $text = 'Başarıyla Silindi...';

                        if($ths->top_model=="Menu"){
                            return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));         
                        }else{
                            return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));  
                        }
                }
        }

        public function content_addexist($id, $cid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu_new_ths = NewTopHasSub::find($id);
            $menu=$menu_new_ths->thatElementForMenu;

            if(is_null($id)){
                $content = new Content();
            }else{
                $ths=NewTopHasSub::find($cid);
                if(!empty($ths)){
                   $content = $ths->thatElementForContent;  
                }
                else{
                    $content=null; 
                }
               
            }

            $menus = Menu::join('new_top_has_sub','new_top_has_sub.sub_id','=','menu.id')
                           ->join('menuvariable','menuvariable.menu_id','=','new_top_has_sub.sub_id')
                           ->whereNotIn('type', ['mixed', 'link', 'content'])->where('deleted', 'no')->where('sub_model', 'Menu')->where('status', 'active')->orderBy('type', 'desc')->orderBy('order', 'asc')->get();
            
            $contents = Content::join('new_top_has_sub','new_top_has_sub.sub_id','=','content.id')
                           ->join('contentvariable','contentvariable.content_id','=','content.id')
                           ->whereIn('type', ['text', 'photo', 'form', 'code', 'photogallery', 'link', 'slide'])->where('deleted', 'no')->where('status', 'active')->orderBy('type', 'desc')->orderBy('order', 'asc')->get();
            $contents=$contents->unique('sub_id');
       
            $form = Content::orderBy('id', 'asc')->get();
            
            return view('menu.contentaddexist', array('menu' => $menu, 'menus' => $menus, 'content' => $content, 'contents' => $contents, 'form' => $form, 'langs' => $langs));
        }

        public function content_save_exist(Request $request)
        {   
            //dd($request->existcontent);
            
            $text = "";
            if($request->crud == 'addexist'){

                if(!is_null($request->existcontent)){

                    if (!is_null($request->menu_id) && !is_null($request->content_id)) {


                        $existcontent=Content::find($request->existcontent);
                        $top_has_sub = new NewTopHasSub();
                        $content_ths=NewTopHasSub::find($request->content_id);

                        $top_has_sub->top_id = $content_ths->sub_id;
                        $top_has_sub->top_model='Content';
                        $top_has_sub->sub_id = $request->existcontent;
                        $top_has_sub->sub_model = 'Content';
                        $top_has_sub->order = 200;

                       
                        $top_has_sub->props = $existcontent->topContentThs->first()->props;

                        $top_has_sub->save();

                        

                    } elseif (!is_null($request->menu_id) && is_null($request->content_id)) {
                        $menu_ths=NewTopHasSub::find($request->menu_id);
                        $existcontent=Content::find($request->existcontent);
                        //dd($existcontent);

                        $top_has_sub = new NewTopHasSub();
                        $top_has_sub->top_id = $menu_ths->sub_id;
                        $top_has_sub->top_model='Menu';
                        $top_has_sub->sub_id = $request->existcontent;
                        $top_has_sub->sub_model = 'Content';

                        $top_has_sub->order = 200;

                        $top_has_sub->props = $existcontent->topContentThs->first()->props;


                        $top_has_sub->save();

                    } elseif (is_null($request->menu_id) && !is_null($request->content_id)) {

                        dd("null : !null");

                    } elseif (is_null($request->menu_id) && is_null($request->content_id)) {

                        dd("null : null");

                    }

                }elseif (!is_null($request->existmenu)){

                    if (!is_null($request->menu_id) && !is_null($request->content_id)) {
                        $content_ths=NewTopHasSub::find($request->content_id);
                        $top_has_sub = new NewTopHasSub();

                        $top_has_sub->top_id =  $content_ths->sub_id;
                        $top_has_sub->top_model='Content';
                        $top_has_sub->sub_id = $request->existmenu;
                        $top_has_sub->sub_model = 'MenuSum';

                        $top_has_sub->order = 200;

                    
                        $_props = array();
                        $_props['props_sum_type'] = '';
                        $_props['props_sum_count'] = '';
                        $_props['props_section'] = 'container';
                        $_props['props_colortheme'] = 'light';
                        $_props['props_sum_colvalue'] = 'col-lg-12';//default sütün genişliği atandı
                        $_props['tag'] = 'no';
                        $_props['display_type'] = 'horizontal';
                        $top_has_sub->props = json_encode($_props);
                   

                        $top_has_sub->save();

                    } elseif (!is_null($request->menu_id) && is_null($request->content_id)) {
                        $menu_ths=NewTopHasSub::find($request->menu_id);
                        $top_has_sub = new NewTopHasSub();
                        $top_has_sub->top_id = $menu_ths->sub_id;
                        $top_has_sub->top_model='Menu';
                        $top_has_sub->sub_id = $request->existmenu;
                        $top_has_sub->sub_model = 'MenuSum';

                        $top_has_sub->order = 200;

                    
                        $_props = array();
                        $_props['props_sum_type'] = '';
                        $_props['props_sum_count'] = '';
                        $_props['props_section'] = 'container';
                        $_props['props_colortheme'] = 'light';
                        $_props['props_sum_colvalue'] = 'col-lg-12';//default sütün genişliği atandı
                        $_props['tag'] = 'no';
                        $_props['display_type'] = 'horizontal';
                        $top_has_sub->props = json_encode($_props);
                   

                        $top_has_sub->save();

                    } elseif (is_null($request->menu_id) && !is_null($request->content_id)) {

                        dd("null : !null");

                    } elseif (is_null($request->menu_id) && is_null($request->content_id)) {

                        dd("null : null");

                    }

                }

                $text = 'Başarıyla Eklendi...';
                return redirect('menu/content/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

            }

        }
        /** CONTENT **/

        /** FORM LIST-DETAIL **/
        public function form_list()
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            return view('form.list', array('langs' => $langs));
        }

        public function form_detail($formid)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $form = Content::join('new_top_has_sub','new_top_has_sub.sub_id','=','content.id')->where('new_top_has_sub.id', $formid)->where('type', 'form')->where('deleted', 'no')->first();
            $form_ths=NewTopHasSub::find($formid);
            $formDist = FormData::select('source_id', 'source_type')->where('form_id',$form_ths->sub_id)->distinct()->get();

            if (!is_null($form)) {
                return view('form.detail', array('form' => $form, 'formDist' => $formDist, 'langs' => $langs));
            }
            
        }

        public function form_change_visible($formid, $dataid)
        {

            $formdata = FormData::where('id', $dataid)->orderBy('id', 'desc')->first();

            if($formdata->visible == 'yes'){
                $formdata->visible = 'no';
            }elseif($formdata->visible == 'no'){
                $formdata->visible = 'yes';
            }

            $formdata->save();

            $text = 'İşlem Başarıyla Gerçekleştirildi...';
            return redirect('form/detail/'.$formid)->with('message', array('text' => $text, 'status' => 'success'));

        }
        /** FORM LIST-DETAIL **/

        /** ADD-ON **/
        public function addons($id, $cid)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            if(is_null($id)){
                $content = new Content();
            }else{
                $content = Content::find($cid);
            }
            //$photogalleryitem = ContentPhotoGalleryVariable::find($fid);
            return view('menu.addons', array('menu' => $menu, 'content' => $content, 'langs' => $langs));
        }

        public function addon_add_edit($id, $cid, $aid = null, $lang = null)
        {

            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = CategoryTopHasSub::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

            $menu = Menu::find($id);
            $content = Content::find($cid);

            if(is_null($aid)){
                $addon = new Content();
            }else{
                $addon = Content::find($aid);
                if(!is_null($lang) && is_null($addon->variableLang($lang))){
                    $addonVariable = new ContentVariable();
                    $addonVariable->content_id = $addon->id;
                    $addonVariable->lang_code = $lang;
                    $addonVariable->title = '';
                    $addonVariable->save();
                }
            }
            
            return view('menu.addoncrud', array('menu' => $menu, 'content' => $content, 'addon' => $addon, 'langs' => $langs, 'tags' => $tags, 'categories' => $categories));
        }

        public function addon_delete($id, $cid, $aid = null)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $categories = CategoryTopHasSub::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $menu = Menu::find($id);
            $content = Content::find($cid);
            $addon = Content::find($aid);
            return view('menu.addoncrud', array('menu' => $menu, 'content' => $content, 'addon' => $addon, 'langs' => $langs, 'activeLang' => $activeLang, 'tags' => $tags, 'categories' => $categories));
        }

        public function addon_save(Request $request)
        {
            //dd($request->input());
            if($request->crud == 'add'){
                $rules = array(
                    'title' => 'required|max:255',
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
               }
               $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
               $addon = new Content();
               $addon->top_content = $request->content_id;
               if ($request->type != 'null') {
                $addon->type = $request->type.'';
            }
            $addon->order = $request->order;
            $addon->status = ($request->status == 'active') ? 'active' : 'passive';
            $addon->save();

            foreach ($languages as $language) {
                $addonVariable = new ContentVariable();
                $addonVariable->content_id = $addon->id;
                $addonVariable->lang_code = $language->code;
                $addonVariable->title = $request->title;
                $addonVariable->save();
            }

            $text = 'Başarıyla Eklendi...';
            return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));

        }elseif($request->crud == 'edit'){
            if (is_null($request->lang)) {
                $rules = array(
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
               }
               $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

               $addon = Content::find($request->addon_id);
               $addon->order = $request->order;
               $addon->status = ($request->status == 'active') ? 'active' : 'passive';
               $addon->save();

               if ($addon->type == 'photogallery') {
                foreach ($languages as $language) {
                    $addonVariable = Content::find($request->addon_id)->variableLang($language->code);
                    $addonVariable->props = $request->props;
                    $addonVariable->save();
                }
            }

            $tagItems = ContentHasTag::where('content_id', $request->addon_id)->delete();
            if (!is_null($request->tag)) {
                foreach ($request->tag as $tt) {
                    $tagItem = new ContentHasTag();
                    $tagItem->content_id = $request->addon_id;
                    $tagItem->tag_id = $tt;
                    $tagItem->save();
                }
            }

            $categoryItems = ContentHasCategory::where('content_id', $request->addon_id)->delete();
            if (!is_null($request->category)) {
                foreach ($request->category as $cc) {
                    $categoryItem = new ContentHasCategory();
                    $categoryItem->content_id = $request->addon_id;
                    $categoryItem->category_id = $cc;
                    $categoryItem->save();
                }
            }

            $text = 'Başarıyla Düzenlendi...';
        }else{
            $rules = array(
                'title' => 'required|max:255'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
           }
           $addon = Content::find($request->addon_id);
           $addonVariable = Content::find($request->addon_id)->variableLang($request->lang);
           $addonVariable->title = $request->title;
           if ($addon->type == 'photogallery') {
            $addonVariable->props = $request->props;
        }
        if ($addon->type == 'text') {
        }else{
            $addonVariable->row = 'normal';
            $addonVariable->height = null;
        }
        $addonVariable->save();
        $text = 'Başarıyla Düzenlendi...';
    }
    return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));

}elseif($request->crud == 'delete'){
                //dd($request->input());

                //MenuHasContent::where('menu_id', $request->menu_id)->where('content_id', $request->addon_id)->delete();
    if(MenuHasContent::where('content_id', $request->addon_id)->count() == 0){
        $addon = Content::find($request->addon_id);
        $addon->deleted = 'yes';
        $addon->status = 'passive';
        $addon->save();
        $text = 'Başarıyla Silindi...';
    }else{
        $addon = Content::find($request->addon_id);
        $addon->top_content = null;
        $addon->save();
        $text = 'Başarıyla Silindi...';
    }
    return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));
}
return redirect('menu/content/'.$request->menu_id.'/addons/'.$addon->top_content)->with('message', array('text' => $text, 'status' => 'success'));
}
/** ADD-ON **/

/** TAG **/
public function tag()
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    return view('menu.tag', array('langs' => $langs));
}

public function tag_add_edit($tid = null, $lang = null)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    if(is_null($tid)){
        $tag = new Tag();
    }else{
        $tag = Tag::find($tid);
    }
    return view('menu.tagcrud', array('tag' => $tag, 'langs' => $langs));
}
public function tag_content($tid)
{
    $content_has_tag_ids=ContentHasTag::where('tag_id',$tid)->pluck('content_id');
    $tagVariable=TagVariable::where('tag_id',$tid)->first();
    $tag_content=ContentVariable::whereIn('content_id',$content_has_tag_ids)->get();
    
    return view('menu.tagcontent', array('tag_content' => $tag_content,'tagVariable' => $tagVariable));
}

public function tag_delete($tid)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $tag = Tag::find($tid);
    return view('menu.tagcrud', array('tag' => $tag, 'langs' => $langs));
}

public function tag_save(Request $request)
{
    //dd($request->input());
    $text = "";
    if ($request->crud == 'add') {
        $rules = array(
            'title' => 'required|max:255',
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
       $tag = new Tag();
       $tag->order = $request->order;
       $tag->status = ($request->status == 'active') ? 'active' : 'passive';
       $tag->save();
       foreach ($languages as $language) {
        $tagVariable = new TagVariable();
        $tagVariable->tag_id = $tag->id;
        $tagVariable->lang_code = $language->code;
        $tagVariable->title = $request->title;
        $tagVariable->slug = str_slug($request->title.'-'.$language->code, '-');  //SLUGGG
        $slugCount = TagVariable::where('slug', $tagVariable->slug)->count();
        if($slugCount!=0){
            $random = str_random(3);
            $tagVariable->slug=$tagVariable->slug.$random;
        }
        $tagVariable->save();
    }
    $text = 'Başarıyla Eklendi...';
}else if($request->crud == 'edit'){
    if (is_null($request->lang)) {
        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       $tag = Tag::find($request->tag_id);
       $tag->order = $request->order;
       $tag->status = ($request->status == 'active') ? 'active' : 'passive';
       $tag->save();
   }else{
                    //dd($request->input());
    $rules = array(
        'title' => 'required'
    );
    $validator = Validator::make(Input::all(), $rules);
    if ($validator->fails()) {
       return \Redirect::back()->withErrors($validator)->withInput();
   }
   $tagVariable = Tag::find($request->tag_id)->variableLang($request->lang);
   if (!isset($tagVariable)) {
    $tagVariable = new TagVariable();
    $tagVariable->tag_id = $request->tag_id;
    $tagVariable->lang_code = $request->lang;
}
$tagVariable->title = $request->title;
$tagVariable->save();
}
$text = 'Başarıyla Düzenlendi...';
}else if($request->crud == 'delete'){
    $tag = Tag::find($request->tag_id);
    $tag->deleted = 'yes';
    $tag->status = 'passive';
    $tag->save();
    $text = 'Başarıyla Silindi...';
}
return redirect('tag')->with('message', array('text' => $text, 'status' => 'success'));
}
/** TAG **/

/** CATEGORY **/
public function category()
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

    return view('menu.category', array('langs' => $langs));
}

public function category_add_edit($cid = null, $lang = null)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $topcats=CategoryTopHasSub::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    if(is_null($cid)){
        $category = new CategoryTopHasSub();
        $top_cat = new CategoryTopHasSub();
    }else{
        $category = CategoryTopHasSub::find($cid);
        $top_cat=CategoryTopHasSub::where('id',$cid)->first();
    }
    return view('menu.categorycrud', array('category' => $category, 'langs' => $langs, 'topcats' => $topcats, 'top_cat' => $top_cat));
}

public function subcat2($_chts,$_array){
        foreach ($_chts as $_cht) {
            if($_cht->subCat->count() == 0){
                array_push($_array,$_cht->id);
            }else{
                $_array = array_merge($_array, $this->subcat2($_cht->subCat, $_array));
            }
        } 
        return array_unique($_array);
    }
function topcats($cths ,$catnames){

}

public function category_content($cid)
{   
    
    $arr=array();
    
    $chts=CategoryTopHasSub::where('id',$cid)->get();
    $content_has_cat_ids=ContentHasCategory::whereIn('category_id',$this->subcat2($chts,$arr))->pluck('content_id');

    //dd( $content_has_cat_ids);
    $cat_Variable=categoryVariable::where('category_id',$chts[0]->category_id)->first();

    $cat_content=ContentVariable::whereIn('content_id', $content_has_cat_ids)->get();
    
    return view('menu.categorycontent', array('cat_Variable' => $cat_Variable, 'cat_content' => $cat_content));
}
public function category_delete($cid)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $category = CategoryTopHasSub::find($cid);
    return view('menu.categorycrud', array('category' => $category, 'langs' => $langs));
}

public function category_save(Request $request)
{
            //dd($request->input());
    $text = "";
    if ($request->crud == 'add') {
        $rules = array(
            'title' => 'required|max:255',
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        
       $category = new Category();
       $category->save();

       $Cths =new CategoryTopHasSub;
       if($request->top_cat=='null'){
           $Cths->top_category_id = NULL;
       }
       else{
           $Cths->top_category_id = $request->top_cat;
       } 
       $Cths->category_id = $category->id;
       $Cths->order = $request->order;
       $Cths->status = ($request->status == 'active') ? 'active' : 'passive';
       $Cths->save();
       
        

       foreach ($languages as $language) {
        $categoryVariable = new CategoryVariable();
        $categoryVariable->category_id = $category->id;
        $categoryVariable->lang_code = $language->code;
        $categoryVariable->title = $request->title;
        $categoryVariable->slug = str_slug($request->title.'-'.$language->code, '-');  //SLUGGG
        $slugCount = CategoryVariable::where('slug', $categoryVariable->slug)->count();
        if($slugCount!=0){
            $random = str_random(3);
            $categoryVariable->slug=$categoryVariable->slug.$random;
        }
        $categoryVariable->save();
           
    }
    $text = 'Başarıyla Eklendi...';
}else if($request->crud == 'edit'){
    if (is_null($request->lang)) {
        //dd($request->input());
        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       $Cths = CategoryTopHasSub::where('id',$request->category_id)->first();
       $Cths->order = $request->order;
       $Cths->top_category_id = $request->top_cat;
       $Cths->status = ($request->status == 'active') ? 'active' : 'passive';
       $Cths->save();
    }else{
        
        $rules = array(
            'title' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
        }
        $Cths = CategoryTopHasSub::where('id',$request->category_id)->first();
     
        $categoryVariable = CategoryVariable::where('category_id',$Cths->category_id)->first();
        //dd($categoryVariable);
        if (!isset($categoryVariable)) {
            $categoryVariable = new CategoryVariable();
            $categoryVariable->category_id = $request->category_id;
            $categoryVariable->lang_code = $request->lang;
        }
        $categoryVariable->title = $request->title;
        $categoryVariable->slug = $request->slug; //SLUGGG
        $slugCount = CategoryVariable::where('slug', $categoryVariable->slug)->count();
        if($slugCount!=0){
            $random = str_random(3);
            $categoryVariable->slug=$categoryVariable->slug.$random;
        }
        $categoryVariable->save();
    }
    $text = 'Başarıyla Düzenlendi...';
    }else if($request->crud == 'delete'){
        $category = CategoryTopHasSub::find($request->category_id);
        $category->deleted = 'yes';
        $category->status = 'passive';
        $category->save();
        $text = 'Başarıyla Silindi...';
    }
return redirect('category')->with('message', array('text' => $text, 'status' => 'success'));
}
/** CATEGORY **/

/** PHOTO GALLERY **/
public function photogalleryitem($id, $cid, $fid, $lang = null)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;
    
    if(is_null($id)){
        $content = new Content();
    }else{
        $ths=NewTopHasSub::find($cid);
        if(!empty($ths)){
           $content = $ths->thatElementForContent;  
        }
        else{
            $content=null; 
        }
       
    }
    $photogalleryitem = ContentPhotoGalleryVariable::find($fid);
    return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
}

public function photogalleryitem_delete($id, $cid, $fid, $lang = null)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;

    if(is_null($id)){
        $content = new Content();
    }else{
        $ths=NewTopHasSub::find($cid);
        if(!empty($ths)){
           $content = $ths->thatElementForContent;  
        }
        else{
            $content=null; 
        }
       
    }
    $photogalleryitem = ContentPhotoGalleryVariable::find($fid);
    return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
}

public function photogalleryitem_save(Request $request)
{
            //dd($request->input());
    $text = "";
    if ($request->crud == 'photogallery_edit') {
        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       if($request->crud == 'photogallery_edit'){

        $photogalleryitem = ContentPhotoGalleryVariable::find($request->photo_gallery_id);
        $photogalleryitem->name = $request->name;
        $photogalleryitem->description = $request->description;
        $photogalleryitem->order = $request->order;
        $photogalleryitem->status = ($request->status == 'active') ? 'active' : 'passive';
        $photogalleryitem->save();
        $text = 'Başarıyla Düzenlendi...';

    }
}else if($request->crud == 'photogallery_delete'){
    $photogalleryitem = ContentPhotoGalleryVariable::find($request->photo_gallery_id);
    $photogalleryitem->deleted = 'yes';
    $photogalleryitem->status = 'passive';
    $photogalleryitem->save();
    $text = 'Başarıyla Silindi...';
}
return redirect('menu/content/'.$request->menu_id.'/edit/'.$request->content_id.'/'.$request->lang)->with('message', array('text' => $text, 'status' => 'success'));
}
/** PHOTO GALLERY **/

/** SLIDE GALLERY **/
public function slideitem($id, $cid, $fid, $lang = null)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;
    if(is_null($cid)){
        $content = new Content();
    }else{
        $ths=NewTopHasSub::find($cid);
        if(!empty($ths)){
           $content = $ths->thatElementForContent;  
        }
        else{
            $content=null; 
        }
       
    }
    $slideitem = ContentSlideVariable::find($fid);
    return view('menu.slideitem', array('menu' => $menu, 'content' => $content, 'slideitem' => $slideitem, 'activeLang' => $activeLang));
}

public function slideitem_delete($id, $cid, $fid, $lang = null)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;
       $ths=NewTopHasSub::find($cid);
        if(!empty($ths)){
           $content = $ths->thatElementForContent;  
        }
    $slideitem = ContentSlideVariable::find($fid);
    return view('menu.slideitem', array('menu' => $menu, 'content' => $content, 'slideitem' => $slideitem, 'activeLang' => $activeLang));
}

public function slideitem_save(Request $request)
{
            //dd($request->input());
    $text = "";
    if ($request->crud == 'slide_edit') {
        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       if($request->crud == 'slide_edit'){

        $slideitem = ContentSlideVariable::find($request->slide_id);
        $slideitem->title = $request->title;
        $slideitem->description = $request->description;

        $slideitem->button_text = $request->button_text;
        $slideitem->button_url = $request->button_url;
        $slideitem->theme = $request->theme;
        $slideitem->align = $request->align;

        $slideitem->order = $request->order;
        $slideitem->status = ($request->status == 'active') ? 'active' : 'passive';
        $slideitem->save();
        $text = 'Başarıyla Düzenlendi...';

    }
}else if($request->crud == 'slide_delete'){
    $slideitem = ContentSlideVariable::find($request->slide_id);
    $slideitem->deleted = 'yes';
    $slideitem->status = 'passive';
    $slideitem->save();
    $text = 'Başarıyla Silindi...';
}

return redirect('menu/content/'.$request->menu_id.'/edit/'.$request->content_id.'/'.$request->lang)->with('message', array('text' => $text, 'status' => 'success'));
}
/** SLIDE GALLERY **/

/** ADDON PHOTO GALLERY **/
public function addon_photogalleryitem($id, $cid, $aid, $fid)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);
    if(is_null($id)){
        $content = new Content();
    }else{
        $content = Content::find($cid);
    }
    $photogalleryitem = ContentPhotoGalleryVariable::find($fid);
    return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
}

public function addon_photogalleryitem_delete($id, $cid, $aid, $fid)
{
    $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu = Menu::find($id);
    $content = Content::find($cid);
    $photogalleryitem = ContentPhotoGalleryVariable::find($fid);
    return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem, 'activeLang' => $activeLang));
}

public function addon_photogalleryitem_save(Request $request)
{
    $text = "";
    if ($request->crud == 'photogallery_edit') {
        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       if($request->crud == 'photogallery_edit'){

        $photogalleryitem = ContentPhotoGalleryVariable::find($request->fid);
        $photogalleryitem->name = $request->name;
        $photogalleryitem->description = $request->description;
        $photogalleryitem->order = $request->order;
        $photogalleryitem->status = ($request->status == 'active') ? 'active' : 'passive';
        $photogalleryitem->save();
        $text = 'Başarıyla Düzenlendi...';

    }
}else if($request->crud == 'photogallery_delete'){
    $photogalleryitem = ContentPhotoGalleryVariable::find($request->fid);
    $photogalleryitem->deleted = 'yes';
    $photogalleryitem->status = 'passive';
    $photogalleryitem->save();
    $text = 'Başarıyla Silindi...';
    return redirect('menu/content/'.$request->menu_id.'/galup/'.$request->id)->with('message', array('text' => $text, 'status' => 'success'));
}
return redirect('menu/content/'.$request->menu_id.'/galup/'.$request->id)->with('message', array('text' => $text, 'status' => 'success'));
}
/** ADDON PHOTO GALLERY **/

/** SLIDER **/
public function stslider($id = null)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $topmenus = NewTopHasSub::where('deleted', 'no')->get();

    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;
    return view('menu.stslider', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
}

public function stslider_add_edit($id, $sid = null)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    
    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;

    if(is_null($id)){
        $slide = new Slider();
    }else{
        $slide = Slider::find($sid);
    }
    return view('menu.stslidercrud', array('menu' => $menu, 'slide' => $slide, 'langs' => $langs));
}

public function stslider_delete($id, $sid = null)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;
    if(is_null($id)){
        $slide = new Slider();
    }else{
        $slide = Slider::find($sid);
    }
    return view('menu.stslidercrud', array('menu' => $menu, 'slide' => $slide, 'langs' => $langs));
}

public function stslider_save(Request $request)
{
           // dd($request->input());
    $text = "";
    if($request->crud == 'add'){

        $rules = array(
            'title' => 'required|max:255',
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }

       $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
       $menu_new_ths = NewTopHasSub::find($request->menu_id);
       $menu=$menu_new_ths->thatElementForMenu;

       $slide = new Slider();
       $slide->menu_id =  $menu->id;
       $slide->theme = $request->theme;
       $slide->align = $request->align;
       $slide->type = $request->type;
       $slide->order = $request->order;
       $slide->status = ($request->status == 'active') ? 'active' : 'passive';
       $slide->save();

       foreach ($languages as $language) {
        $slideVariable = new SliderVariable();
        $slideVariable->slider_id = $slide->id;
        $slideVariable->lang_code = $language->code;
        $slideVariable->title = $request->title;
        $slideVariable->description = $request->description;
        $slideVariable->button_text = $request->button_text;
        $slideVariable->button_url = $request->button_url;
        $slideVariable->save();
    }

    $text = 'Başarıyla Eklendi...';
    return redirect('menu/stslider/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

}else if($request->crud == 'edit'){

    if (is_null($request->lang)) {

        $rules = array(
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }

       $slide = Slider::where('id', $request->slider_id)->first();
       $slide->theme = $request->theme;
       $slide->align = $request->align;
       $slide->order = $request->order;
       $slide->status = ($request->status == 'active') ? 'active' : 'passive';
       $slide->save();

   }else{

    $rules = array(
        'title' => 'required|max:255'
    );
    $validator = Validator::make(Input::all(), $rules);
    if ($validator->fails()) {
       return \Redirect::back()->withErrors($validator)->withInput();
   }

   $slide = Slider::where('id', $request->slider_id)->first();
   $slideVariable = $slide->variableLang($request->lang);
   $slideVariable->title = $request->title;
   $slideVariable->description = $request->description;
   $slideVariable->button_text = $request->button_text;
   $slideVariable->button_url = $request->button_url;
   $slideVariable->save();

}
return redirect('menu/stslider/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

}else if($request->crud == 'delete'){

    $slide = Slider::find($request->slider_id);
    $slide->deleted = 'yes';
    $slide->status = 'passive';
    $slide->save();
    $text = 'Başarıyla Silindi...';
    return redirect('menu/stslider/'.$request->menu_id)->with('message', array('text' => $text, 'status' => 'success'));

}
}
/** SLIDER **/

/** MAP MARKER **/
public function mapmarker($id, $cid)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;

    if(is_null($id)){
        $content = new Content();
    }else{
        $ths=NewTopHasSub::find($cid);
        if(!empty($ths)){
           $content = $ths->thatElementForContent;  
        }
        else{
            $content=null; 
        }
       
    }

    return view('menu.mapmarker', array('menu' => $menu, 'content' => $content, 'langs' => $langs));
}

public function mapmarker_add($id, $cid)
{
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;

    $districts = District::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $cities = City::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $counties = County::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $ths=NewTopHasSub::find($cid);
    $content = $ths->thatElementForContent;
    $staff = new Staff();

    return view('menu.mapmarkercrud', array('menu' => $menu, 'content' => $content, 'districts' => $districts, 'cities' => $cities, 'counties' => $counties, 'staff' => $staff, 'langs' => $langs));
}

public function mapmarker_edit($id, $cid, $sid, $lang = null)
{
            //dd($id.' - '.$cid);
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;

    $districts = District::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $cities = City::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $counties = County::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

    $content = Content::find($cid);
    $staff = Staff::find($sid);

    return view('menu.mapmarkercrud', array('menu' => $menu, 'content' => $content, 'districts' => $districts, 'cities' => $cities, 'counties' => $counties, 'staff' => $staff, 'langs' => $langs));
}

public function mapmarker_delete($id, $cid, $sid, $lang = null)
{
            //dd($id.' - '.$cid);
    $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $menu_new_ths = NewTopHasSub::find($id);
    $menu=$menu_new_ths->thatElementForMenu;

    $districts = District::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $cities = City::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    $counties = County::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

    $content = Content::find($cid);
    $staff = Staff::find($sid);

    return view('menu.mapmarkercrud', array('menu' => $menu, 'content' => $content, 'districts' => $districts, 'cities' => $cities, 'counties' => $counties, 'staff' => $staff, 'langs' => $langs));
}

public function mapmarker_save(Request $request)
{
        //dd($request->input());
    $text = "";
    if($request->crud == 'add'){

        $rules = array(
            'name' => 'required|max:255',
            'order' => 'numeric|min:1|max:100000'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
       }
       $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

       $menu = Menu::find($request->menu_id);
       $content = Content::find($request->content_id);

       $staff = new Staff();
       $staff->content_id = $request->content_id;

       if ($request->district == 'null') { 
        $staff->district_id = null; 
    }else{
        $staff->district_id = $request->district; 
    }

    if ($request->city == 'null') { 
        $staff->city_id = null; 
    }else{
        $staff->city_id = $request->city; 
    }

    if ($request->county == 'null') { 
        $staff->county_id = null; 
    }else{
        $staff->county_id = $request->county; 
    }

    $staff->order = $request->order;
    $staff->status = ($request->status == 'active') ? 'active' : 'passive';

    $staff->save();

    foreach ($languages as $language) {

        $staffVariable = new StaffVariable();
        $staffVariable->staff_id = $staff->id;
        $staffVariable->lang_code = $language->code;
        $staffVariable->name = $request->name;
                    $staffVariable->slug = str_slug($request->name, '-');  //SLUGGG
                    $staffVariable->title = $request->ptitle;
                    $staffVariable->address = $request->address;
                    $staffVariable->email = $request->email;
                    $staffVariable->phone = $request->phone;
                    $staffVariable->gsm = $request->gsm;

                    $staffVariable->photo_url = '';
                    $staffVariable->save();
                }

                $text = 'Başarıyla Eklendi...';
                return redirect('menu/content/'.$menu->topHasSub->first()->id.'/mapmarker/'.$content->topContentThs->first()->id)->with('message', array('text' => $text, 'status' => 'success')); 

            }else if($request->crud == 'edit'){

                if (is_null($request->lang)) {
                    $rules = array(
                        'order' => 'numeric|min:1|max:100000'
                    );
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->fails()) {
                       return \Redirect::back()->withErrors($validator)->withInput();
                   }

                   $menu = Menu::find($request->menu_id);
                   $content = Content::find($request->content_id);


                   $staff = Staff::find($request->staff_id);

                   $staff->order = $request->order; 
                   $staff->status = ($request->status == 'active') ? 'active' : 'passive';
                   $staff->save();

                   $text = 'Başarıyla Düzenlendi...';
               }else{
                    //dump($request->input());
                $rules = array(
                    'name' => 'required|max:255'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
               }

               $menu = Menu::find($request->menu_id);
               $content = Content::find($request->content_id);



               $staff = Staff::find($request->staff_id);

               if ($request->district == 'null') { 
                $staff->district_id = null; 
            }else{
                $staff->district_id = $request->district; 
            }

            if ($request->city == 'null') { 
                $staff->city_id = null; 
            }else{
                $staff->city_id = $request->city; 
            }

            if ($request->county == 'null') { 
                $staff->county_id = null; 
            }else{
                $staff->county_id = $request->county; 
            }
            $staff->save();

            $staffVariable = $staff->variableLang($request->lang);
            $staffVariable->name = $request->name;
                    $staffVariable->slug = str_slug($request->name, '-');  //SLUGGG
                    $staffVariable->title = $request->ptitle;
                    $staffVariable->address = $request->address;
                    $staffVariable->email = $request->email;
                    $staffVariable->phone = $request->phone;
                    $staffVariable->gsm = $request->gsm;
                    $staffVariable->save();

                    $text = 'Başarıyla Düzenlendi...';
                }

                return redirect('menu/content/'.$menu->topHasSub->first()->id.'/mapmarker/'.$content->topContentThs->first()->id)->with('message', array('text' => $text, 'status' => 'success')); 
                
            }else if($request->crud == 'delete'){
                $menu = Menu::find($request->menu_id);
                $content = Content::find($request->content_id);
                $staff = Staff::find($request->staff_id);

                $staff->deleted = 'yes';
                $staff->status = 'passive';
                $staff->save();
                $text = 'Başarıyla Silindi...';
                return redirect('menu/content/'.$menu->topHasSub->first()->id.'/mapmarker/'.$content->topContentThs->first()->id)->with('message', array('text' => $text, 'status' => 'success')); 
            }
        }
        /** MAP MARKER **/

        public function stimage($id)
        {
            $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            $topmenus = NewTopHasSub::where('deleted', 'no')->get();
            $menu_new_ths = NewTopHasSub::find($id);
            $menu=$menu_new_ths->thatElementForMenu;
            return view('menu.stimage', array('menu' => $menu, 'topmenus' => $topmenus, 'langs' => $langs));
        }

        /**ORDERS*/
        public function orders()
        {
            return view('orders.list');

        }
        public function orderdetail($oid)
        {
            $order=ShoppingOrder::find($oid);
            $allcontent=ContentVariable::all();
            $customers_info=json_decode($order->address_information)->adress;
            $adress=json_decode($customers_info)->address;
            $town_city=json_decode($order->address_information)->town."/".json_decode($order->address_information)->city;
            $phone=json_decode($customers_info)->phone;

            if(!empty($order->user_id)){
                $customer_name=json_decode($order->address_information)->firstname." ".json_decode($order->address_information)->lastname;
            }else{
                 $customer_name=json_decode($customers_info)->firstname." ".json_decode($customers_info)->lastname;
            }
            return view('orders.detail',array('order' => $order,'customer_name' => $customer_name,'allcontent' => $allcontent,'adress' => $adress,'phone' => $phone,'town_city' => $town_city));

        }
        public function order_save(Request $request)
        {
            //dd($request->input());
            $order=ShoppingOrder::find($request->order_id);

            $order->status=$request->orderstatus;

            $order->save();

            $data=$order;
            
            Mail::to("uygar@ideakup.com")->send(new OrderShippedInfo($data));
            return redirect('orders/list');
        }
        /**Store*/
        public function store()
        {
            return view('store.list');

        }
        public function store_settings()
        {
            $shop_settigs=ShoppingOrderSettings::find(1);
            
            return view('store.settings',array('shop_settigs' => $shop_settigs));

        }
        public function store_save(Request $request)
        {

            //dd($request->input());
            $shop_settigs=ShoppingOrderSettings::find(1);
          
            $shop_settigs->shipping_cost = ($request->shipping_cost == 'active') ? 'active' : 'passive';
            $shop_settigs->shipping_type=$request->shipping_type;
            $shop_settigs->shipping_cost_price=$request->shipping_cost_price;
            $shop_settigs->min_shippingcost=$request->min_shippingcost;
            $shop_settigs->shopping_text=$request->shopping_text;


            $payment_info = array();
            $payment_info['payment_method'] = $request->payment_method;
            $payment_info['data1'] = $request->data1;
            $payment_info['data2'] = $request->data2;
            $payment_info['data3'] = $request->data3;
            $payment_info['url'] = $request->url;


            $shop_settigs->extra=json_encode($payment_info);

            $shop_settigs->save();
            return redirect('store/list');
        }
        public function importconfirm(Request $request)
        {
            
            $products=json_decode($request->after_products[0]);
            //dd($products);

            foreach($products as $product){
                $product_varible=ContentVariable::where('content_id',$product->content_id)->first();
               // dd($product);

                $product_varible->title=$product->title;
                $product_varible->content=$product->content;
                $product_varible->stock=$product->stock;
                $product_varible->final_price=$product->final_price;

                $product_varible->save();
               
            }




            return redirect('store/list');


        }
      
 

    }
