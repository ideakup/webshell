<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\ContentVariable;
use Validator;
use Illuminate\Support\Facades\Input;


class İmportController extends Controller
{
    //
    public function productDataİmport(Request $request) 
    {
        $rules = array(
            'product_file' => 'required|mimetypes:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
        }
       
        //$product_varible=ContentVariable::where('content_id',83)->first();
        if($request->hasFile('product_file')){  
             $data = Excel::toArray( array(), request()->file('product_file'), \Maatwebsite\Excel\Excel::XLSX);

             $data=$data[0];
             $before_products = array();
             $after_products = array();
             for($i=6; $i<count($data); $i++){

                    $content_id=$data[$i][1];
                    $product_name=$data[$i][2];   
                    $product_stock=$data[$i][3];
                    $product_tax_rate=$data[$i][4];
                    $product_price=$data[$i][5];
                    $product_discounted_price=$data[$i][6];

                    //dd($product_tax_rate);

                    $product_varible1=ContentVariable::where('content_id',$content_id)->first();

                    array_push($before_products,$product_varible1);

                    $product_varible=ContentVariable::where('content_id',$content_id)->first();

                    if(!is_numeric($product_price) && !is_numeric($product_discounted_price)){
                       $product_price=null; 
                       $product_discounted_price=null;
                    }

                    $product_content=json_decode($product_varible->content);
                    if(!is_null($product_discounted_price)){
                        $current_price=$product_discounted_price;
                    }else{
                        $current_price=$product_price;
                    }
                    if(!is_null($product_tax_rate)){
                        $tax_rate=$product_tax_rate;
                    }
                    else{
                        $tax_rate=0;
                    }

                    $product_final_price=(( $current_price/100)*$tax_rate)+$current_price;
                    
                    //-------update-------
                    $product_varible->title=$product_name;
                    $product_varible->stock=$product_stock;
                    $product_varible->final_price=$product_final_price;

                    $product_content->price =number_format((float)$product_price, 2, '.', '');
                    $product_content->discounted_price = $product_discounted_price;
                    $product_content->stock = $product_stock;
                    $product_content->tax_rate = $product_tax_rate;
                    $product_content->final_price = number_format((float)$product_final_price, 2, '.', '');

                    $product_varible->content = json_encode($product_content);


                    
                    array_push($after_products,$product_varible);
                    //dd($after_products);
                
             }

             
             
             
        }

                return view('store.importcheck',array('before_products' => $before_products,'after_products' => $after_products));

    }
    
}
