<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;
use App\Language;
use App\Menu;
use App\Content;
use App\ContentVariable;
use App\ContentSlideVariable;
use App\ContentPhotoGalleryVariable;
use App\Tag;
use App\Category;
use App\CategoryTopHasSub;

use App\Slider;
use App\Calendar;
use App\FormData;
use App\TopHasSub;
use App\NewTopHasSub;
use App\ShoppingOrder;


use App\District;
use App\DistrictVariable;
use App\Staff;
use App\StaffVariable;

use App\Country;
use App\CountryVariable;
use App\City;
use App\CityVariable;
use App\County;
use App\CountyVariable;

use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{

    public function menus($param, $order = null){
        $returnArr = array();
        $menus = NewTopHasSub::where('sub_model', 'Menu')->where('deleted', 'no')->where('top_id', $param)->orderBy('order');

        if(!empty($order)){
            foreach ($order as $o) {
                $menus = $menus->orderBy($o[0], $o[1]);
            }
        }

        $menus = $menus->get();

        foreach ($menus as $menu) {
            //dump($menu->id.'-'.$menu->top_id.'-'.$menu->sub_id.'-'.$menu->order);
            $returnArr[] = $menu;

            foreach ($this->menus($menu->sub_id, $order) as $value) {
                $returnArr[] = $value;
            }


        }
        return collect($returnArr);
    }
    public function contents($param,$sub_model, $order = null){
        $returnArr = array();
        $contents = NewTopHasSub::whereIn('sub_model',['Content', 'MenuSum'])->where('top_model', $sub_model)->where('deleted', 'no')->where('top_id', $param)->orderBy('order');


        if(!empty($order)){
            foreach ($order as $o) {
                $contents = $contents->orderBy($o[0], $o[1]);
            }
        }

        $contents = $contents->get();
        //dd($contents);

        foreach ($contents as $content) {
            //dump($content->id.'-'.$content->top_id.'-'.$content->sub_id.'-'.$content->order);
            $returnArr[] = $content;

            foreach ($this->contents($content->sub_id,$content->sub_model, $order) as $value) {
                $returnArr[] = $value;
            }


        }
        return collect($returnArr);
    }
    private $photos_path;

    public function __construct()
    {
        $this->middleware('auth');
        $this->photos_path = public_path(config('webshell.upload.path'));
    }

    public function index()
    {
        /*
        $menu = Menu::where('deleted', 'no')->where('id', 1)->first();
        $content = $menu->content()->where('deleted', 'no');
        $content = $content->orderBy('id', 'desc')->get();
        dd($content);
        */
        return view('dashboard');
    }

    public function getMenuAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        
        $orderBys = array();
        $orderBys[] = array('id', 'asc');
        $menus = $this->menus(null, $orderBys);


        //dd( $menus);
        //dd($menus[1]->topMenu->variable);
        $color_code=-1;
        $iTotalRecords = $menus->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            if($menus[$i]->top_model==null){
                $color_code++;
            }

            $params['slidertype'] = json_decode($menus[$i]->props)->slidertype;
            $records["data"][] = array(
                $menus[$i]->id,
                $menus[$i]->order,
                $menus[$i]->status,
                $menus[$i]->thatElementForMenu->variable->name,
                $menus[$i]->thatElementForMenu->variable->slug,
                is_null($menus[$i]->top_id) ? '' : $menus[$i]->topElementForMenu->variable->name,
                $menus[$i]->thatElementForMenu->type,
                $params,
                $color_code
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getContentAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        $orderBys = array();
        $orderBys[] = array('id', 'asc');
        $menu = NewTopHasSub::find($_REQUEST['menuId']);
        //dd($_REQUEST['contentId']);
        
        if (empty($_REQUEST['contentId'])) {
            //$contents = $menu->menuHasContent()->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
            /*$contents = NewTopHasSub::where('deleted', 'no')->where('top_id',$menu->sub_id)->where('top_model','Menu')->whereIn('sub_model',['Content', 'MenuSum'])->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();*/
            $contents = $this->contents($menu->sub_id,$menu->sub_model);
        }else{
            /*$topcontentths=NewTopHasSub::find($_REQUEST['contentId']);
            $contents = NewTopHasSub::where('deleted', 'no')->where('top_id',$topcontentths->sub_id)->where('top_model','Content')->whereIn('sub_model',['Content', 'MenuSum'])->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();*/
            //$contents = Content::where('deleted', 'no')->where('top_content', $_REQUEST['contentId']);
            //$contents = $contents->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
        }
        //dd($contents);
        $color_code=-1;

        $iTotalRecords = $contents->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        
        for ($i = $iDisplayStart; $i < $end; $i++) {

           if($contents[$i]->sub_model=='Content'){
                $cont = $contents[$i]->thatElementForContent;
                $title = $cont->variable->title;
                $ttype = 'content';
                $id= $contents[$i]->id;
            }else if($contents[$i]->sub_model=='MenuSum'){
                $cont = $contents[$i]->thatElementForMenusum;
                $title = $cont->variable->name;
                $ttype = 'menu';
                $id= $contents[$i]->id;
            }
            $type = $cont->type.'##'.$ttype;
            if(empty(json_decode($contents[$i]->props)->bg_img))
            $bg_img_status="false";
            else{
               $bg_img_status=json_decode($contents[$i]->props)->bg_img;
            }
            if(empty(json_decode($contents[$i]->props)->tag))
                $tag="";
            else{
              if(!(json_decode($contents[$i]->props)->tag=="no")){
                   $tag=Tag::where('id',json_decode($contents[$i]->props)->tag)->first();
                   $tag=$tag->variable->title;
               }
               else{
                    $tag="";
               }
            }
            if($contents[$i]->top_model=='Menu'){
                $color_code++;
            }
            //dd($contents[$i]);
            $records["data"][] = array(
                $id,
                $contents[$i]->order,
                $title,//.' - '.$_REQUEST['contentId'],
                $cont->variable->slug,
                $type,
                $contents[$i]->status,
                $bg_img_status,
                $tag,
                $color_code
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getTagAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        $order_by = array('id', 'order', 'title', 'status', 'actions');

        $tags = Tag::where('deleted', 'no');
        $tags = $tags->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();    
        
        $iTotalRecords = $tags->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $records["data"][] = array(
                $tags[$i]->id,
                $tags[$i]->order,
                $tags[$i]->variable->title,
                $tags[$i]->status
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getCategoryAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        $order_by = array('id', 'order', 'title', 'top_cat_id','status', 'actions');

        $categories = CategoryTopHasSub::where('deleted', 'no');
        $categories = $categories->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $categories->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        for ($i = $iDisplayStart; $i < $end; $i++) {
            $records["data"][] = array(
                $categories[$i]->id,
                $categories[$i]->order,
                $categories[$i]->variable->title,
                is_null($categories[$i]->top_category_id) ? '' : $categories[$i]->topCategoryVariable->title,
                $categories[$i]->status
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getContentAddonsAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        $order_by = array('id', 'order', 'title', 'type',  'status', 'actions');

        $contents = Content::where('deleted', 'no')->where('top_content', $_REQUEST['contentId']);
        $contents = $contents->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $contents->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $contents[$i]->id,
                $contents[$i]->order,
                $contents[$i]->variable->title,
                $contents[$i]->type,
                $contents[$i]->status
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getSliderAjax()
    {
        
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        $menu = NewTopHasSub::find($_REQUEST['menuId']);
        $order_by = array('id', 'order', 'title', 'status', 'actions');
        
        $sliders = Slider::where('deleted', 'no')->where('menu_id', $menu->sub_id );
        $sliders = $sliders->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
       
        $iTotalRecords = $sliders->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $sliders[$i]->id,
                $sliders[$i]->order,
                $sliders[$i]->variable->title,
                $sliders[$i]->status,
                ''
            );
        }
        
        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getPhotoGalleryAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        
        $order_by = array('id', 'order', 'title', 'type', 'status', 'actions');

        $contents = Content::where('deleted', 'no')->where('type', 'photogallery');
        $contents = $contents->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $contents->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $activeLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        
        for ($i = $iDisplayStart; $i < $end; $i++) {

            $menuRoad = '';
            $menuID = null;
            
            $menuitem = $contents[$i]->menu()->first();
            //dump($contents[$i]);
            
            if(!is_null($menuitem)){

                $menuRoad = $menuitem->variableLang($activeLang->code)->name;
                $menuID = $menuitem->id;
                
                /*
                    while ($menuitem->top_id != null) {
                        $menuitem = $menuitem->topMenu;
                        $menuRoad = $menuitem->variableLang($activeLang->code)->name . ' > ' . $menuRoad;
                    }
                */

                }

                $records["data"][] = array(
                    $contents[$i]->id,
                    $contents[$i]->order,
                    $contents[$i]->variable->title,
                    $menuRoad,
                    $contents[$i]->status,
                    $menuID
                );


            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
    }

    public function getCalendarAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }

        $order_by = array('id', 'order', 'status', 'title', 'start', 'end', 'capacity', 'price', 'actions');

        $calendar = Calendar::where('deleted', 'no');
        $calendar = $calendar->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $calendar->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $endDatee = '';
            if (!empty($calendar[$i]->end)) {
                $endDatee = Carbon::parse($calendar[$i]->end)->format('d/m/Y H:i');
            }

            $capp = '';
            if($calendar[$i]->capacity > 0){
                $capp = $calendar[$i]->capacity;
            }elseif($calendar[$i]->capacity == 0){
                $capp = 'Sınırsız';
            }elseif($calendar[$i]->capacity == -1){
                $capp = 'Kapalı Grup';
            }

            $records["data"][] = array(
                $calendar[$i]->id,
                $calendar[$i]->order,
                $calendar[$i]->status,
                $calendar[$i]->variable->title,
                Carbon::parse($calendar[$i]->start)->format('d/m/Y H:i'),
                $endDatee,
                $capp,
                ($calendar[$i]->price == 0) ? 'Ücretsiz' : $calendar[$i]->price,
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getFormAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }

        $order_by = array('id', 'order', 'status', 'title', 'actions');

        $form = Content::join('new_top_has_sub','new_top_has_sub.sub_id','=','content.id')
        ->where('type', 'form')->where('deleted', 'no')->where('sub_model','Content');
        $form = $form->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
        $form=$form->unique('sub_id');
        $iTotalRecords = $form->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
                //dd($form[0]->variable->title);

        for ($i = $iDisplayStart; $i < $end; $i++) {

        //$uniqForms = FormData::select('source_id', 'source_type', 'form_id')->distinct()->get();
            $menuIDsTemp = array();
            foreach ($form as $frm) {
                $menuIDsTemp[] = $frm->id;
            }
            $menuIDs = array_unique($menuIDsTemp);

            $calendarIDs = array();
            foreach (Calendar::where('form_id', $form[$i]->id)->distinct()->get() as $calendar) {
                $calendarIDs[] = $calendar->id;
            }

            $records["data"][] = array(
                $form[$i]->id,
                $form[$i]->order,
                $form[$i]->status,
                $form[$i]->variableForForm->title, //.' - '.json_encode($menuIDs).' - '.json_encode($calendarIDs),
        );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getFormDataAjax()
    {

        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }

        $form = Content::join('new_top_has_sub','new_top_has_sub.sub_id','=','content.id')
        ->where('new_top_has_sub.id', $_REQUEST['formId'])->where('type', 'form')->where('deleted', 'no')->first();
        //dd( $form->variableForForm->first());
        $formvariable = $form->variableForForm;
        $formvariableArr = json_decode($formvariable->content);

        $order_by = array('id');

        foreach($formvariableArr as $field){
            if(isset($field->name)){
                $order_by[] = $field->name;
            }
        }

        $order_by[] = 'actions';

        if(empty($_REQUEST['search']['value'])){
            $formdata = FormData::where('form_id', $form->sub_id)->orderBy('id', 'desc')->get();
        }elseif($_REQUEST['search']['value'] == 'null'){
            $formdata = FormData::where('form_id',$form->sub_id)->orderBy('id', 'desc')->get();
        }else{
            $arr = explode(':',$_REQUEST['search']['value']);
            $formdata = FormData::where('form_id',$form->sub_id)->where('source_type', $arr[0])->where('source_id', $arr[1])->orderBy('id', 'desc')->get();
        }
        //dd($form);
        //$formdata = $form->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $formdata->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array($formdata[$i]->id);
            $formDataArr = json_decode($formdata[$i]->data);

            foreach($formDataArr as $fieldname => $value){
                if(starts_with($fieldname, 'date')){
                    $records["data"][count($records["data"])-1][] = Carbon::parse($value)->format('d.m.Y');
                }else{
                    $records["data"][count($records["data"])-1][] = $value;
                }
            }

            $sourceData = '';

            if($formdata[$i]->source_type == 'calendar'){
                $sourceData = 'Etkinlik - '.Calendar::find($formdata[$i]->source_id)->variable->title;
            }else if($formdata[$i]->source_type == 'menu'){
                $source = Menu::find($formdata[$i]->source_id);

                if($formdata[$i]->lang_code=="tr"){
                    $sourceData ='<span class="m-badge m-badge--danger m-badge--wide">'.$formdata[$i]->lang_code.'</span> Menü - '.$source->variable->name;
                }
                else if($formdata[$i]->lang_code=="en"){
                    $sourceData ='<span class="m-badge m-badge--primary m-badge--wide">'.$formdata[$i]->lang_code.'</span> Menü - '.$source->variable->name;
                }
                $ths = NewTopHasSub::find($form->id);
                //dd($form->id);

            }else if($formdata[$i]->source_type == 'content'){
                $source = Content::find($formdata[$i]->source_id);
                $sourceData = 'İçerik - '.$source->variable->title;
                $ths = NewTopHasSub::find($form->id);
            }


            if(json_decode($ths->props)->props_comment_status == 'active'){
                $form_visible = $formdata[$i]->visible;
            }else{
                $form_visible = 'passive';
            }


            $records["data"][count($records["data"])-1][] = $sourceData;
            $records["data"][count($records["data"])-1][] = $form_visible;
            $records["data"][count($records["data"])-1][] = '';

        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }
    public function getOrdersAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }

        $order_by = array('id','created_at','customer_name', 'order_no', 'status','actions');

        //dd($_REQUEST);
        $orders = ShoppingOrder::where('status','!=' ,'pending-payment')
        ->where('status','!=' ,'fail-payment')->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $orders->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            if(!empty($orders[$i]->user_id)){
                $name=json_decode($orders[$i]->address_information)->firstname." ".json_decode($orders[$i]->address_information)->lastname;
            }else{
                 $customers_info=json_decode($orders[$i]->address_information)->adress;
                 $name=json_decode($customers_info)->firstname." ".json_decode($customers_info)->lastname;
            }

            $customers_info=json_decode($orders[$i]->address_information)->adress;
            $records["data"][] = array(
                $orders[$i]->id,
                $orders[$i]->created_at->format('d/m/Y--H:i'),
                $name,
                $orders[$i]->order_no,
                $orders[$i]->status,
                ''
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }
    public function getStoreAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }

        $order_by = array('content.id','product_img','product_name', 'stock', 'tax','price','d_price','final_price');

        //dd($_REQUEST);
        $products =  NewTopHasSub::join('content','content.id','=','new_top_has_sub.sub_id')
        ->join('contentvariable','contentvariable.content_id','=','content.id')
        ->where('type','group-product')->where('deleted', 'no')->where('sub_model', 'Content')
        ->orderBy('content.id')->get();
        //dd($products);

        $iTotalRecords = $products->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $product_content=json_decode($products[$i]->content);
            //dd($product_content);

            $records["data"][] = array(
                $products[$i]->id,
                $product_content->photo,
                $products[$i]->title,
                $products[$i]->stock,
                $product_content->tax_rate,
                $product_content->price,
                $product_content->discounted_price,
                $products[$i]->final_price,
                ''
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }
    public function getEventsessionAjax()
    {
        //dd("asdas");
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }

        $order_by = array('id','eventsession_date','event','show_type', 'sale_status', 'capacity','actions');

        $today=\Carbon\Carbon::now();
        
        $eventsession = NewTopHasSub::join('event_session','event_session.id','=','new_top_has_sub.sub_id')
        ->where('sub_model','EventSession')->where('deleted', 'no')
        ->whereDate('eventsession_date',">=", $today)
        ->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
        //dd($eventsession);
        $iTotalRecords = $eventsession->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            
            $event=$eventsession[$i]->topElementForContent;
            $aylar = array(1=>"Ocak",2=>"Şubat",3=>"Mart",4=>"Nisan",5=>"Mayıs",6=>"Haziran",7=>"Temmuz",8=>"Ağustos",9=>"Eylül",10=>"Ekim",11=>"Kasım",12=>"Aralık");
            $dt = \Carbon\Carbon::parse($eventsession[$i]->eventsession_date); 
            //dd($dt);
            $records["data"][] = array(
                $eventsession[$i]->id,
                $dt->day." ".$aylar[$dt->month]." ".$dt->year."  ".$dt->format('H:i'),
                $event->variable->title,
                $eventsession[$i]->show_type,
                $eventsession[$i]->sale_status,
                $eventsession[$i]->capacity,
                ''
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }


    public function uploadFile(Request $request)
    {
    //dd($request->input('slider_type'));
        if($request->input('slider_type')=="video"){
            $video = $request->file('file');
            if (!is_array($video)) {
                $video = [$video];
            }

            //dd($video[0]);
            $fileName = str_slug(str_before($video[0]->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$video[0]->getClientOriginalExtension();

            $target_file=$this->photos_path.'/video/'.$fileName;
           // dd($target_file);

             if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
               
                $slide = Slider::find($request->slider_id);
                $slideVariable = $slide->variableLang($request->lang_code);
                $slideVariable->image_url = $fileName;
                $slideVariable->save();
                echo json_encode(array('location' => $fileName, 'id' => $slideVariable->id));
                die;
            }
            //dd($fileName);

        }
        else{
            $photos = $request->file('file');
            if (!is_array($photos)) {
                $photos = [$photos];
            }
            $uploadConfig = config('webshell.upload.imageStandart');

            if(!File::exists($this->photos_path)) { File::makeDirectory($this->photos_path); }

            foreach ($uploadConfig as $key => $value) {
                if(!File::exists($this->photos_path.'/'.$key)) { File::makeDirectory($this->photos_path.'/'.$key); }
            }

            for ($i = 0; $i < count($photos); $i++) {

                $photo = $photos[$i];
                $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
                
                foreach ($uploadConfig as $key => $conf) {

                    $imageObj = Image::make($photo);

                    if ($conf['widen']) {
                        $imageObj->widen($conf['width'], function ($constraint) {
                            $constraint->upsize();
                        });
                    }
                     //dd($photo->getClientOriginalExtension());
                    $ext=$photo->getClientOriginalExtension();

                    if($ext=="png"){
                        $imageObj->save($this->photos_path . '/' . $key . '/' . $fileName,93);

                    }
                    else{
                        $imageObj->save($this->photos_path . '/' . $key . '/' . $fileName, 93,'jpg');

                    }
        }
       


            }

            if ($request->uploadType == 'groupimage') {

                if(isset($request->contentid)){
                    $content = Content::find($request->contentid);
                    $contentVariable = $content->variableLang($request->lang_code);
                    //$_content = array();
                    //$_content['photo'] = $fileName;
                    //$contentVariable->content = json_encode($_content);
                    $_content=json_decode($contentVariable->content);
                    $_content->photo=$fileName;//static çekiliyor
                    //dd($_content);
                    $contentVariable->content = json_encode($_content);
                    $contentVariable->save();
                    echo json_encode(array('location' => $fileName, 'id' => $contentVariable->id));
                    die;
                }


            }
            if ($request->uploadType == 'contentimage') {

                if(isset($request->contentid)){
                    $content = Content::find($request->contentid);
                    $contentVariable = $content->variableLang($request->lang_code);
                    $contentVariable->content = $fileName;
                    $contentVariable->save();
                    echo json_encode(array('location' => $fileName, 'id' => $contentVariable->id));
                    die;
                }
                

            }
            if ($request->uploadType == 'section_bg_img') {

                if(isset($request->contentid)){
                    $content = Content::find($request->contentid);
                    $contentVariable = $content->variableLang($request->lang_code);

                    $section_content = array();
                    $section_content['section_bg_img'] = $fileName;
                    $contentVariable->content = json_encode($section_content);

                    $contentVariable->save();
                    echo json_encode(array('location' => $fileName, 'id' => $contentVariable->id));
                    die;
                }
                

            }else if ($request->uploadType == 'galleryimage') {

                if(isset($request->contentid)){
                    $contentPhotoGalleryVariable = new ContentPhotoGalleryVariable();
                    $contentPhotoGalleryVariable->content_id = $request->contentid;
                    $contentPhotoGalleryVariable->lang = $request->lang_code;
                    $contentPhotoGalleryVariable->url = $fileName;
                    $contentPhotoGalleryVariable->order = 1000;
                    $contentPhotoGalleryVariable->save();
                    echo json_encode(array('location' => $fileName, 'id' => $contentPhotoGalleryVariable->id));
                    die;
                }

            }else if ($request->uploadType == 'slideimage') {

                if(isset($request->contentid)){
                    $content = Content::find($request->contentid);

                    $contentSlide = new ContentSlideVariable();
                    $contentSlide->content_id  = $request->contentid;
                    $contentSlide->lang_code = $request->lang_code;
                    $contentSlide->title = '';
                    $contentSlide->image_url = $fileName;
                    $contentSlide->order = 1000;
                    $contentSlide->save();

                    echo json_encode(array('location' => $fileName, 'id' => $contentSlide->id));
                    die;
                }

            }else if ($request->uploadType == 'stimage') {

                if(isset($request->menuid)){
                    $menu = Menu::find($request->menuid);
                    $menuVariable = $menu->variableLang($request->lang_code);
                    $menuVariable->stvalue = $fileName;
                    $menuVariable->save();

                    echo json_encode(array('location' => $fileName, 'id' => $menuVariable->id));
                    die;
                }

            }else if ($request->uploadType == 'stslider') {

                $slide = Slider::find($request->slider_id);
                $slideVariable = $slide->variableLang($request->lang_code);
                $slideVariable->image_url = $fileName;
                $slideVariable->save();
                echo json_encode(array('location' => $fileName, 'id' => $slideVariable->id));
                die;

            }else if ($request->uploadType == 'calendarimage') {

                if(isset($request->calendarid)){
                    $calendarVariable = Calendar::find($request->calendarid)->variableLang($request->lang_code);
                    $calendarVariable->image_name = $fileName;
                    $calendarVariable->save();
                    echo json_encode(array('location' => $fileName, 'id' => $calendarVariable->id));
                    die;
                }

            }else if ($request->uploadType == 'contentbgimage') {
                if(isset($request->contentid)){
                    $content = Content::find($request->contentid);
                    $contentVariable = $content->variableLang($request->lang_code);
                    $contentVariable->bgimageurl = $fileName;
                    $contentVariable->save();
                    echo json_encode(array('location' => $fileName));
                    die;
                }
            }else if ($request->uploadType == 'editor') {
                echo json_encode(array('location' => url('upload/xlarge/'.$fileName)));
                die;
            }else if ($request->uploadType == 'staffimage') {

                if(isset($request->contentid)){
                    $staff = Staff::find($request->contentid);
                    $staffVariable = $staff->variableLang($request->lang_code);
                    $staffVariable->photo_url = $fileName;
                    $staffVariable->save();
                    echo json_encode(array('location' => $fileName, 'id' => $staffVariable->id));
                    die;
                }
            }

        }
    }

    public function cropThumbnail(Request $request)
    {
        //dd($request->input());
        if($request->input('uploadType') == 'mapturkey'){
            $uploadConfig = config('webshell.upload.imageThumbnail.thumbnailStaff');
        }else{
            $uploadConfig = config('webshell.upload.imageThumbnail.thumbnail');
        }

        if(!File::exists($this->photos_path)) { File::makeDirectory($this->photos_path); }
        if(!File::exists($this->photos_path.'/thumbnail')) { File::makeDirectory($this->photos_path.'/thumbnail'); }

        $photo = $request->file('file');

        if ($request->uploadType == 'photo') {
            $photoRow = ContentVariable::find($request->input('id'));
            $fileName = $photoRow->content;
        }else if ($request->uploadType == 'photogallery') {
            $photoRow = ContentPhotoGalleryVariable::find($request->input('id'));
            $fileName = $photoRow->url;
        }else if ($request->uploadType == 'mapturkey') {
            $photoRow = Staff::find($request->input('id'))->variableLang($request->lang_code);
            $fileName = $photoRow->photo_url;
        }

        $imageObj = Image::make($photo);

        if ($uploadConfig['widen']) {
            $imageObj->widen($uploadConfig['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        /*
            if ($uploadConfig['crop']) {

                if($uploadConfig['height'] == 0){
                    $imageObj->crop($uploadConfig['width'], ($uploadConfig['width']/$uploadConfig['aspectRatio']));
                }else{
                    $imageObj->crop($uploadConfig['width'], $uploadConfig['height']);
                }
                
            }
        */

        $imageObj->save($this->photos_path . '/thumbnail/' . $fileName, 93);

        if ($request->uploadType == 'photo') {
            $photoRow->content = $fileName;
        }else if ($request->uploadType == 'photogallery') {
            $photoRow->url = $fileName;
        }

        $photoRow->save();
        echo json_encode(array('location' => $fileName, 'id' => $photoRow->id));
        die;

    }

    public function getMapMarkerAjax()
    {
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }
        $order_by = array('id', 'order', 'name', 'ptitle', 'district_id', 'city_id', 'status', 'actions');

        $menu = NewTopHasSub::where('deleted', 'no')->where('sub_id',$_REQUEST['menuId'])->where('sub_model','Menu')->first();
        $topcontentths=NewTopHasSub::find($_REQUEST['contentId']);
    
        
        //DB::enableQueryLog();
        //dd($contents);
        $mapmarkers = Staff::where('deleted', 'no')->where('content_id', $topcontentths->sub_id);
        $mapmarkers = $mapmarkers->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
        //dd(DB::getQueryLog());

        $iTotalRecords = $mapmarkers->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $mapmarkers[$i]->id,
                $mapmarkers[$i]->order,
                $mapmarkers[$i]->variable->name,
                config('webshell.map_ptitle.'.$mapmarkers[$i]->variable->title.'.title'),
                (is_null($mapmarkers[$i]->district_id)) ? '' : $mapmarkers[$i]->district->variable->name,
                ((is_null($mapmarkers[$i]->city)) ? '' : $mapmarkers[$i]->city->variable->name).((is_null($mapmarkers[$i]->county)) ? '' : ' - '.$mapmarkers[$i]->county->variable->name),
                $mapmarkers[$i]->status
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }


    }
