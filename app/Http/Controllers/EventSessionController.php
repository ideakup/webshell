<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NewTopHasSub;
use App\EventSession;
use App\Content;



class EventSessionController extends Controller
{
          
    public function eventsession()
    {
        return view('eventsession.list');

    }
    public function add()
    {

        $events=Content::where('type','group-event')->get();
        return view('eventsession.add',array('events' => $events));

    }
    public function edit($es_id)
    {
        $events=Content::where('type','group-event')->get();
        $event_session=EventSession::find($es_id); 
        $event_session_ths=$event_session->topHasSub;

        $dt = \Carbon\Carbon::parse($event_session->eventsession_date); 
        $price_attribute=json_decode($event_session->price);
        return view('eventsession.edit',array('event_session' => $event_session,'events' => $events,'event_session_ths' => $event_session_ths,'dt' => $dt,'price_attribute' => $price_attribute));
    }
    public function delete($es_id)
    {
        $event_session=EventSession::find($es_id); 
        $event_session_ths=$event_session->topHasSub;

        $dt = \Carbon\Carbon::parse($event_session->eventsession_date); 
        return view('eventsession.delete',array('event_session' => $event_session,'event_session_ths' => $event_session_ths,'dt' => $dt));
    }
    public function save(Request $request){
        //dd($request->input());
        if($request->crud=='add'){
            $event_session = new EventSession();

            $event_session->show_type=$request->show_type;
            $event_session->eventsession_date=$request->eventsession_date;
            $event_session->sale_status=$request->sale_status;
            $event_session->capacity=$request->capacity;

            $price = array();
            $price['tax_rate'] = $request->tax_rate;
            $price['price'] = $request->price;
            $price["discounted_price"] = $request->discounted_price;
       
            $event_session->price= json_encode($price);

            $event_session->save();

            //NEW TOPHASSUB INSERT
            $eventsession_new_ths=new NewTopHasSub();
            $eventsession_new_ths->top_id=$request->event;
            $eventsession_new_ths->top_model='Content';
            $eventsession_new_ths->sub_id=$event_session->id;
            $eventsession_new_ths->sub_model='EventSession';
            
            $eventsession_new_ths->status = 'active';
            $eventsession_new_ths->deleted = 'no';
            $eventsession_new_ths->order = 276276;
            
            $eventsession_new_ths->save();

        }
        else if($request->crud == 'edit'){
            $event_session=EventSession::find($request->eventsession_id); 
            $event_session_ths=$event_session->topHasSub;

            $event_session_ths->top_id=$request->event;
            $event_session_ths->save();

            $event_session->show_type=$request->show_type;
            $event_session->eventsession_date=$request->eventsession_date;
            $event_session->sale_status=$request->sale_status;
            $event_session->capacity=$request->capacity;
            $price = array();
            $price['tax_rate'] = $request->tax_rate;
            $price['price'] = $request->price;
            $price["discounted_price"] = $request->discounted_price;
       
            $event_session->price= json_encode($price);

            $event_session->save();

        }
        else if($request->crud == 'delete'){
            $event_session=EventSession::find($request->eventsession_id); 
            $event_session_ths=$event_session->topHasSub;

            $event_session_ths->deleted = 'yes';
            $event_session_ths->status = 'passive';
            $event_session_ths->save();
        }

        return redirect('/eventsession/list');


    }
}
