<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistrictVariable extends Model
{
    protected $table = 'map_districtvariable';
}
