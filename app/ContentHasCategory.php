<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentHasCategory extends Model
{
    protected $table = 'content_has_category';

    public function categoryTopHasSub()
    {
        return $this->hasOne('App\CategoryTopHasSub', 'id', 'category_id');
    }
}
