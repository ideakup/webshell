<?php

namespace App\Exports;

use App\Content;
use App\FormData;
use App\NewTopHasSub;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProductDataExport implements FromView
{
    
    protected $arr;
    
    public function __construct(array $arr)
    {
        $this->arr = $arr;
    }

    public function view(): View
    {
        $products =  NewTopHasSub::join('content','content.id','=','new_top_has_sub.sub_id')
        ->join('contentvariable','contentvariable.content_id','=','content.id')
        ->where('type','group-product')->where('deleted', 'no')->get();
    	
    	//dd($products);
    	return view('exports.productData', array('products' => $products));
    }
}
