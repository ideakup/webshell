<?php

namespace App\Exports;

use App\Content;
use App\FormData;
use App\NewTopHasSub;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class FormDataExport implements FromView
{
    
    protected $arr;
    
    public function __construct(array $arr)
    {
        $this->arr = $arr;
    }

    public function view(): View
    {
        $form_ths=NewTopHasSub::where('id',$this->arr['formId'])->where('deleted', 'no')->first();
        //dd( $form_ths);
    	$form = Content::where('id', $form_ths->sub_id)->where('type', 'form')->first();
    	$formData = FormData::where('form_id', $form_ths->sub_id);
    	
    	if($this->arr['formDist']!= 'null'){
    		$formdist = explode(':', $this->arr['formDist']);
    		$formData = $formData->where('source_type', $formdist[0])->where('source_id', $formdist[1]);
    	}
    	
    	$formData = $formData->get();
    	return view('exports.formData', array('form' => $form, 'formData' => $formData));
    }
}
