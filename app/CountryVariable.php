<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryVariable extends Model
{
    protected $table = 'map_countryvariable';
}
